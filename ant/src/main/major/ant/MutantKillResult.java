package major.ant;

/**
 * Wrapper class that indicates the kill reason for a given mutant number.
 */
public class MutantKillResult {

    public enum KillReason {FAIL, EXC, TIME};

    private int mutantNo;
    private KillReason reason;
    private String stackTrace;

    public MutantKillResult(int mutantNo, KillReason reason) {
        this.mutantNo = mutantNo;
        this.reason = reason;
    }

    public MutantKillResult(int mutantNo, KillReason reason, String stackTrace) {
        this.mutantNo = mutantNo;
        this.reason = reason;
        this.stackTrace = stackTrace;
    }

    public int getMutantNo() {
        return mutantNo;
    }

    public KillReason getKillReason() {
        return reason;
    }

    public String getStackTrace() {
        return stackTrace;
    }
}
