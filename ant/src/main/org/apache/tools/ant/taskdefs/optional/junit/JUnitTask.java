/*
/*  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.apache.tools.ant.taskdefs.optional.junit;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import junit.framework.AssertionFailedError;
import junit.framework.JUnit4TestCaseFacade;
import junit.framework.Test;
import junit.framework.TestCase;
import major.ant.MutantKillResult;
import major.ant.MutantKillResult.KillReason;

import org.apache.tools.ant.AntClassLoader;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.taskdefs.ExecuteWatchdog;
import org.apache.tools.ant.taskdefs.LogOutputStream;
import org.apache.tools.ant.taskdefs.PumpStreamHandler;
import org.apache.tools.ant.types.Assertions;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.CommandlineJava;
import org.apache.tools.ant.types.EnumeratedAttribute;
import org.apache.tools.ant.types.Environment;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.Permissions;
import org.apache.tools.ant.types.PropertySet;
import org.apache.tools.ant.util.FileUtils;
import org.apache.tools.ant.util.LoaderUtils;
import org.apache.tools.ant.util.SplitClassLoader;

/**
 * Runs JUnit tests.
 *
 * <p> JUnit is a framework to create unit tests. It has been initially
 * created by Erich Gamma and Kent Beck.  JUnit can be found at <a
 * href="http://www.junit.org">http://www.junit.org</a>.
 *
 * <p> <code>JUnitTask</code> can run a single specific
 * <code>JUnitTest</code> using the <code>test</code> element.</p>
 * For example, the following target <code><pre>
 *   &lt;target name="test-int-chars" depends="jar-test"&gt;
 *       &lt;echo message="testing international characters"/&gt;
 *       &lt;junit printsummary="no" haltonfailure="yes" fork="false"&gt;
 *           &lt;classpath refid="classpath"/&gt;
 *           &lt;formatter type="plain" usefile="false" /&gt;
 *           &lt;test name="org.apache.ecs.InternationalCharTest" /&gt;
 *       &lt;/junit&gt;
 *   &lt;/target&gt;
 * </pre></code>
 * <p>runs a single junit test
 * (<code>org.apache.ecs.InternationalCharTest</code>) in the current
 * VM using the path with id <code>classpath</code> as classpath and
 * presents the results formatted using the standard
 * <code>plain</code> formatter on the command line.</p>
 *
 * <p> This task can also run batches of tests.  The
 * <code>batchtest</code> element creates a <code>BatchTest</code>
 * based on a fileset.  This allows, for example, all classes found in
 * directory to be run as testcases.</p>
 *
 * <p>For example,</p><code><pre>
 * &lt;target name="run-tests" depends="dump-info,compile-tests" if="junit.present"&gt;
 *   &lt;junit printsummary="no" haltonfailure="yes" fork="${junit.fork}"&gt;
 *     &lt;jvmarg value="-classic"/&gt;
 *     &lt;classpath refid="tests-classpath"/&gt;
 *     &lt;sysproperty key="build.tests" value="${build.tests}"/&gt;
 *     &lt;formatter type="brief" usefile="false" /&gt;
 *     &lt;batchtest&gt;
 *       &lt;fileset dir="${tests.dir}"&gt;
 *         &lt;include name="**&#047;*Test*" /&gt;
 *       &lt;/fileset&gt;
 *     &lt;/batchtest&gt;
 *   &lt;/junit&gt;
 * &lt;/target&gt;
 * </pre></code>
 * <p>this target finds any classes with a <code>test</code> directory
 * anywhere in their path (under the top <code>${tests.dir}</code>, of
 * course) and creates <code>JUnitTest</code>'s for each one.</p>
 *
 * <p> Of course, <code>&lt;junit&gt;</code> and
 * <code>&lt;batch&gt;</code> elements can be combined for more
 * complex tests. For an example, see the ant <code>build.xml</code>
 * target <code>run-tests</code> (the second example is an edited
 * version).</p>
 *
 * <p> To spawn a new Java VM to prevent interferences between
 * different testcases, you need to enable <code>fork</code>.  A
 * number of attributes and elements allow you to set up how this JVM
 * runs.
 *
 *****************************************************************************************
 *
 * TODO: This class is a mess! Also, Ant's JUnit wrapper has caused quite some trouble
 * when running Major on tests that rely on the behavior of JUnit core.
 *
 * We will get rid of this back-end entirely as we have built a new one that builds on top
 * of JUnit core. We will still have an Ant plug-in that will in turn call the JUnit core
 * back-end.
 *
 *****************************************************************************************
 *
 * @since Ant 1.2
 *
 * @see JUnitTest
 * @see BatchTest
 */
public class JUnitTask extends Task {

    private static final String LINE_SEP
        = System.getProperty("line.separator");
    private static final String CLASSPATH = "CLASSPATH";
    private CommandlineJava commandline;
    private Vector tests = new Vector();
    private Vector batchTests = new Vector();
    private Vector formatters = new Vector();
    private File dir = null;

    private Integer timeout = null;
    private boolean summary = false;
    private boolean reloading = true;
    private String summaryValue = "";
    private JUnitTaskMirror.JUnitTestRunnerMirror runner = null;

    private boolean newEnvironment = false;
    private Environment env = new Environment();

    private boolean includeAntRuntime = true;
    private Path antRuntimeClasses = null;

    // Do we send output to System.out/.err in addition to the formatters?
    private boolean showOutput = false;

    // Do we send output to the formatters ?
    private boolean outputToFormatters = true;

    private boolean logFailedTests = true;

    private File tmpDir;
    private AntClassLoader classLoader = null;
    private Permissions perm = null;
    private ForkMode forkMode = new ForkMode("perTest");

    private boolean splitJunit = false;
    private boolean enableTestListenerEvents = false;
    private JUnitTaskMirror delegate;
    private ClassLoader mirrorLoader;

    /** A boolean on whether to get the forked path for ant classes */
    private boolean forkedPathChecked = false;

    //   Attributes for basetest
    private boolean haltOnError = false;
    private boolean haltOnFail  = false;
    private boolean filterTrace = true;
    private boolean fork        = false;
    private String  failureProperty;
    private String  errorProperty;

    private static final int STRING_BUFFER_SIZE = 128;
    /**
     * @since Ant 1.7
     */
    public static final String TESTLISTENER_PREFIX =
        "junit.framework.TestListener: ";

    /**
     * Name of magic property that enables test listener events.
     */
    public static final String ENABLE_TESTLISTENER_EVENTS =
        "ant.junit.enabletestlistenerevents";

    private static final FileUtils FILE_UTILS = FileUtils.getFileUtils();

    /**
     * If true, force ant to re-classload all classes for each JUnit TestCase
     *
     * @param value force class reloading for each test case
     */
    public void setReloading(boolean value) {
        reloading = value;
    }

    /**
     * If true, smartly filter the stack frames of
     * JUnit errors and failures before reporting them.
     *
     * <p>This property is applied on all BatchTest (batchtest) and
     * JUnitTest (test) however it can possibly be overridden by their
     * own properties.</p>
     * @param value <tt>false</tt> if it should not filter, otherwise
     * <tt>true<tt>
     *
     * @since Ant 1.5
     */
    public void setFiltertrace(boolean value) {
        this.filterTrace = value;
    }

    /**
     * If true, stop the build process when there is an error in a test.
     * This property is applied on all BatchTest (batchtest) and JUnitTest
     * (test) however it can possibly be overridden by their own
     * properties.
     * @param value <tt>true</tt> if it should halt, otherwise
     * <tt>false</tt>
     *
     * @since Ant 1.2
     */
    public void setHaltonerror(boolean value) {
        this.haltOnError = value;
    }

    /**
     * Property to set to "true" if there is a error in a test.
     *
     * <p>This property is applied on all BatchTest (batchtest) and
     * JUnitTest (test), however, it can possibly be overriden by
     * their own properties.</p>
     * @param propertyName the name of the property to set in the
     * event of an error.
     *
     * @since Ant 1.4
     */
    public void setErrorProperty(String propertyName) {
        this.errorProperty = propertyName;
    }

    /**
     * If true, stop the build process if a test fails
     * (errors are considered failures as well).
     * This property is applied on all BatchTest (batchtest) and
     * JUnitTest (test) however it can possibly be overridden by their
     * own properties.
     * @param value <tt>true</tt> if it should halt, otherwise
     * <tt>false</tt>
     *
     * @since Ant 1.2
     */
    public void setHaltonfailure(boolean value) {
        this.haltOnFail = value;
    }

    /**
     * Property to set to "true" if there is a failure in a test.
     *
     * <p>This property is applied on all BatchTest (batchtest) and
     * JUnitTest (test), however, it can possibly be overriden by
     * their own properties.</p>
     * @param propertyName the name of the property to set in the
     * event of an failure.
     *
     * @since Ant 1.4
     */
    public void setFailureProperty(String propertyName) {
        this.failureProperty = propertyName;
    }

    /**
     * If true, JVM should be forked for each test.
     *
     * <p>It avoids interference between testcases and possibly avoids
     * hanging the build.  this property is applied on all BatchTest
     * (batchtest) and JUnitTest (test) however it can possibly be
     * overridden by their own properties.</p>
     * @param value <tt>true</tt> if a JVM should be forked, otherwise
     * <tt>false</tt>
     * @see #setTimeout
     *
     * @since Ant 1.2
     */
    public void setFork(boolean value) {
        this.fork = value;
    }

    /**
     * Set the behavior when {@link #setFork fork} fork has been enabled.
     *
     * <p>Possible values are "once", "perTest" and "perBatch".  If
     * set to "once", only a single Java VM will be forked for all
     * tests, with "perTest" (the default) each test will run in a
     * fresh Java VM and "perBatch" will run all tests from the same
     * &lt;batchtest&gt; in the same Java VM.</p>
     *
     * <p>This attribute will be ignored if tests run in the same VM
     * as Ant.</p>
     *
     * <p>Only tests with the same configuration of haltonerror,
     * haltonfailure, errorproperty, failureproperty and filtertrace
     * can share a forked Java VM, so even if you set the value to
     * "once", Ant may need to fork mutliple VMs.</p>
     * @param mode the mode to use.
     * @since Ant 1.6.2
     */
    public void setForkMode(ForkMode mode) {
        this.forkMode = mode;
    }

    /**
     * If true, print one-line statistics for each test, or "withOutAndErr"
     * to also show standard output and error.
     *
     * Can take the values on, off, and withOutAndErr.
     * @param value <tt>true</tt> to print a summary,
     * <tt>withOutAndErr</tt> to include the test&apos;s output as
     * well, <tt>false</tt> otherwise.
     * @see SummaryJUnitResultFormatter
     *
     * @since Ant 1.2
     */
    public void setPrintsummary(SummaryAttribute value) {
        summaryValue = value.getValue();
        summary = value.asBoolean();
    }

    /**
     * Print summary enumeration values.
     */
    public static class SummaryAttribute extends EnumeratedAttribute {
        /**
         * list the possible values
         * @return  array of allowed values
         */
        public String[] getValues() {
            return new String[] {"true", "yes", "false", "no",
                                 "on", "off", "withOutAndErr"};
        }

        /**
         * gives the boolean equivalent of the authorized values
         * @return boolean equivalent of the value
         */
        public boolean asBoolean() {
            String v = getValue();
            return "true".equals(v)
                || "on".equals(v)
                || "yes".equals(v)
                || "withOutAndErr".equals(v);
        }
    }

    /**
     * Set the timeout value (in milliseconds).
     *
     * <p>If the test is running for more than this value, the test
     * will be canceled. (works only when in 'fork' mode).</p>
     * @param value the maximum time (in milliseconds) allowed before
     * declaring the test as 'timed-out'
     * @see #setFork(boolean)
     *
     * @since Ant 1.2
     */
    public void setTimeout(Integer value) {
        timeout = value;
    }

    /**
     * Set the maximum memory to be used by all forked JVMs.
     * @param   max     the value as defined by <tt>-mx</tt> or <tt>-Xmx</tt>
     *                  in the java command line options.
     *
     * @since Ant 1.2
     */
    public void setMaxmemory(String max) {
        getCommandline().setMaxmemory(max);
    }

    /**
     * The command used to invoke the Java Virtual Machine,
     * default is 'java'. The command is resolved by
     * java.lang.Runtime.exec(). Ignored if fork is disabled.
     *
     * @param   value   the new VM to use instead of <tt>java</tt>
     * @see #setFork(boolean)
     *
     * @since Ant 1.2
     */
    public void setJvm(String value) {
        getCommandline().setVm(value);
    }

    /**
     * Adds a JVM argument; ignored if not forking.
     *
     * @return create a new JVM argument so that any argument can be
     * passed to the JVM.
     * @see #setFork(boolean)
     *
     * @since Ant 1.2
     */
    public Commandline.Argument createJvmarg() {
        return getCommandline().createVmArgument();
    }

    /**
     * The directory to invoke the VM in. Ignored if no JVM is forked.
     * @param   dir     the directory to invoke the JVM from.
     * @see #setFork(boolean)
     *
     * @since Ant 1.2
     */
    public void setDir(File dir) {
        this.dir = dir;
    }

    /**
     * Adds a system property that tests can access.
     * This might be useful to tranfer Ant properties to the
     * testcases when JVM forking is not enabled.
     *
     * @since Ant 1.3
     * @deprecated since ant 1.6
     * @param sysp environment variable to add
     */
    public void addSysproperty(Environment.Variable sysp) {

        getCommandline().addSysproperty(sysp);
    }

    /**
     * Adds a system property that tests can access.
     * This might be useful to tranfer Ant properties to the
     * testcases when JVM forking is not enabled.
     * @param sysp new environment variable to add
     * @since Ant 1.6
     */
    public void addConfiguredSysproperty(Environment.Variable sysp) {
        // get a build exception if there is a missing key or value
        // see bugzilla report 21684
        String testString = sysp.getContent();
        getProject().log("sysproperty added : " + testString, Project.MSG_DEBUG);
        getCommandline().addSysproperty(sysp);
    }

    /**
     * Adds a set of properties that will be used as system properties
     * that tests can access.
     *
     * This might be useful to tranfer Ant properties to the
     * testcases when JVM forking is not enabled.
     *
     * @param sysp set of properties to be added
     * @since Ant 1.6
     */
    public void addSyspropertyset(PropertySet sysp) {
        getCommandline().addSyspropertyset(sysp);
    }

    /**
     * Adds path to classpath used for tests.
     *
     * @return reference to the classpath in the embedded java command line
     * @since Ant 1.2
     */
    public Path createClasspath() {
        return getCommandline().createClasspath(getProject()).createPath();
    }

    /**
     * Adds a path to the bootclasspath.
     * @return reference to the bootclasspath in the embedded java command line
     * @since Ant 1.6
     */
    public Path createBootclasspath() {
        return getCommandline().createBootclasspath(getProject()).createPath();
    }

    /**
     * Adds an environment variable; used when forking.
     *
     * <p>Will be ignored if we are not forking a new VM.</p>
     * @param var environment variable to be added
     * @since Ant 1.5
     */
    public void addEnv(Environment.Variable var) {
        env.addVariable(var);
    }

    /**
     * If true, use a new environment when forked.
     *
     * <p>Will be ignored if we are not forking a new VM.</p>
     *
     * @param newenv boolean indicating if setting a new environment is wished
     * @since Ant 1.5
     */
    public void setNewenvironment(boolean newenv) {
        newEnvironment = newenv;
    }

    /**
     * Preset the attributes of the test
     * before configuration in the build
     * script.
     * This allows attributes in the <junit> task
     * be be defaults for the tests, but allows
     * individual tests to override the defaults.
     */
    private void preConfigure(BaseTest test) {
        test.setFiltertrace(filterTrace);
        test.setHaltonerror(haltOnError);
        if (errorProperty != null) {
            test.setErrorProperty(errorProperty);
        }
        test.setHaltonfailure(haltOnFail);
        if (failureProperty != null) {
            test.setFailureProperty(failureProperty);
        }
        test.setFork(fork);
    }

    /**
     * Add a new single testcase.
     * @param   test    a new single testcase
     * @see JUnitTest
     *
     * @since Ant 1.2
     */
    public void addTest(JUnitTest test) {
        tests.addElement(test);
        preConfigure(test);
    }

    /**
     * Adds a set of tests based on pattern matching.
     *
     * @return  a new instance of a batch test.
     * @see BatchTest
     *
     * @since Ant 1.2
     */
    public BatchTest createBatchTest() {
        BatchTest test = new BatchTest(getProject());
        batchTests.addElement(test);
        preConfigure(test);
        return test;
    }

    /**
     * Add a new formatter to all tests of this task.
     *
     * @param fe formatter element
     * @since Ant 1.2
     */
    public void addFormatter(FormatterElement fe) {
        formatters.addElement(fe);
    }

    /**
     * If true, include ant.jar, optional.jar and junit.jar in the forked VM.
     *
     * @param b include ant run time yes or no
     * @since Ant 1.5
     */
    public void setIncludeantruntime(boolean b) {
        includeAntRuntime = b;
    }

    /**
     * If true, send any output generated by tests to Ant's logging system
     * as well as to the formatters.
     * By default only the formatters receive the output.
     *
     * <p>Output will always be passed to the formatters and not by
     * shown by default.  This option should for example be set for
     * tests that are interactive and prompt the user to do
     * something.</p>
     *
     * @param showOutput if true, send output to Ant's logging system too
     * @since Ant 1.5
     */
    public void setShowOutput(boolean showOutput) {
        this.showOutput = showOutput;
    }

    /**
     * If true, send any output generated by tests to the formatters.
     *
     * @param outputToFormatters if true, send output to formatters (Default
     *                           is true).
     * @since Ant 1.7.0
     */
    public void setOutputToFormatters(boolean outputToFormatters) {
        this.outputToFormatters = outputToFormatters;
    }

    /**
     * If true, write a single "FAILED" line for failed tests to Ant's
     * log system.
     *
     * @since Ant 1.8.0
     */
    public void setLogFailedTests(boolean logFailedTests) {
        this.logFailedTests = logFailedTests;
    }

    /**
     * Assertions to enable in this program (if fork=true)
     * @since Ant 1.6
     * @param asserts assertion set
     */
    public void addAssertions(Assertions asserts) {
        if (getCommandline().getAssertions() != null) {
            throw new BuildException("Only one assertion declaration is allowed");
        }
        getCommandline().setAssertions(asserts);
    }

    /**
     * Sets the permissions for the application run inside the same JVM.
     * @since Ant 1.6
     * @return .
     */
    public Permissions createPermissions() {
        if (perm == null) {
            perm = new Permissions();
        }
        return perm;
    }

    /**
     * If set, system properties will be copied to the cloned VM - as
     * well as the bootclasspath unless you have explicitly specified
     * a bootclaspath.
     *
     * <p>Doesn't have any effect unless fork is true.</p>
     * @param cloneVm a <code>boolean</code> value.
     * @since Ant 1.7
     */
    public void setCloneVm(boolean cloneVm) {
        getCommandline().setCloneVm(cloneVm);
    }

    /**
     * Creates a new JUnitRunner and enables fork of a new Java VM.
     *
     * @throws Exception under ??? circumstances
     * @since Ant 1.2
     */
    public JUnitTask() throws Exception {
    }

    /**
     * Where Ant should place temporary files.
     *
     * @param tmpDir location where temporary files should go to
     * @since Ant 1.6
     */
    public void setTempdir(File tmpDir) {
        if (tmpDir != null) {
            if (!tmpDir.exists() || !tmpDir.isDirectory()) {
                throw new BuildException(tmpDir.toString()
                                         + " is not a valid temp directory");
            }
        }
        this.tmpDir = tmpDir;
    }

    /**
     * Whether test listener events shall be generated.
     *
     * <p>Defaults to false.</p>
     *
     * <p>This value will be overridden by the magic property
     * ant.junit.enabletestlistenerevents if it has been set.</p>
     *
     * @since Ant 1.8.2
     */
    public void setEnableTestListenerEvents(boolean b) {
        enableTestListenerEvents = b;
    }

    /**
     * Whether test listener events shall be generated.
     * @since Ant 1.8.2
     */
    public boolean getEnableTestListenerEvents() {
        String e = getProject().getProperty(ENABLE_TESTLISTENER_EVENTS);
        if (e != null) {
            return Project.toBoolean(e);
        }
        return enableTestListenerEvents;
    }

    /**
     * Adds the jars or directories containing Ant, this task and
     * JUnit to the classpath - this should make the forked JVM work
     * without having to specify them directly.
     *
     * @since Ant 1.4
     */
    public void init() {
        antRuntimeClasses = new Path(getProject());
        splitJunit = !addClasspathResource("/junit/framework/TestCase.class");
        addClasspathEntry("/org/apache/tools/ant/launch/AntMain.class");
        addClasspathEntry("/org/apache/tools/ant/Task.class");
        addClasspathEntry("/org/apache/tools/ant/taskdefs/optional/junit/JUnitTestRunner.class");
        addClasspathEntry("/org/apache/tools/ant/taskdefs/optional/junit/JUnit4TestMethodAdapter.class");
    }

    private static JUnitTaskMirror createMirror(JUnitTask task, ClassLoader loader) {
        try {
            loader.loadClass("junit.framework.Test"); // sanity check
        } catch (ClassNotFoundException e) {
            throw new BuildException(
                    "The <classpath> for <junit> must include junit.jar "
                    + "if not in Ant's own classpath",
                    e, task.getLocation());
        }
        try {
            Class c = loader.loadClass(JUnitTaskMirror.class.getName() + "Impl");
            if (c.getClassLoader() != loader) {
                throw new BuildException("Overdelegating loader", task.getLocation());
            }
            Constructor cons = c.getConstructor(new Class[] {JUnitTask.class});
            return (JUnitTaskMirror) cons.newInstance(new Object[] {task});
        } catch (Exception e) {
            throw new BuildException(e, task.getLocation());
        }
    }

    /**
     * Sets up the delegate that will actually run the tests.
     *
     * <p>Will be invoked implicitly once the delegate is needed.</p>
     *
     * @since Ant 1.7.1
     */
    protected void setupJUnitDelegate() {
        final ClassLoader myLoader = JUnitTask.class.getClassLoader();
        if (splitJunit) {
            final Path path = new Path(getProject());
            path.add(antRuntimeClasses);
            Path extra = getCommandline().getClasspath();
            if (extra != null) {
                path.add(extra);
            }
            mirrorLoader = (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
                public Object run() {
                    return new SplitClassLoader(myLoader, path, getProject(),
                                     new String[] {
                                         "BriefJUnitResultFormatter",
                                         "JUnit4TestMethodAdapter",
                                         "JUnitResultFormatter",
                                         "JUnitTaskMirrorImpl",
                                         "JUnitTestRunner",
                                         "JUnitVersionHelper",
                                         "OutErrSummaryJUnitResultFormatter",
                                         "PlainJUnitResultFormatter",
                                         "SummaryJUnitResultFormatter",
                                         "TearDownOnVmCrash",
                                         "XMLJUnitResultFormatter",
                                     });
                }
            });
        } else {
            mirrorLoader = myLoader;
        }
        delegate = createMirror(this, mirrorLoader);
    }

    /**
     * Runs the testcase.
     *
     * @throws BuildException in case of test failures or errors
     * @since Ant 1.2
     */
    public void execute1() throws BuildException {
        checkMethodLists();

        setupJUnitDelegate();

        List testLists = new ArrayList();

        boolean forkPerTest = forkMode.getValue().equals(ForkMode.PER_TEST);
        if (forkPerTest || forkMode.getValue().equals(ForkMode.ONCE)) {
            testLists.addAll(executeOrQueue(getIndividualTests(),
                                            forkPerTest));
        } else { /* forkMode.getValue().equals(ForkMode.PER_BATCH) */
            final int count = batchTests.size();
            for (int i = 0; i < count; i++) {
                BatchTest batchtest = (BatchTest) batchTests.elementAt(i);
                testLists.addAll(executeOrQueue(batchtest.elements(), false));
            }
            testLists.addAll(executeOrQueue(tests.elements(), forkPerTest));
        }

        try {
            Iterator iter = testLists.iterator();
            while (iter.hasNext()) {
                List l = (List) iter.next();
                if (l.size() == 1) {
                    execute((JUnitTest) l.get(0));
                } else {
                    execute(l);
                }
            }
        } finally {
            cleanup();
        }
    }

    /**
     * Run the tests.
     * @param arg one JunitTest
     * @throws BuildException in case of test failures or errors
     */
    protected void execute(JUnitTest arg) throws BuildException {
        validateTestName(arg.getName());

        JUnitTest test = (JUnitTest) arg.clone();
        // set the default values if not specified
        //@todo should be moved to the test class instead.
        if (test.getTodir() == null) {
            test.setTodir(getProject().resolveFile("."));
        }

        if (test.getOutfile() == null) {
            test.setOutfile("TEST-" + test.getName());
        }

        // execute the test and get the return code
        TestResultHolder result = null;
        if (!test.getFork()) {
            result = executeInVM(test);
        } else {
            ExecuteWatchdog watchdog = createWatchdog();
            result = executeAsForked(test, watchdog, null);
            // null watchdog means no timeout, you'd better not check with null
        }

        actOnTestResult(result, test, "Test " + test.getName());
    }

    /**
     * Throws a <code>BuildException</code> if the given test name is invalid.
     * Validity is defined as not <code>null</code>, not empty, and not the
     * string &quot;null&quot;.
     * @param testName the test name to be validated
     * @throws BuildException if <code>testName</code> is not a valid test name
     */
    private void validateTestName(String testName) throws BuildException {
        if (testName == null || testName.length() == 0
            || testName.equals("null")) {
            throw new BuildException("test name must be specified");
        }
    }

    /**
     * Execute a list of tests in a single forked Java VM.
     * @param testList the list of tests to execute.
     * @throws BuildException on error.
     */
    protected void execute(List testList) throws BuildException {
        JUnitTest test = null;
        // Create a temporary file to pass the test cases to run to
        // the runner (one test case per line)
        File casesFile = createTempPropertiesFile("junittestcases");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(casesFile));

            log("Creating casesfile '" + casesFile.getAbsolutePath()
                + "' with content: ", Project.MSG_VERBOSE);
            PrintStream logWriter =
                new PrintStream(new LogOutputStream(this, Project.MSG_VERBOSE));

            Iterator iter = testList.iterator();
            while (iter.hasNext()) {
                test = (JUnitTest) iter.next();
                printDual(writer, logWriter, test.getName());
                if (test.getMethods() != null) {
                    printDual(writer, logWriter, ":" + test.getMethodsString().replace(',', '+'));
                }
                if (test.getTodir() == null) {
                    printDual(writer, logWriter,
                              "," + getProject().resolveFile("."));
                } else {
                    printDual(writer, logWriter, "," + test.getTodir());
                }

                if (test.getOutfile() == null) {
                    printlnDual(writer, logWriter,
                                "," + "TEST-" + test.getName());
                } else {
                    printlnDual(writer, logWriter, "," + test.getOutfile());
                }
            }
            writer.flush();
            writer.close();
            writer = null;

            // execute the test and get the return code
            ExecuteWatchdog watchdog = createWatchdog();
            TestResultHolder result =
                executeAsForked(test, watchdog, casesFile);
            actOnTestResult(result, test, "Tests");
        } catch (IOException e) {
            log(e.toString(), Project.MSG_ERR);
            throw new BuildException(e);
        } finally {
            FileUtils.close(writer);

            try {
                FILE_UTILS.tryHardToDelete(casesFile);
            } catch (Exception e) {
                log(e.toString(), Project.MSG_ERR);
            }
        }
    }

    /**
     * Execute a testcase by forking a new JVM. The command will block
     * until it finishes. To know if the process was destroyed or not
     * or whether the forked Java VM exited abnormally, use the
     * attributes of the returned holder object.
     * @param  test       the testcase to execute.
     * @param  watchdog   the watchdog in charge of cancelling the test if it
     * exceeds a certain amount of time. Can be <tt>null</tt>, in this case
     * the test could probably hang forever.
     * @param casesFile list of test cases to execute. Can be <tt>null</tt>,
     * in this case only one test is executed.
     * @return the test results from the JVM itself.
     * @throws BuildException in case of error creating a temporary property file,
     * or if the junit process can not be forked
     */
    private TestResultHolder executeAsForked(JUnitTest test,
                                             ExecuteWatchdog watchdog,
                                             File casesFile)
        throws BuildException {

        if (perm != null) {
            log("Permissions ignored when running in forked mode!",
                Project.MSG_WARN);
        }

        CommandlineJava cmd;
        try {
            cmd = (CommandlineJava) (getCommandline().clone());
        } catch (CloneNotSupportedException e) {
            throw new BuildException("This shouldn't happen", e, getLocation());
        }
        if (casesFile == null) {
            cmd.createArgument().setValue(test.getName());
            if (test.getMethods() != null) {
                cmd.createArgument().setValue(Constants.METHOD_NAMES + test.getMethodsString());
            }
        } else {
            log("Running multiple tests in the same VM", Project.MSG_VERBOSE);
            cmd.createArgument().setValue(Constants.TESTSFILE + casesFile);
        }

        cmd.createArgument().setValue(Constants.FILTERTRACE + test.getFiltertrace());
        cmd.createArgument().setValue(Constants.HALT_ON_ERROR + test.getHaltonerror());
        cmd.createArgument().setValue(Constants.HALT_ON_FAILURE
                                      + test.getHaltonfailure());
        checkIncludeAntRuntime(cmd);

        checkIncludeSummary(cmd);

        cmd.createArgument().setValue(Constants.SHOWOUTPUT
                                      + String.valueOf(showOutput));
        cmd.createArgument().setValue(Constants.OUTPUT_TO_FORMATTERS
                                      + String.valueOf(outputToFormatters));
        cmd.createArgument().setValue(Constants.LOG_FAILED_TESTS
                                      + String.valueOf(logFailedTests));

        // #31885
        cmd.createArgument().setValue(Constants.LOGTESTLISTENEREVENTS
                                      + String.valueOf(getEnableTestListenerEvents()));

        StringBuffer formatterArg = new StringBuffer(STRING_BUFFER_SIZE);
        final FormatterElement[] feArray = mergeFormatters(test);
        for (int i = 0; i < feArray.length; i++) {
            FormatterElement fe = feArray[i];
            if (fe.shouldUse(this)) {
                formatterArg.append(Constants.FORMATTER);
                formatterArg.append(fe.getClassname());
                File outFile = getOutput(fe, test);
                if (outFile != null) {
                    formatterArg.append(",");
                    formatterArg.append(outFile);
                }
                cmd.createArgument().setValue(formatterArg.toString());
                formatterArg = new StringBuffer();
            }
        }

        File vmWatcher = createTempPropertiesFile("junitvmwatcher");
        cmd.createArgument().setValue(Constants.CRASHFILE
                                      + vmWatcher.getAbsolutePath());
        File propsFile = createTempPropertiesFile("junit");
        cmd.createArgument().setValue(Constants.PROPSFILE
                                      + propsFile.getAbsolutePath());
        Hashtable p = getProject().getProperties();
        Properties props = new Properties();
        for (Enumeration e = p.keys(); e.hasMoreElements();) {
            Object key = e.nextElement();
            props.put(key, p.get(key));
        }
        try {
            FileOutputStream outstream = new FileOutputStream(propsFile);
            props.store(outstream, "Ant JUnitTask generated properties file");
            outstream.close();
        } catch (java.io.IOException e) {
            FILE_UTILS.tryHardToDelete(propsFile);
            throw new BuildException("Error creating temporary properties "
                                     + "file.", e, getLocation());
        }

        Execute execute = new Execute(
            new JUnitLogStreamHandler(
                this,
                Project.MSG_INFO,
                Project.MSG_WARN),
            watchdog);
        execute.setCommandline(cmd.getCommandline());
        execute.setAntRun(getProject());
        if (dir != null) {
            execute.setWorkingDirectory(dir);
        }

        String[] environment = env.getVariables();
        if (environment != null) {
            for (int i = 0; i < environment.length; i++) {
                log("Setting environment variable: " + environment[i],
                    Project.MSG_VERBOSE);
            }
        }
        execute.setNewenvironment(newEnvironment);
        execute.setEnvironment(environment);

        log(cmd.describeCommand(), Project.MSG_VERBOSE);

        checkForkedPath(cmd);

        TestResultHolder result = new TestResultHolder();
        try {
            result.exitCode = execute.execute();
        } catch (IOException e) {
            throw new BuildException("Process fork failed.", e, getLocation());
        } finally {
            String vmCrashString = "unknown";
            BufferedReader br = null;
            try {
                if (vmWatcher.exists()) {
                    br = new BufferedReader(new FileReader(vmWatcher));
                    vmCrashString = br.readLine();
                } else {
                    vmCrashString = "Monitor file ("
                            + vmWatcher.getAbsolutePath()
                            + ") missing, location not writable,"
                            + " testcase not started or mixing ant versions?";
                }
            } catch (Exception e) {
                e.printStackTrace();
                // ignored.
            } finally {
                FileUtils.close(br);
                if (vmWatcher.exists()) {
                    FILE_UTILS.tryHardToDelete(vmWatcher);
                }
            }

            boolean crash = (watchdog != null && watchdog.killedProcess())
                || !Constants.TERMINATED_SUCCESSFULLY.equals(vmCrashString);

            if (casesFile != null && crash) {
                test = createDummyTestForBatchTest(test);
            }

            if (watchdog != null && watchdog.killedProcess()) {
                result.timedOut = true;
                logTimeout(feArray, test, vmCrashString);
            } else if (crash) {
                result.crashed = true;
                logVmCrash(feArray, test, vmCrashString);
            }

            if (!FILE_UTILS.tryHardToDelete(propsFile)) {
                throw new BuildException("Could not delete temporary "
                                         + "properties file '"
                                         + propsFile.getAbsolutePath() + "'.");
            }
        }

        return result;
    }

    /**
     * Adding ant runtime.
     * @param cmd command to run
     */
    private void checkIncludeAntRuntime(CommandlineJava cmd) {
        if (includeAntRuntime) {
            Map/*<String, String>*/ env = Execute.getEnvironmentVariables();
            String cp = (String) env.get(CLASSPATH);
            if (cp != null) {
                cmd.createClasspath(getProject()).createPath()
                    .append(new Path(getProject(), cp));
            }
            log("Implicitly adding " + antRuntimeClasses + " to CLASSPATH",
                Project.MSG_VERBOSE);
            cmd.createClasspath(getProject()).createPath()
                .append(antRuntimeClasses);
        }
    }


    /**
     * check for the parameter being "withoutanderr" in a locale-independent way.
     * @param summaryOption the summary option -can be null
     * @return true if the run should be withoutput and error
     */
    private boolean equalsWithOutAndErr(String summaryOption) {
        return "withoutanderr".equalsIgnoreCase(summaryOption);
    }

    private void checkIncludeSummary(CommandlineJava cmd) {
        if (summary) {
            String prefix = "";
            if (equalsWithOutAndErr(summaryValue)) {
                prefix = "OutErr";
            }
            cmd.createArgument()
                .setValue(Constants.FORMATTER
                          + "org.apache.tools.ant.taskdefs.optional.junit."
                          + prefix + "SummaryJUnitResultFormatter");
        }
    }

    /**
     * Check the path for multiple different versions of
     * ant.
     * @param cmd command to execute
     */
    private void checkForkedPath(CommandlineJava cmd) {
        if (forkedPathChecked) {
            return;
        }
        forkedPathChecked = true;
        if (!cmd.haveClasspath()) {
            return;
        }
        AntClassLoader loader = null;
        try {
            loader =
                AntClassLoader.newAntClassLoader(null, getProject(),
                                                 cmd.createClasspath(getProject()),
                                                 true);
            String projectResourceName =
                LoaderUtils.classNameToResource(Project.class.getName());
            URL previous = null;
            try {
                for (Enumeration e = loader.getResources(projectResourceName);
                     e.hasMoreElements();) {
                    URL current = (URL) e.nextElement();
                    if (previous != null && !urlEquals(current, previous)) {
                        log("WARNING: multiple versions of ant detected "
                            + "in path for junit "
                            + LINE_SEP + "         " + previous
                            + LINE_SEP + "     and " + current,
                            Project.MSG_WARN);
                        return;
                    }
                    previous = current;
                }
            } catch (Exception ex) {
                // Ignore exception
            }
        } finally {
            if (loader != null) {
                loader.cleanup();
            }
        }
    }

    /**
     * Compares URLs for equality but takes case-sensitivity into
     * account when comparing file URLs and ignores the jar specific
     * part of the URL if present.
     */
    private static boolean urlEquals(URL u1, URL u2) {
        String url1 = maybeStripJarAndClass(u1);
        String url2 = maybeStripJarAndClass(u2);
        if (url1.startsWith("file:") && url2.startsWith("file:")) {
            return new File(FILE_UTILS.fromURI(url1))
                .equals(new File(FILE_UTILS.fromURI(url2)));
        }
        return url1.equals(url2);
    }

    private static String maybeStripJarAndClass(URL u) {
        String s = u.toString();
        if (s.startsWith("jar:")) {
            int pling = s.indexOf('!');
            s = s.substring(4, pling == -1 ? s.length() : pling);
        }
        return s;
    }

    /**
     * Create a temporary file to pass the properties to a new process.
     * Will auto-delete on (graceful) exit.
     * The file will be in the project basedir unless tmpDir declares
     * something else.
     * @param prefix
     * @return created file
     */
    private File createTempPropertiesFile(String prefix) {
        File propsFile =
            FILE_UTILS.createTempFile(prefix, ".properties",
                tmpDir != null ? tmpDir : getProject().getBaseDir(), true, true);
        return propsFile;
    }


    /**
     * Pass output sent to System.out to the TestRunner so it can
     * collect it for the formatters.
     *
     * @param output output coming from System.out
     * @since Ant 1.5
     */
    protected void handleOutput(String output) {
        if (output.startsWith(TESTLISTENER_PREFIX)) {
            log(output, Project.MSG_VERBOSE);
        } else if (runner != null) {
            if (outputToFormatters) {
                runner.handleOutput(output);
            }
            if (showOutput) {
                super.handleOutput(output);
            }
        } else {
            super.handleOutput(output);
        }
    }

    /**
     * Handle an input request by this task.
     * @see Task#handleInput(byte[], int, int)
     * This implementation delegates to a runner if it
     * present.
     * @param buffer the buffer into which data is to be read.
     * @param offset the offset into the buffer at which data is stored.
     * @param length the amount of data to read.
     *
     * @return the number of bytes read.
     * @exception IOException if the data cannot be read.
     *
     * @since Ant 1.6
     */
    protected int handleInput(byte[] buffer, int offset, int length)
        throws IOException {
        if (runner != null) {
            return runner.handleInput(buffer, offset, length);
        } else {
            return super.handleInput(buffer, offset, length);
        }
    }


    /**
     * Pass output sent to System.out to the TestRunner so it can
     * collect ot for the formatters.
     *
     * @param output output coming from System.out
     * @since Ant 1.5.2
     */
    protected void handleFlush(String output) {
        if (runner != null) {
            runner.handleFlush(output);
            if (showOutput) {
                super.handleFlush(output);
            }
        } else {
            super.handleFlush(output);
        }
    }

    /**
     * Pass output sent to System.err to the TestRunner so it can
     * collect it for the formatters.
     *
     * @param output output coming from System.err
     * @since Ant 1.5
     */
    public void handleErrorOutput(String output) {
        if (runner != null) {
            runner.handleErrorOutput(output);
            if (showOutput) {
                super.handleErrorOutput(output);
            }
        } else {
            super.handleErrorOutput(output);
        }
    }


    /**
     * Pass output sent to System.err to the TestRunner so it can
     * collect it for the formatters.
     *
     * @param output coming from System.err
     * @since Ant 1.5.2
     */
    public void handleErrorFlush(String output) {
        if (runner != null) {
            runner.handleErrorFlush(output);
            if (showOutput) {
                super.handleErrorFlush(output);
            }
        } else {
            super.handleErrorFlush(output);
        }
    }

    // in VM is not very nice since it could probably hang the
    // whole build. IMHO this method should be avoided and it would be best
    // to remove it in future versions. TBD. (SBa)

    /**
     * Execute inside VM.
     * @param arg one JUnitTest
     * @throws BuildException under unspecified circumstances
     * @return the results
     */
    private TestResultHolder executeInVM(JUnitTest arg) throws BuildException {
        if (delegate == null) {
            setupJUnitDelegate();
        }

        JUnitTest test = (JUnitTest) arg.clone();
        test.setProperties(getProject().getProperties());
        if (dir != null) {
            log("dir attribute ignored if running in the same VM",
                Project.MSG_WARN);
        }

        if (newEnvironment || null != env.getVariables()) {
            log("Changes to environment variables are ignored if running in "
                + "the same VM.", Project.MSG_WARN);
        }

        if (getCommandline().getBootclasspath() != null) {
            log("bootclasspath is ignored if running in the same VM.",
                Project.MSG_WARN);
        }

        CommandlineJava.SysProperties sysProperties =
                getCommandline().getSystemProperties();
        if (sysProperties != null) {
            sysProperties.setSystem();
        }

        try {
            log("Using System properties " + System.getProperties(),
                Project.MSG_VERBOSE);
            if (splitJunit) {
                classLoader = (AntClassLoader) delegate.getClass().getClassLoader();
            } else {
                createClassLoader();
            }
            if (classLoader != null) {
                classLoader.setThreadContextLoader();
            }
            runner = delegate.newJUnitTestRunner(test, test.getMethods(), test.getHaltonerror(),
                                         test.getFiltertrace(),
                                         test.getHaltonfailure(), false,
                                         getEnableTestListenerEvents(),
                                         classLoader);
            if (summary) {

                JUnitTaskMirror.SummaryJUnitResultFormatterMirror f =
                    delegate.newSummaryJUnitResultFormatter();
                f.setWithOutAndErr(equalsWithOutAndErr(summaryValue));
                f.setOutput(getDefaultOutput());
                if(!mutationAnalysis) runner.addFormatter(f);
            }

            runner.setPermissions(perm);

            final FormatterElement[] feArray = mergeFormatters(test);
            for (int i = 0; i < feArray.length; i++) {
                FormatterElement fe = feArray[i];
                if (fe.shouldUse(this)) {
                    File outFile = getOutput(fe, test);
                    if (outFile != null) {
                        fe.setOutfile(outFile);
                    } else {
                        fe.setOutput(getDefaultOutput());
                    }
                    if(!mutationAnalysis) runner.addFormatter(fe.createFormatter(classLoader));
                }
            }

            TestResultHolder result = new TestResultHolder();

            /*** MAJOR BEGIN ***/
            if (mutationAnalysis){
                // Add own formatter to gather runtime and mutation analysis statistics
                runner.addFormatter(major);

                if(true/*__M_NO!=0*/){
                    try {
                        Class config = classLoader.forceLoadSystemClass(NAME_IDENT_CLASS);
                        config.getField(NAME_IDENT).setInt(null, __M_NO);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }
                }

                if(timeout !=null && timeout.intValue()>0){
                    Thread currentTest = exportKillMap ? new TestRunnerThreadMap(__M_NO, test, runner, result) : new TestRunnerThread(__M_NO, runner, result);
                    currentTest.start();
                    try {
                        currentTest.join(timeout.intValue());
                    } catch (InterruptedException ignore) {
                        printStackTrace(ignore);
                    }

                    if (currentTest.isAlive()) {
                            currentTest.stop();
                            // We do not wait for the current thread to end!
                            //try {
                                //currentTest.join();
                            //} catch (InterruptedException e) {
                                //printStackTrace(e);
                            //}
                    }
                    currentTest = null;
                }else{
                    // set current time to estimate init time for first test case
                    major.stopSuite=System.currentTimeMillis();
                    runner.run();
                    result.exitCode = runner.getRetCode();
                }
            }
            /*** MAJOR END ***/
            else{
                runner.run();
                result.exitCode = runner.getRetCode();
            }
            return result;
        } finally {
            if (sysProperties != null) {
                sysProperties.restoreSystem();
            }
            if (classLoader != null) {
                classLoader.resetThreadContextLoader();
            }
        }
    }

    /**
     * @return <tt>null</tt> if there is a timeout value, otherwise the
     * watchdog instance.
     *
     * @throws BuildException under unspecified circumstances
     * @since Ant 1.2
     */
    protected ExecuteWatchdog createWatchdog() throws BuildException {
        if (timeout == null) {
            return null;
        }
        return new ExecuteWatchdog((long) timeout.intValue());
    }

    /**
     * Get the default output for a formatter.
     *
     * @return default output stream for a formatter
     * @since Ant 1.3
     */
    protected OutputStream getDefaultOutput() {
        return new LogOutputStream(this, Project.MSG_INFO);
    }

    /**
     * Merge all individual tests from the batchtest with all individual tests
     * and return an enumeration over all <tt>JUnitTest</tt>.
     *
     * @return enumeration over individual tests
     * @since Ant 1.3
     */
    protected Enumeration getIndividualTests() {
        if(mutationAnalysis && testsSorted){
            return Collections.enumeration(currentTest);
        }

        final int count = batchTests.size();
        final Enumeration[] enums = new Enumeration[ count + 1];
        for (int i = 0; i < count; i++) {
            BatchTest batchtest = (BatchTest) batchTests.elementAt(i);
            enums[i] = batchtest.elements();
        }
        enums[enums.length - 1] = tests.elements();
        return Enumerations.fromCompound(enums);
    }

    /**
     * Verifies all <code>test</code> elements having the <code>methods</code>
     * attribute specified and having the <code>if</code>-condition resolved
     * to true, that the value of the <code>methods</code> attribute is valid.
     * @exception BuildException if some of the tests matching the described
     *                           conditions has invalid value of the
     *                           <code>methods</code> attribute
     * @since 1.8.2
     */
    private void checkMethodLists() throws BuildException {
        if (tests.isEmpty()) {
            return;
        }

        Enumeration testsEnum = tests.elements();
        while (testsEnum.hasMoreElements()) {
            JUnitTest test = (JUnitTest) testsEnum.nextElement();
            if (test.hasMethodsSpecified() && test.shouldRun(getProject())) {
                test.resolveMethods();
            }
        }
    }

    /**
     * return an enumeration listing each test, then each batchtest
     * @return enumeration
     * @since Ant 1.3
     */
    protected Enumeration allTests() {
        Enumeration[] enums = {tests.elements(), batchTests.elements()};
        return Enumerations.fromCompound(enums);
    }

    /**
     * @param test junit test
     * @return array of FormatterElement
     * @since Ant 1.3
     */
    private FormatterElement[] mergeFormatters(JUnitTest test) {
        Vector feVector = (Vector) formatters.clone();
        test.addFormattersTo(feVector);
        FormatterElement[] feArray = new FormatterElement[feVector.size()];
        feVector.copyInto(feArray);
        return feArray;
    }

    /**
     * If the formatter sends output to a file, return that file.
     * null otherwise.
     * @param fe  formatter element
     * @param test one JUnit test
     * @return file reference
     * @since Ant 1.3
     */
    protected File getOutput(FormatterElement fe, JUnitTest test) {
        if (fe.getUseFile()) {
            String base = test.getOutfile();
            if (base == null) {
                base = JUnitTaskMirror.JUnitTestRunnerMirror.IGNORED_FILE_NAME;
            }
            String filename = base + fe.getExtension();
            File destFile = new File(test.getTodir(), filename);
            String absFilename = destFile.getAbsolutePath();
            return getProject().resolveFile(absFilename);
        }
        return null;
    }

    /**
     * Search for the given resource and add the directory or archive
     * that contains it to the classpath.
     *
     * <p>Doesn't work for archives in JDK 1.1 as the URL returned by
     * getResource doesn't contain the name of the archive.</p>
     *
     * @param resource resource that one wants to lookup
     * @since Ant 1.4
     */
    protected void addClasspathEntry(String resource) {
        addClasspathResource(resource);
    }

    /**
     * Implementation of addClasspathEntry.
     *
     * @param resource resource that one wants to lookup
     * @return true if something was in fact added
     * @since Ant 1.7.1
     */
    private boolean addClasspathResource(String resource) {
        /*
         * pre Ant 1.6 this method used to call getClass().getResource
         * while Ant 1.6 will call ClassLoader.getResource().
         *
         * The difference is that Class.getResource expects a leading
         * slash for "absolute" resources and will strip it before
         * delegating to ClassLoader.getResource - so we now have to
         * emulate Class's behavior.
         */
        if (resource.startsWith("/")) {
            resource = resource.substring(1);
        } else {
            resource = "org/apache/tools/ant/taskdefs/optional/junit/"
                + resource;
        }

        File f = LoaderUtils.getResourceSource(getClass().getClassLoader(),
                                               resource);
        if (f != null) {
            log("Found " + f.getAbsolutePath(), Project.MSG_DEBUG);
            antRuntimeClasses.createPath().setLocation(f);
            return true;
        } else {
            log("Couldn\'t find " + resource, Project.MSG_DEBUG);
            return false;
        }
    }

    static final String TIMEOUT_MESSAGE =
        "Timeout occurred. Please note the time in the report does"
        + " not reflect the time until the timeout.";

    /**
     * Take care that some output is produced in report files if the
     * watchdog kills the test.
     *
     * @since Ant 1.5.2
     */
    private void logTimeout(FormatterElement[] feArray, JUnitTest test,
                            String testCase) {
        logVmExit(feArray, test, TIMEOUT_MESSAGE, testCase);
    }

    /**
     * Take care that some output is produced in report files if the
     * forked machine exited before the test suite finished but the
     * reason is not a timeout.
     *
     * @since Ant 1.7
     */
    private void logVmCrash(FormatterElement[] feArray, JUnitTest test, String testCase) {
        logVmExit(
            feArray, test,
            "Forked Java VM exited abnormally. Please note the time in the report"
            + " does not reflect the time until the VM exit.",
            testCase);
    }

    /**
     * Take care that some output is produced in report files if the
     * forked machine terminated before the test suite finished
     *
     * @since Ant 1.7
     */
    private void logVmExit(FormatterElement[] feArray, JUnitTest test,
                           String message, String testCase) {
        if (delegate == null) {
            setupJUnitDelegate();
        }

        try {
            log("Using System properties " + System.getProperties(),
                Project.MSG_VERBOSE);
            if (splitJunit) {
                classLoader = (AntClassLoader) delegate.getClass().getClassLoader();
            } else {
                createClassLoader();
            }
            if (classLoader != null) {
                classLoader.setThreadContextLoader();
            }

            test.setCounts(1, 0, 1);
            test.setProperties(getProject().getProperties());
            for (int i = 0; i < feArray.length; i++) {
                FormatterElement fe = feArray[i];
                if (fe.shouldUse(this)) {
                    JUnitTaskMirror.JUnitResultFormatterMirror formatter =
                        fe.createFormatter(classLoader);
                    if (formatter != null) {
                        OutputStream out = null;
                        File outFile = getOutput(fe, test);
                        if (outFile != null) {
                            try {
                                out = new FileOutputStream(outFile);
                            } catch (IOException e) {
                                // ignore
                            }
                        }
                        if (out == null) {
                            out = getDefaultOutput();
                        }
                        delegate.addVmExit(test, formatter, out, message,
                                           testCase);
                    }
                }
            }
            if (summary) {
                JUnitTaskMirror.SummaryJUnitResultFormatterMirror f =
                    delegate.newSummaryJUnitResultFormatter();
                f.setWithOutAndErr(equalsWithOutAndErr(summaryValue));
                delegate.addVmExit(test, f, getDefaultOutput(), message, testCase);
            }
        } finally {
            if (classLoader != null) {
                classLoader.resetThreadContextLoader();
            }
        }
    }

    /**
     * Creates and configures an AntClassLoader instance from the
     * nested classpath element.
     *
     * @since Ant 1.6
     */
    private void createClassLoader() {
        Path userClasspath = getCommandline().getClasspath();
        if (userClasspath != null) {
            if (reloading || classLoader == null) {
                deleteClassLoader();
                Path classpath = (Path) userClasspath.clone();
                if (includeAntRuntime) {
                    log("Implicitly adding " + antRuntimeClasses
                        + " to CLASSPATH", Project.MSG_VERBOSE);
                    classpath.append(antRuntimeClasses);
                }
                classLoader = getProject().createClassLoader(classpath);
                if (getClass().getClassLoader() != null
                    && getClass().getClassLoader() != Project.class.getClassLoader()) {
                    classLoader.setParent(getClass().getClassLoader());
                }
                if (mutationAnalysis) {
                    classLoader.setParentFirst(false);
                } else {
                    classLoader.setParentFirst(true);
                }
                classLoader.addJavaLibraries();
                log("Using CLASSPATH " + classLoader.getClasspath(),
                    Project.MSG_VERBOSE);
                // make sure the test will be accepted as a TestCase
                classLoader.addSystemPackageRoot("junit");
                // make sure the test annotation are accepted
                classLoader.addSystemPackageRoot("org.junit");
                classLoader.addSystemPackageRoot("org.hamcrest");
                // will cause trouble in JDK 1.1 if omitted
                classLoader.addSystemPackageRoot("org.apache.tools.ant");
            }
        }
    }

    /**
     * Removes resources.
     *
     * <p>Is invoked in {@link #execute execute}.  Subclasses that
     * don't invoke execute should invoke this method in a finally
     * block.</p>
     *
     * @since Ant 1.7.1
     */
    protected void cleanup() {
        deleteClassLoader();
        delegate = null;
    }

    /**
     * Removes a classloader if needed.
     * @since Ant 1.7
     */
    private void deleteClassLoader() {
        if (classLoader != null) {
            classLoader.cleanup();
            classLoader = null;
        }
        if (mirrorLoader instanceof SplitClassLoader) {
            ((SplitClassLoader) mirrorLoader).cleanup();
        }
        mirrorLoader = null;
    }

    /**
     * Get the command line used to run the tests.
     * @return the command line.
     * @since Ant 1.6.2
     */
    protected CommandlineJava getCommandline() {
        if (commandline == null) {
            commandline = new CommandlineJava();
            commandline.setClassname("org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunner");
        }
        return commandline;
    }

    /**
     * Forked test support
     * @since Ant 1.6.2
     */
    private static final class ForkedTestConfiguration {
        private boolean filterTrace;
        private boolean haltOnError;
        private boolean haltOnFailure;
        private String errorProperty;
        private String failureProperty;

        /**
         * constructor for forked test configuration
         * @param filterTrace
         * @param haltOnError
         * @param haltOnFailure
         * @param errorProperty
         * @param failureProperty
         */
        ForkedTestConfiguration(boolean filterTrace, boolean haltOnError,
                                boolean haltOnFailure, String errorProperty,
                                String failureProperty) {
            this.filterTrace = filterTrace;
            this.haltOnError = haltOnError;
            this.haltOnFailure = haltOnFailure;
            this.errorProperty = errorProperty;
            this.failureProperty = failureProperty;
        }

        /**
         * configure from a test; sets member variables to attributes of the test
         * @param test
         */
        ForkedTestConfiguration(JUnitTest test) {
            this(test.getFiltertrace(),
                    test.getHaltonerror(),
                    test.getHaltonfailure(),
                    test.getErrorProperty(),
                    test.getFailureProperty());
        }

        /**
         * equality test checks all the member variables
         * @param other
         * @return true if everything is equal
         */
        public boolean equals(Object other) {
            if (other == null
                || other.getClass() != ForkedTestConfiguration.class) {
                return false;
            }
            ForkedTestConfiguration o = (ForkedTestConfiguration) other;
            return filterTrace == o.filterTrace
                && haltOnError == o.haltOnError
                && haltOnFailure == o.haltOnFailure
                && ((errorProperty == null && o.errorProperty == null)
                    ||
                    (errorProperty != null
                     && errorProperty.equals(o.errorProperty)))
                && ((failureProperty == null && o.failureProperty == null)
                    ||
                    (failureProperty != null
                     && failureProperty.equals(o.failureProperty)));
        }

        /**
         * hashcode is based only on the boolean members, and returns a value
         * in the range 0-7.
         * @return hash code value
         */
        public int hashCode() {
            // CheckStyle:MagicNumber OFF
            return (filterTrace ? 1 : 0)
                + (haltOnError ? 2 : 0)
                + (haltOnFailure ? 4 : 0);
            // CheckStyle:MagicNumber ON
        }
    }

    /**
     * These are the different forking options
     * @since 1.6.2
     */
    public static final class ForkMode extends EnumeratedAttribute {

        /**
         * fork once only
         */
        public static final String ONCE = "once";
        /**
         * fork once per test class
         */
        public static final String PER_TEST = "perTest";
        /**
         * fork once per batch of tests
         */
        public static final String PER_BATCH = "perBatch";

        /** No arg constructor. */
        public ForkMode() {
            super();
        }

        /**
         * Constructor using a value.
         * @param value the value to use - once, perTest or perBatch.
         */
        public ForkMode(String value) {
            super();
            setValue(value);
        }

        /** {@inheritDoc}. */
        public String[] getValues() {
            return new String[] {ONCE, PER_TEST, PER_BATCH};
        }
    }

    /**
     * Executes all tests that don't need to be forked (or all tests
     * if the runIndividual argument is true.  Returns a collection of
     * lists of tests that share the same VM configuration and haven't
     * been executed yet.
     * @param testList the list of tests to be executed or queued.
     * @param runIndividual if true execute each test individually.
     * @return a list of tasks to be executed.
     * @since 1.6.2
     */
    protected Collection executeOrQueue(Enumeration testList,
                                        boolean runIndividual) {
        Map testConfigurations = new HashMap();
        while (testList.hasMoreElements()) {
            JUnitTest test = (JUnitTest) testList.nextElement();
            if (test.shouldRun(getProject())) {
                if (runIndividual || !test.getFork()) {
                    execute(test);
                } else {
                    ForkedTestConfiguration c =
                        new ForkedTestConfiguration(test);
                    List l = (List) testConfigurations.get(c);
                    if (l == null) {
                        l = new ArrayList();
                        testConfigurations.put(c, l);
                    }
                    l.add(test);
                }
            }
        }
        return testConfigurations.values();
    }

    /**
     * Logs information about failed tests, potentially stops
     * processing (by throwing a BuildException) if a failure/error
     * occurred or sets a property.
     * @param exitValue the exitValue of the test.
     * @param wasKilled if true, the test had been killed.
     * @param test      the test in question.
     * @param name      the name of the test.
     * @since Ant 1.6.2
     */
    protected void actOnTestResult(int exitValue, boolean wasKilled,
                                   JUnitTest test, String name) {
        TestResultHolder t = new TestResultHolder();
        t.exitCode = exitValue;
        t.timedOut = wasKilled;
        actOnTestResult(t, test, name);
    }

    /**
     * Logs information about failed tests, potentially stops
     * processing (by throwing a BuildException) if a failure/error
     * occurred or sets a property.
     * @param result    the result of the test.
     * @param test      the test in question.
     * @param name      the name of the test.
     * @since Ant 1.7
     */
    protected void actOnTestResult(TestResultHolder result, JUnitTest test,
                                   String name) {
        // we do not act on results during mutation analysis
        if(__M_NO>0) return;

        // if there is an error/failure and that it should halt, stop
        // everything otherwise just log a statement
        boolean fatal = result.timedOut || result.crashed;
        boolean errorOccurredHere =
            result.exitCode == JUnitTaskMirror.JUnitTestRunnerMirror.ERRORS || fatal;
        boolean failureOccurredHere =
            result.exitCode != JUnitTaskMirror.JUnitTestRunnerMirror.SUCCESS || fatal;
        if (errorOccurredHere || failureOccurredHere) {
            if ((errorOccurredHere && test.getHaltonerror())
                || (failureOccurredHere && test.getHaltonfailure())) {
                throw new BuildException(name + " failed"
                    + (result.timedOut ? " (timeout)" : "")
                    + (result.crashed ? " (crashed)" : ""), getLocation());
            } else {
                if (logFailedTests) {
                    log(name + " FAILED"
                        + (result.timedOut ? " (timeout)" : "")
                        + (result.crashed ? " (crashed)" : ""),
                        Project.MSG_ERR);
                }
                if (errorOccurredHere && test.getErrorProperty() != null) {
                    getProject().setNewProperty(test.getErrorProperty(), "true");
                }
                if (failureOccurredHere && test.getFailureProperty() != null) {
                    getProject().setNewProperty(test.getFailureProperty(), "true");
                }
            }
        }
    }

    /**
     * A value class that contains the result of a test.
     */
    protected static class TestResultHolder {
        // CheckStyle:VisibilityModifier OFF - bc
        /** the exit code of the test. */
        public int exitCode = JUnitTaskMirror.JUnitTestRunnerMirror.ERRORS;
        /** true if the test timed out */
        public boolean timedOut = false;
        /** true if the test crashed */
        public boolean crashed = false;
        // CheckStyle:VisibilityModifier ON
    }

    /**
     * A stream handler for handling the junit task.
     * @since Ant 1.7
     */
    protected static class JUnitLogOutputStream extends LogOutputStream {
        private Task task; // local copy since LogOutputStream.task is private

        /**
         * Constructor.
         * @param task the task being logged.
         * @param level the log level used to log data written to this stream.
         */
        public JUnitLogOutputStream(Task task, int level) {
            super(task, level);
            this.task = task;
        }

        /**
         * Logs a line.
         * If the line starts with junit.framework.TestListener: set the level
         * to MSG_VERBOSE.
         * @param line the line to log.
         * @param level the logging level to use.
         */
        protected void processLine(String line, int level) {
            if (line.startsWith(TESTLISTENER_PREFIX)) {
                task.log(line, Project.MSG_VERBOSE);
            } else {
                super.processLine(line, level);
            }
        }
    }

    /**
     * A log stream handler for junit.
     * @since Ant 1.7
     */
    protected static class JUnitLogStreamHandler extends PumpStreamHandler {
        /**
         * Constructor.
         * @param task the task to log.
         * @param outlevel the level to use for standard output.
         * @param errlevel the level to use for error output.
         */
        public JUnitLogStreamHandler(Task task, int outlevel, int errlevel) {
            super(new JUnitLogOutputStream(task, outlevel),
                  new LogOutputStream(task, errlevel));
        }
    }

    static final String NAME_OF_DUMMY_TEST = "Batch-With-Multiple-Tests";

    /**
     * Creates a JUnitTest instance that shares all flags with the
     * passed in instance but has a more meaningful name.
     *
     * <p>If a VM running multiple tests crashes, we don't know which
     * test failed.  Prior to Ant 1.8.0 Ant would log the error with
     * the last test of the batch test, which caused some confusion
     * since the log might look as if a test had been executed last
     * that was never started.  With Ant 1.8.0 the test's name will
     * indicate that something went wrong with a test inside the batch
     * without giving it a real name.</p>
     *
     * @see https://issues.apache.org/bugzilla/show_bug.cgi?id=45227
     */
    private static JUnitTest createDummyTestForBatchTest(JUnitTest test) {
        JUnitTest t = (JUnitTest) test.clone();
        int index = test.getName().lastIndexOf('.');
        // make sure test looks as if it was in the same "package" as
        // the last test of the batch
        String pack = index > 0 ? test.getName().substring(0, index + 1) : "";
        t.setName(pack + NAME_OF_DUMMY_TEST);
        return t;
    }

    private static void printDual(BufferedWriter w, PrintStream s, String text)
        throws IOException {
        w.write(String.valueOf(text));
        s.print(text);
    }

    private static void printlnDual(BufferedWriter w, PrintStream s, String text)
        throws IOException {
        w.write(String.valueOf(text));
        w.newLine();
        s.println(text);
    }

    // References for driver class and corresponding methods
    Class configClass;
    Method configReset;
    Method configCoverageList;
    Method configKilledList;

    private final String NAME_IDENT_CLASS = "major.mutation.Config";
    private final String NAME_IDENT       = "__M_NO";
    private final String NAME_COV_LIST    = "getCoverageList";
    private final String NAME_RESET       = "reset";

    /**
     * Filenames for csv output
     */
    // mapping which test covers which mutants
    private String FILE_MUT_COV_MAP = "covMap.csv";
    // mapping which test kills which mutants
    private String FILE_MUT_KILL_MAP = "killMap.csv";
    // mapping test number to test name
    private String FILE_TEST_NAME_MAP = "testMap.csv";
    // summary of the mutation analysis
    private String FILE_SUMMARY = "summary.csv";
    // statistics of the entire mutation analysis
    private String FILE_RESULTS = null;
    // mapping mutant number to kill reason
    private String FILE_KILL_DETAILS = null;
    // list of mutants to exclude from analysis
    private String FILE_EXCLUDE_MUTANTS=null;
    // list of mutants to include in analysis
    private String FILE_INCLUDE_MUTANTS=null;
    // list of presorted tests
    private String FILE_PRESORTED_TESTS=null;
    // Major's log file for mutant generation
    private String FILE_MUT_LOG = "mutants.log";

    private boolean colorTerm = hasColorSupport();

    // Flag that generally indicates whether mutation anaylsis should be processed
    private boolean mutationAnalysis = false;
    private int __M_NO=0;

    private int genMutants;
    private int covMutants;

    // Flags configurable within Ant
    private boolean coverage = true;
    private boolean debug = false;
    private int threshold=50;
    private int timeoutFactor=8;
    private int timeoutOffset=0;

    private boolean exportCovMap     = false;
    private boolean coverageOnly     = false;
    private boolean exportKillMap    = false;
    private boolean sortMethods      = false;
    private boolean exportStackTrace = false;

    private boolean excludeFailing = true;

    private Set<Integer> includeSet = new TreeSet<Integer>();
    private Set<Integer> excludeSet = new TreeSet<Integer>();

    // Name of the propery that holds the real runtime of a test case
    private static final String RUNTIME_COV="major.runtime.cov";
    private static final String RUNTIME_ORIG="major.runtime.orig";
    private static final String TEST_COUNTER="major.test.counter";

    private static final String SEPARATOR = "------------------------------------------------------------";

    public enum Sorting{
        sort_classes, sort_methods, sort_hybrid_methods, sort_hybrid_classes, random, original, orig_classes;
    }

    private Sorting sort = Sorting.original;

    private MajorResultFormatterTestSuite major;
    private boolean testsSorted = false;
    private boolean getCoverage = true;

    // Set of ordered tests using an own comparator
    private TreeSet<JUnitTest> sortedTests = new TreeSet<JUnitTest>(new MajorComparator<JUnitTest>());
    private TreeSet<JUnitTest> sortedTestsReal = new TreeSet<JUnitTest>(new MajorComparator<JUnitTest>());

    // Set of failing tests -> exclude for mutation analysis
    private Set<String> failingTestClasses = new TreeSet<String>();
    private Set<String> failingTestMethods = new TreeSet<String>();

    // Collection for shuffeled tests (only used if useRandom is set to true)
    private Collection<JUnitTest> randomTests;
    // Reference to the test (suite) which is to be executed
    private Collection<JUnitTest> currentTest = sortedTests;
    // Mapping tests to covered mutants
    private Map<String,List<Integer>> mutationCoverageMap = new HashMap<String,List<Integer>>(8000);
    private Map<String,List<MutantKillResult>> mutationKillMap = new HashMap<String,List<MutantKillResult>>(8000);

    // Map for presorted tests
    private Map<String,Integer> presortMap;


    // Set of all mutants
    private Set<Integer> allMutants = new TreeSet<Integer>();

    // Set of all killed mutants - fail
    private Set<Integer> killedMutants = new HashSet<Integer>(64000);
    // Set of all killed mutants - timeout
    private Set<Integer> timeoutMutants = new TreeSet<Integer>();
    // Set of all killed mutants - exception
    private Set<Integer> exceptionMutants = new TreeSet<Integer>();

    // counters for mutation analysis
    private int failure=0;
    private int err_exception=0;
    private int err_timeout=0;

    private long test_time=0;

    /**
     * Wrapper for Mutation Analysis to avoid recursion
     */
    public void execute() throws BuildException {
        if(!mutationAnalysis) {
            // run original test suite
            execute1();
            return;
        }
        // Indicate that mutation analysis is enabled
        logMajor("Mutation analysis enabled"+(debug?" (debug mode)":""));


        // Check whether any test in the test suite is configured with the fork option
        Enumeration<BaseTest> en = getIndividualTests();
        while(en.hasMoreElements()){
            if(en.nextElement().getFork()){
                logMajorError("Forking a JVM (fork=true) is currently not supported!");
                logMajor("Please adapt build.xml and rerun tests.");
                return;
            }
        }

        try{
            // Initialize driver and read mutants log
            configClass = getClass().getClassLoader().loadClass(NAME_IDENT_CLASS);
            configReset = configClass.getMethod(NAME_RESET, null);
            configCoverageList = configClass.getMethod(NAME_COV_LIST, null);

            if(new File(FILE_MUT_LOG).exists()) {
                BufferedReader br = new BufferedReader(new FileReader(FILE_MUT_LOG));
                while (br.readLine()!=null) ++genMutants;
                br.close();
            } else {
                logMajorError("Could not read mutants log file!");
                logMajor("Please adapt build.xml and set the property \"mutantsLogFile\".");
                return;
            }

            // Init include list if necessary
            if(FILE_INCLUDE_MUTANTS!=null && new File(FILE_INCLUDE_MUTANTS).exists()){
                String str;
                BufferedReader br = new BufferedReader(new FileReader(FILE_INCLUDE_MUTANTS));
                while((str=br.readLine())!=null){
                    if(str.matches("\\s*#.*")) continue;
                    if(str.matches("\\s*")) continue;
                    includeSet.add(Integer.parseInt(str));
                }
                br.close();
            }

            // Init exclude list if necessary
            if(FILE_EXCLUDE_MUTANTS!=null && new File(FILE_EXCLUDE_MUTANTS).exists()){
                String str;
                BufferedReader br = new BufferedReader(new FileReader(FILE_EXCLUDE_MUTANTS));
                while((str=br.readLine())!=null){
                    if(str.matches("\\s*#.*")) continue;
                    if(str.matches("\\s*")) continue;
                    excludeSet.add(Integer.parseInt(str));
                }
                br.close();
            }

            // Init presorted tests if necessary
            if(FILE_PRESORTED_TESTS!=null && new File(FILE_PRESORTED_TESTS).exists()){
                String str;
                BufferedReader br = new BufferedReader(new FileReader(FILE_PRESORTED_TESTS));
                presortMap = new HashMap<String,Integer>(1000);
                int counter=0;
                while((str=br.readLine())!=null){
                    if(str.matches("\\s*#.*")) continue;
                    if(str.matches("\\s*")) continue;
                    presortMap.put(str, counter++);
                }
                br.close();
            }
        }catch(Exception e){
            // Report all Exceptions as RuntimeException to kill the whole process
            throw new RuntimeException("Unexpected error during initialization", e);
        }

        long preProcessingStart = System.currentTimeMillis();

        // run the original version but do not record mutation coverage

        __M_NO=-1;
        getCoverage = false;
        // we do not assume that the SUT is defect free! Failing tests will be excluded from the analysis!
        //haltOnError=true;
        //haltOnFail=true;

        // the result formatter that records the runtime
        major = (sort==Sorting.sort_methods) ? new MajorResultFormatterTestMethods() :
                (sort==Sorting.sort_hybrid_methods || sort==Sorting.sort_hybrid_classes) ? new MajorResultFormatterHybrid() :
                (sort==Sorting.sort_classes || sort==Sorting.orig_classes) ? new MajorResultFormatterTestClasses() :new MajorResultFormatterTestSuite();

        // execute original test suite and order tests according to runtime
        // disable default logging -> is handled by our own result formatter
        logFailedTests=false;
        execute1();
        // export sorted tests if necessary
        if(FILE_PRESORTED_TESTS!=null && !new File(FILE_PRESORTED_TESTS).exists()){
            try {
                BufferedWriter wr = new BufferedWriter(new FileWriter(FILE_PRESORTED_TESTS));
                for(JUnitTest t : sortedTests){
                    wr.write(t.getName()); wr.newLine();
                }
                wr.flush();
                wr.close();
            } catch (IOException e) {
                logMajorError("Cannot export list of sorted tests: "+e.getMessage());
            }
        }
        testsSorted = true;

        String ignoreOrExclude = excludeFailing ? "Exclude " : "Ignore ";
        // At this point we know which tests failed in the original order -> report the number
        // check failed classes for original, random, and sort_classes
        if(sort==Sorting.sort_classes || sort==Sorting.original || sort==Sorting.random){
            if(failingTestClasses.size()>0){
                // Report number of failing tests (if any)
                logMajor(ignoreOrExclude + failingTestClasses.size()+" failing test class"+(failingTestClasses.size()>1?"es:":":"));
                for(String test : failingTestClasses){
                    logMajorError(" - "+test);
                }
            }
        }
        else if(sort==Sorting.sort_methods){
            if(failingTestMethods.size()>0){
                // Report number of failing tests (if any)
                logMajor(ignoreOrExclude + failingTestMethods.size()+" failing test method"+(failingTestMethods.size()>1?"s:":":"));
                for(String test : failingTestMethods){
                    logMajorError(" - "+test);
                }
            }
        }
        // Excluding failing tests currently not supported for hybrid approaches!
        else{
            if(failingTestMethods.size()+failingTestClasses.size() > 0){
                logMajor(SEPARATOR);
                logMajorError("Excluding failing tests is currently only supported for:");
                logMajorError(" - "+Sorting.original);
                logMajorError(" - "+Sorting.random);
                logMajorError(" - "+Sorting.sort_classes);
                logMajorError(" - "+Sorting.sort_methods);
                logMajor("Please adapt build.xml or test suite, and rerun tests.");
                return;
            }
        }

        // run the original version and record mutation coverage
           __M_NO=0;
        getCoverage = true;

        Iterator<JUnitTest> iter=null;
        // Run tests in sorted order to check independence of tests

        //Iterator<JUnitTest> iter = sortedTests.iterator();
        iter = sortedTests.iterator();
        // Shuffle sorted tests iff attribute sort is set to random
        if(sort==Sorting.random){
            randomTests = new ArrayList<JUnitTest>(sortedTests);
            Collections.shuffle((ArrayList<JUnitTest>)randomTests);
            iter = randomTests.iterator();
        }
        // Indicate the rerun of the ordered or randomized tests
        logMajor(SEPARATOR);
        logMajor("Run "+(sortedTests.size())+" "+(sort==Sorting.random?"randomized":"ordered")+" test"+(sortedTests.size()>1?"s":"")+" to verify independence");
           int currentTestNo=0;
        while(iter.hasNext()){
               JUnitTest t = iter.next();
               currentTest = Collections.singleton(t);
            long start=System.currentTimeMillis();
            execute1();
            long runtime=System.currentTimeMillis()-start;

            // we do not report the runtime necessary to run the coverage analysis!
            t.getProperties().setProperty(RUNTIME_COV, ""+runtime);
            t.getProperties().setProperty(RUNTIME_ORIG, ""+t.getRunTime());
                if(debug) logMajorHighlight(++currentTestNo+"/"+sortedTests.size()+": "+getTestName(t)+": "+t.getRunTime()+"ms / "+((List<Integer>)mutationCoverageMap.get(getTestName(t))).size()+" mutants covered");
                // The runtime of the original with coverage is only a good estimation for the mutation coverage but not for
                // expression checking due to the high time overhead of the expression checking
                // TODO: Do not take coverage analysis runtime into account???
                // t.setRunTime(Math.max(runtime,t.getRunTime()));
            sortedTestsReal.add(t);
        }
           long preProcessingStop = System.currentTimeMillis();

        // Use real runtime instead of estimated runtime only if we do not use expression checking! -> TODO refactoring
           // TODO: Do not take coverage analysis runtime into account???
        // if(configKillableList==null) sortedTests=sortedTestsReal;

        getCoverage = false;
        covMutants = allMutants.size();

        // we do not halt on any error during mutation analysis
        haltOnError=false;
        haltOnFail=false;

        String preproc = String.format("%.2f", ((double)preProcessingStop-preProcessingStart)/1000);

        logMajor(SEPARATOR);
        logMajor("Preprocessing time: "+preproc+" seconds");

        logMajor(SEPARATOR);
        logMajor("Mutants generated: "+genMutants);
        logMajor("Mutants covered:   "+covMutants + String.format(" (%.2f%%)", ((double)covMutants)/genMutants*100));

        // Export test map and provide a unique numbering for tests (used in results, cov and kill maps)
        logMajor(SEPARATOR);
        logMajor("Export test map to ("+FILE_TEST_NAME_MAP+")");
        iter = (sort==Sorting.random) ? randomTests.iterator() : sortedTests.iterator();
        int testId=1;
        try{
              // Write testMap file, which is always generated
            BufferedWriter testMap = new BufferedWriter(new FileWriter(FILE_TEST_NAME_MAP));
            testMap.write("TestNo,TestName");
            testMap.newLine();
            while(iter.hasNext()){
                JUnitTest t = iter.next();
                testMap.write(testId+","+getTestName(t)+"\n");
                t.getProperties().setProperty(TEST_COUNTER, ""+testId);

                ++testId;
            }
            testMap.flush();
            testMap.close();
        }catch(Exception e){
            throw new RuntimeException(e);
        }

        // Export coverage map if necessary
        if(exportCovMap){
            logMajor("Export coverage map to ("+FILE_MUT_COV_MAP+")");
            iter = (sort==Sorting.random) ? randomTests.iterator() : sortedTests.iterator();
            try{
                BufferedWriter mutCovMap = new BufferedWriter(new FileWriter(FILE_MUT_COV_MAP));
                mutCovMap.write("TestNo,MutantNo");
                mutCovMap.newLine();

                while(iter.hasNext()) {
                    JUnitTest t = iter.next();
                    for(Integer mutant : mutationCoverageMap.get(getTestName(t))){
                        mutCovMap.write(t.getProperties().getProperty(TEST_COUNTER)+","+mutant.toString()+"\n");
                    }
                }
                mutCovMap.flush();
                mutCovMap.close();

            }catch(Exception e){
                throw new RuntimeException(e);
            }
        }

        // Check whether weak mutation is enabled
        if(coverageOnly) {
            return;
        }

        // Retain only the mutants in the includeSet if an include file is provided
        if (FILE_INCLUDE_MUTANTS!=null) {
            excludeSet = new TreeSet<Integer>(allMutants);
            excludeSet.removeAll(includeSet);
        }
        // Exclude all mutants from the excludeSet if exclude file is provided
        if (excludeSet.size() > 0) {
            logMajor("Exclude "+excludeSet.size()+" mutants");
            allMutants.removeAll(excludeSet);
        }

        iter = (sort==Sorting.random) ? randomTests.iterator() : sortedTests.iterator();

        logMajor(SEPARATOR);
        logMajor("Run mutation analysis with "+(sortedTests.size())+" individual test"+(sortedTests.size()>1?"s":""));
        //logMajor("Mutants (killed / alive): "+killedMutants.size()+" / "+(allMutants.size()-killedMutants.size()));

        StringBuffer results = new StringBuffer((sortedTests.size()+1)*256);
        // header of result data
        results.append("CurrentTotalRuntime,TestNo,TestRuntimeOriginal,KilledByTest,AnalyzedByTest,CoveredByTest,TotalKilled,TotalFailure,TotalException,TotalTimeout,LiveMutants,TotalAnalyzed\n");
        long start = System.currentTimeMillis();
        int allAnalyzed=0, killed=killedMutants.size(), testCounter=0;

        // Run mutation analysis
        while(iter.hasNext()){
            long startTest = System.currentTimeMillis();
            JUnitTest t = (JUnitTest)iter.next();

            int rtOrig = Integer.parseInt(t.getProperties().getProperty(RUNTIME_ORIG));
            int rtCov  = Integer.parseInt(t.getProperties().getProperty(RUNTIME_COV));
            // Use coverage runtime to have a better approximation of test runtime timeout
            determineAndSetTimeout(rtCov);

            currentTest = Collections.singleton(t);

            // Analyze only covered mutants if coverage is enabled
            Iterator<Integer> iter2 = coverage ? ((List<Integer>)mutationCoverageMap.get(getTestName(t))).iterator() : allMutants.iterator();
            int coveredMutants = coverage ? ((List<Integer>)mutationCoverageMap.get(getTestName(t))).size() : allMutants.size();
            logMajor(SEPARATOR);
            logMajorHighlight((++testCounter)+"/"+(sortedTests.size())+" - "+getTestName(t)+" ("+t.getRunTime()+"ms / "+coveredMutants+"):");
            int analyzed = 0;
            while(iter2.hasNext()){
                Integer mutant = iter2.next();
                // Always skip mutants in exclude list
                if(excludeSet.contains(mutant)) continue;
                // Skip already killed mutants if we are not determining the killMap
                if(!exportKillMap && killedMutants.contains(mutant)) continue;
                ++analyzed;
                __M_NO=mutant.intValue();
                execute1();
            }
            allAnalyzed += analyzed;

            results.append(System.currentTimeMillis()-start+","+t.getProperties().getProperty(TEST_COUNTER)+","+t.getProperties().getProperty(RUNTIME_ORIG)+","+(killedMutants.size()-killed)+","+analyzed+","+coveredMutants+","+killedMutants.size()+","+failure+","+err_exception+","+err_timeout+","+(allMutants.size()-killedMutants.size())+","+allAnalyzed+"\n");
            // TODO: Determine real AVG-Runtime per mutant (exclude timeouts)
            logMajor((System.currentTimeMillis()-start)+" ("+(killedMutants.size()-killed)+" / "+analyzed+" / "+coveredMutants+") -> AVG-RTPM: "+(analyzed==0 ? 0 : ((System.currentTimeMillis()-startTest)/analyzed))+"ms");
            logMajor("Mutants killed / live: "+killedMutants.size()+" ("+failure+"-"+err_exception+"-"+err_timeout+") / "+(allMutants.size()-killedMutants.size()));
            //logMajor("Mutants (analyzed / killed): "+allAnalyzed+" / "+killedMutants.size());
            // store current number of killed mutants
            killed = killedMutants.size();
        }
        double runtime = Math.round(((double)(System.currentTimeMillis()-start))/100);
        runtime /= 10;

        String scoreCov = String.format("%.2f%%", covMutants==0 ? 0 :((double)killedMutants.size())/covMutants*100);
        String scoreGen = String.format("%.2f%%", genMutants==0 ? 0 : ((double)killedMutants.size())/genMutants*100);

        // Print overall summary
        logMajor(SEPARATOR);
        logMajor("Summary:\n");
        logMajor("");
        logMajor("Analysis time:  " +runtime+" seconds");
        logMajor("Mutation score: "+scoreGen + " (" + scoreCov + ")");
        logMajor("Mutants killed / live: "+killedMutants.size()+" ("+failure+"-"+err_exception+"-"+err_timeout+") / "+(allMutants.size()-killedMutants.size()));
        logMajor("Mutant executions: "+allAnalyzed);
        logMajor(SEPARATOR);

        logMajor("Export summary of results (to "+FILE_SUMMARY+")");
        try{
            BufferedWriter bwSummary = new BufferedWriter(new FileWriter(FILE_SUMMARY));
            bwSummary.write("MutantsGenerated,MutantsCovered,MutantsKilled,MutantsLive,RuntimePreprocSeconds,RuntimeAnalysisSeconds");
            bwSummary.newLine();
            bwSummary.write(genMutants+","+allMutants.size()+","+killedMutants.size()+","+(allMutants.size()-killedMutants.size())+","+preproc+","+runtime);
            bwSummary.flush();
            bwSummary.close();
        }catch(Exception e){
            logMajor(SEPARATOR);
            logMajorError("Error while exporting summary:");
            logMajorError(e.getMessage());
        }

        // Export detailed results if necessary
        if(FILE_RESULTS!=null){
            logMajor("Export run-time results (to "+FILE_RESULTS+")");
            try{
                BufferedWriter bwResult = new BufferedWriter(new FileWriter(FILE_RESULTS));
                bwResult.write(results.toString());
                bwResult.flush();
                bwResult.close();
            }catch(Exception e){
                logMajor(SEPARATOR);
                logMajorError("Error while exporting results:");
                logMajorError(e.getMessage());
            }
        }

        // Export details for killed mutants if necessary
        if(FILE_KILL_DETAILS!=null){
            logMajor("Export mutant kill details (to "+FILE_KILL_DETAILS+")");
            try{
                BufferedWriter howKilled = new BufferedWriter(new FileWriter(FILE_KILL_DETAILS));
                howKilled.write("MutantNo,[FAIL | TIME | EXC | LIVE]");
                howKilled.newLine();
                for(Integer mutant : allMutants){
                    if(timeoutMutants.contains(mutant)){
                        howKilled.write(mutant+",TIME");
                    }else if(exceptionMutants.contains(mutant)){
                        howKilled.write(mutant+",EXC");
                    }else if(killedMutants.contains(mutant)){
                        howKilled.write(mutant+",FAIL");
                    }else{
                        howKilled.write(mutant+",LIVE");
                    }
                    howKilled.newLine();
                }
                howKilled.flush();
                howKilled.close();
            }catch(Exception e){
                logMajor(SEPARATOR);
                logMajorError("Error while exporting kill details:");
                logMajorError(e.getMessage());
            }
        }

        // Export Mutant-Kill-Map if necessary
        if(exportKillMap){
            logMajor("Export kill map (to "+FILE_MUT_KILL_MAP+")!");
            try{
                BufferedWriter testMutKillMap = new BufferedWriter(new FileWriter(FILE_MUT_KILL_MAP));
                String baseHeader = "TestNo|MutantNo|[FAIL - TIME - EXC]";
                String header = exportStackTrace ? baseHeader + "|StackTrace" : baseHeader;
                testMutKillMap.write(header);
                testMutKillMap.newLine();
                iter = (sort==Sorting.random) ? randomTests.iterator() : sortedTests.iterator();
                while(iter.hasNext()){
                    JUnitTest t = iter.next();
                    if(mutationKillMap.containsKey(getTestName(t)) && mutationKillMap.get(getTestName(t)).size()>0){
                        for(MutantKillResult result : mutationKillMap.get(getTestName(t))){
                            testMutKillMap.write(t.getProperties().getProperty(TEST_COUNTER) + "|"
                                    + result.getMutantNo() + "|"
                                    + result.getKillReason()
                                    + (exportStackTrace ? "|" + escapeStackTrace(result.getStackTrace()) : ""));
                            testMutKillMap.newLine();
                        }
                    }
                }
                testMutKillMap.flush();
                testMutKillMap.close();
            }catch(Exception e){
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Return displayable name of this test
     */
    private String getTestName(JUnitTest t) {
        if (t.getMethodsString()==null) {
            return t.getName();
        } else {
            return t.getName() + "[" +t.getMethodsString().replace(", ", ";").replace(',', ';') + "]";
        }
    }

    /*
     * Configuration options of Major's junit task
     */
    public void setMutationAnalysis(boolean mutation){
        this.mutationAnalysis=mutation;
    }
    public void setCoverage(boolean coverage){
        this.coverage=coverage;
    }
    public void setSortmethods(boolean sortMethods){
        this.sortMethods=sortMethods;
    }
    public void setDebug(boolean debug){
        this.debug=debug;
    }
    public void setSort(Sorting order){
        this.sort=order;
    }
    public void setThreshold(int th){
        this.threshold=th;
    }
    public void setResultFile(String filename){
        this.FILE_RESULTS=filename;
    }
    public void setSummaryFile(String filename){
        this.FILE_SUMMARY=filename;
    }
    public void setKillDetailsFile(String filename){
        this.FILE_KILL_DETAILS=filename;
    }
    public void setExcludeFile(String filename){
        this.FILE_EXCLUDE_MUTANTS=filename;
    }
    public void setIncludeFile(String filename){
        this.FILE_INCLUDE_MUTANTS=filename;
    }
    public void setPreSortedFile(String filename){
        this.FILE_PRESORTED_TESTS=filename;
    }
    public void setMutantsLogFile(String filename){
        this.FILE_MUT_LOG=filename;
    }
    public void setExportCovMap(boolean covMap){
        this.exportCovMap=covMap;
    }
    public void setExportOnly(boolean covOnly){
        this.coverageOnly=covOnly;
    }
    public void setCovMapFile(String file){
        FILE_MUT_COV_MAP = file;
    }
    public void setTestMapFile(String file){
        FILE_TEST_NAME_MAP = file;
    }
    public void setKillMapFile(String file){
        FILE_MUT_KILL_MAP = file;
    }
    public void setExportKillMap(boolean killMap){
        this.exportKillMap=killMap;
    }
    public void setExportStackTrace(boolean stacktrace){
        this.exportStackTrace = stacktrace;
    }
    public void setTimeoutFactor(int timeoutFactor){
        this.timeoutFactor=timeoutFactor;
    }
    public void setTimeoutOffset(int timeoutOffset){
        this.timeoutOffset=timeoutOffset;
    }
    public void setExcludeFailingTests(boolean excludeFailing) {
        this.excludeFailing = excludeFailing;
    }

    /*
     * Given the runtime of a test in ms, this method
     * determines and sets the corresponding timeout in ms
     */
    private void determineAndSetTimeout(int runtime) {
        int timeout;
        if(runtime*timeoutFactor<100) {
            timeout = 100;
        }else if(runtime <= 250){
            timeout = runtime*timeoutFactor;
        }else if(runtime <= 500){
            timeout = 500*(timeoutFactor>>1);
        }else if(runtime <= 1000){
            timeout = runtime*(timeoutFactor>>1);
        }else{
            if(timeoutFactor>4 && runtime<=2000){
                timeout = (timeoutFactor>>2)*2000;
            }else{
                timeout = runtime*2;
            }
        }
        this.timeout = new Integer(timeout + timeoutOffset);
    }

    private ThreadGroup mutationGroup = new ThreadGroup("Major-ThreadGroup");
    /**
     * Every mutant will be executed with an individual thread which
     * is an instance of this class. All threads are generated within
     * the ThreadGroup <code>mutationGroup</code>
     */
    private class TestRunnerThread extends Thread {
        private JUnitTaskMirror.JUnitTestRunnerMirror runner;
        private TestResultHolder result;
        private int mutant;

        private TestRunnerThread(final int mutant, JUnitTaskMirror.JUnitTestRunnerMirror runner, TestResultHolder result){
            super(mutationGroup, "Mutant-"+mutant);
            this.mutant=mutant;
            this.runner=runner;
            this.result=result;

            setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable th) {
                    // Ignore mutants that were stopped later due to race conditions or infinite loops
                    if (! (th instanceof ThreadDeath)) {
                        logMajorError("Uncaught exception in mutant "+mutant+": "+th);
                    }
                }
            });
        }

        public void run() {
            try{
                runner.run();
            }catch(KilledByFailure kf){
                if (!addKilledMutant(mutant)) {
                    return;
                }
                ++failure;
                if(debug)logMajorError("Failure in mutant "+mutant);
                return;
            }catch(KilledByException ke){
                if (!addKilledMutant(mutant)) {
                    return;
                }
                exceptionMutants.add(mutant);
                ++err_exception;
                if(debug)logMajorError("Exception in mutant "+mutant);
                return;
            }catch(ThreadDeath td){
                if (!addKilledMutant(mutant)) {
                    return;
                }
                timeoutMutants.add(mutant);
                ++err_timeout;
                if(debug)logMajorError("Timeout in mutant "+mutant);
                return;
            // An exception that is not recognized by the result formatter (pre- or postprocessing)
            }catch(Throwable t){
                if (!addKilledMutant(mutant)) {
                    return;
                }
                exceptionMutants.add(mutant);
                ++err_exception;
                if(debug)logMajorError("Pre- or Postprocessing error in mutant "+mutant);
                return;
            }finally{
                int currentNo = getThreadGroup().activeCount();
                // if still more than 1 thread is active -> kill all remaining (runnable) threads!
                if(currentNo>1){
                    Thread[] postExcistingThreads = new Thread[currentNo+5];
                    int tmp = getThreadGroup().enumerate(postExcistingThreads);
                    // assure that array was large enough to store all threads
                    assert tmp < postExcistingThreads.length;

                    for(int i=0; i<tmp; ++i){
                        Thread t = postExcistingThreads[i];
                        if(t.getState()==Thread.State.RUNNABLE && t != this){
                            t.stop();
                        }
                    }
                }

            }
            result.exitCode = runner.getRetCode();
        }
    }

    private synchronized boolean addKilledMutant(Integer mutant) {
        if (killedMutants.contains(mutant)) {
            return false;
        }
        killedMutants.add(mutant);
        return true;
    }

    /*
     * Every mutant will be executed with an individual thread which
     * is an instance of this class.
     */
    private class TestRunnerThreadMap extends Thread {
        private JUnitTaskMirror.JUnitTestRunnerMirror runner;
        private TestResultHolder result;
        private JUnitTest test;
        private int mutant;

        private TestRunnerThreadMap(int mutant, JUnitTest test, JUnitTaskMirror.JUnitTestRunnerMirror runner, TestResultHolder result){
            super(mutationGroup, "Mutant-"+mutant);
            this.mutant=mutant;
            this.runner=runner;
            this.result=result;
            this.test=test;
        }

        public void run() {
            try{
                runner.run();
            }catch(KilledByFailure kf){
                addKilledMutant(KillReason.FAIL, stackTraceAsString(kf.getWrappedThrowable()));
                ++failure;
                throw kf;
            }catch(KilledByException ke){
                addKilledMutant(KillReason.EXC, stackTraceAsString(ke.getWrappedThrowable()));
                ++err_exception;
                throw ke;
            }catch(ThreadDeath td){
                addKilledMutant(KillReason.TIME, stackTraceAsString(td));
                ++err_timeout;
                throw td;
            }finally{
                int currentNo = getThreadGroup().activeCount();
                // if still more than 1 thread is active -> kill all remaining (runnable) threads!
                if(currentNo>1){
                    Thread[] postExcistingThreads = new Thread[currentNo+5];
                    int tmp = getThreadGroup().enumerate(postExcistingThreads);
                    // assure that array was large enough to store all threads
                    assert tmp < postExcistingThreads.length;

                    for(int i=0; i<tmp; ++i){
                        Thread t = postExcistingThreads[i];
                        if(t.getState()==Thread.State.RUNNABLE && t != this){
                            t.stop();
                        }
                    }
                }

            }
            result.exitCode = runner.getRetCode();
        }

        private synchronized void addKilledMutant(KillReason reason, String stackTrace){
            killedMutants.add(new Integer(mutant));
            if(!mutationKillMap.containsKey(getTestName(test))){
                mutationKillMap.put(getTestName(test), new LinkedList<MutantKillResult>());
            }
            mutationKillMap.get(getTestName(test)).add(new MutantKillResult(mutant, reason, stackTrace));
        }
    }

    /*
     * Comparator for reordering the test suite
     */
    private class MajorComparator<T> implements Comparator<T>{
        public int compare(Object o1, Object o2) {
            if(sort==Sorting.sort_classes || sort==Sorting.sort_methods || sort==Sorting.sort_hybrid_methods || sort==Sorting.sort_hybrid_classes){
                if(presortMap!=null){
                    Integer i1 = presortMap.get(((JUnitTest) o1).getName());
                    Integer i2 = presortMap.get(((JUnitTest) o2).getName());

                    if(i1==null || i2==null){
                        logMajorError("List of presorted tests inconsistent: "+o1+" <-> "+o2);
                        throw new RuntimeException("List of presorted tests inconsistent!");
                    }

                    return i1-i2;
                }
                // compare by test runtimes
                long t1 = ((JUnitTest) o1).getRunTime();
                long t2 = ((JUnitTest) o2).getRunTime();
                // we do not allow equal tests
                return (t1<t2)?-1:1;
            }else{
                // compare by test names
                String n1 = ((JUnitTest) o1).getName();
                String n2 = ((JUnitTest) o2).getName();

                return n1.compareTo(n2);
            }
        }
    }

    // Subclasses for already killed mutants
    // Added throwable wrapper to get stacktrace details later
    private class KilledByFailure extends ThreadDeath{
        private static final long serialVersionUID = 1L;
        private Throwable wrappedThrowable;

        public KilledByFailure(Throwable wrappedThrowable) {
            this.wrappedThrowable = wrappedThrowable;
        }

        public Throwable getWrappedThrowable() {
            return wrappedThrowable;
        }

        public void setWrappedThrowable(Throwable wrappedThrowable) {
            this.wrappedThrowable = wrappedThrowable;
        }
    }
    private class KilledByException extends ThreadDeath{
        private static final long serialVersionUID = 1L;
        private Throwable wrappedThrowable;

        public KilledByException(Throwable wrappedThrowable) {
            this.wrappedThrowable = wrappedThrowable;
        }

        public Throwable getWrappedThrowable() {
            return wrappedThrowable;
        }

        public void setWrappedThrowable(Error wrappedThrowable) {
            this.wrappedThrowable = wrappedThrowable;
        }
    }

    /**
     * Simple wrapper to save runtime of test methods
     * This class also provides a runtime-based comparator
     * @author rjust
     *
     */
    private static class TestMethod implements Comparable{
        private String name;
        private long runtime;

        private TestMethod(String name, long runtime){
            this.name=name;
            this.runtime=runtime;
        }

        @Override
        public String toString(){
            return name;
        }

        @Override
        public int compareTo(Object o) {
            TestMethod m = (TestMethod)o;
            return runtime<m.runtime?-1:1;
        }

    }

    private void updateTestCoverage(String testName){
        // Coverage is determined for ordered tests! Also, we have already excluded
        // all failing tests to have a precise coverage of running tests
        if(!getCoverage) return;
        try {
               List<Integer> l = (List<Integer>) configCoverageList.invoke(null, null);
               mutationCoverageMap.put(testName, l);
               allMutants.addAll(l);

               // Reset the coverage information
               configReset.invoke(null, null);
           } catch (Exception e) {
               printStackTrace(e);
               throw new RuntimeException(e);
           }
    }

    /*
     * We use this ResultFormatter to gather the runtime of the
     * individual tests and to mark killed mutants during the
     * mutation analysis process.
     */
    private class MajorResultFormatterHybrid extends MajorResultFormatterTestSuite{
        private long test_start, test_stop;
        private Set<JUnitTest> tmpSet = new TreeSet<JUnitTest>(new MajorComparator<JUnitTest>());
        private JUnitTest tmpTest = null;
        private String currentTestClassName=null;
        // The sorted set to prioritize test methods within a test class
        private Set<TestMethod> testMethods = new TreeSet<TestMethod>();
        // We have to cache the method name since the method may be extracted
        // and in this case we do not add the method to the sorted set
        private String tmpMethodName;

        public void setOutput(OutputStream out) {}
        public void setSystemError(String err) {}
        public void setSystemOutput(String out) {}

        public void startTest(Test t) {
            test_start = System.currentTimeMillis();
        }

        public void endTest(Test t) {
            test_stop = System.currentTimeMillis();
            test_time+=(test_stop-test_start);
            boolean paramTest=false;
            if(!testsSorted){
                //Add every test method as an individual test suite
                JUnitTest indiTest=(JUnitTest)currentTestSuite.clone();
                indiTest.setRunTime(initTime+test_stop-test_start);
                if(t instanceof TestCase){
                    // Reset tmpTest reference to 'null' if the test class within a test suite changes
                    // -> Otherwise a test method would be added to the wrong test class!
                    if(!t.getClass().getName().equals(currentTestClassName)){
                        // The test class changed -> Add old test class (if it exists) with
                        // all methods to the set of individual tests
                        if(tmpTest!=null){
                            StringBuffer buf = new StringBuffer(1024);
                            for(TestMethod m : testMethods){
                                buf.append(m.toString());buf.append(",");
                            }
                            buf.deleteCharAt(buf.length()-1);

                            // Sort methods within class?
                            if(sortMethods)tmpTest.setMethods(buf.toString());
                            tmpSet.add(tmpTest);

                            testMethods.clear();

                            // Reset the tmpTest reference to indicate that we found a new test case within a test suite!
                            tmpTest=null;

                        }
                        // Set current test class name
                        currentTestClassName=t.getClass().getName();
                    }
                    // If the test method was executed within a test suite
                    indiTest.setName(t.getClass().getName());

                    tmpMethodName = ((TestCase)t).getName();
                    indiTest.setMethods(tmpMethodName);

                }else if (t instanceof JUnit4TestCaseFacade){
                    tmpMethodName = ((JUnit4TestCaseFacade)t).getDescription().getMethodName();
                    indiTest.setMethods(tmpMethodName);
                    // Hack: Do not split param tests!
                    paramTest=tmpMethodName.contains("[");
                }else{
                    throw new RuntimeException("Unknown test class: "+t.getClass().getName());
                }

                // if a test method lasts longer than the defined threshold
                // than this test case will be executed as an individual test case!
                if(!(sort==Sorting.sort_hybrid_classes) || ((test_stop-test_start) > threshold && !paramTest)){
                    tmpSet.add(indiTest);
                    if(debug && sort==Sorting.sort_hybrid_classes) logMajor("["+sort+"] Test case runtime gt threshold");

                    // add method to sorted set, we might use it later for sort_hybrid
                    testMethods.add(new TestMethod(tmpMethodName, test_stop-test_start));
                }else{
                    // Since the individual method is not extracted, we add it to the sorted set
                    testMethods.add(new TestMethod(tmpMethodName, test_stop-test_start));

                    if(tmpTest==null){
                        tmpTest = indiTest;
                    }else{
                        tmpTest.setMethods(tmpTest.getMethodsString()+", "+indiTest.getMethodsString());
                        tmpTest.setRunTime(tmpTest.getRunTime()+indiTest.getRunTime());
                    }
                }
            }
        }

        public void startTestSuite(JUnitTest suite) throws BuildException {
            super.startTestSuite(suite);
            test_time=0;
            tmpSet.clear();
            tmpTest=null;
            currentTestClassName=null;
            testMethods.clear();
        }

        /*
         * This method is used to sort the tests based on their runtime
         * while executing the test suite in its original order.
         */
        public void endTestSuite(JUnitTest suite) throws BuildException {
            if(!testsSorted){
                if(sort==Sorting.sort_hybrid_methods && test_time<threshold){
                    // Add complete test suite if the runtime is less than threshold
                    JUnitTest tmp = (JUnitTest)suite.clone();
                    // Just in case that a test class does not contain any test methods
                    if(testMethods.size()>0){
                        // Set method names ordered according to their runtime
                        StringBuffer buf = new StringBuffer(1024);
                        for(TestMethod m : testMethods){
                            buf.append(m.toString());buf.append(",");
                        }
                        buf.deleteCharAt(buf.length()-1);

                        // Sort methods within classes?
                        if(sortMethods)tmp.setMethods(buf.toString());
                        testMethods.clear();
                    }

                    sortedTests.add(tmp);
                    if(debug) log("[SORT_HYBRID_METHODS] Test suite runtime lt threshold");
                }else{
                    // No test method was excluded
                    if(tmpSet.size()==0 && tmpTest!=null){

                        // Sort methods within classes? If not reset methods to null -> execute all
                        if(!sortMethods) tmpTest.setMethods((String)null);
                    }else{
                        // Add all extracted test methods
                        sortedTests.addAll(tmpSet);
                    }
                    // Add test class with all remaining methods
                    if(tmpTest!=null){
                        // Set method names ordered according to their runtime
                        StringBuffer buf = new StringBuffer(1024);
                        for(TestMethod m : testMethods){
                            buf.append(m.toString());buf.append(",");
                        }
                        buf.deleteCharAt(buf.length()-1);

                        // Sort methods within classes?
                        if(sortMethods) tmpTest.setMethods(buf.toString());

                        testMethods.clear();

                        sortedTests.add(tmpTest);
                    }
                }

                if(debug){
                    log("DEBUG: Sorted Tests");
                    for (JUnitTest t : sortedTests){
                        log("[DEBUG] "+getTestName(t)+" -- "+t.getRunTime());
                    }
                }
            }
            updateTestCoverage(getTestName(suite));

            currentTestSuite=null;
        }
    }


    /*
    * We use this ResultFormatter to gather the runtime of the
    * individual test classes and to mark killed mutants during the
    * mutation analysis process.
    */
   private class MajorResultFormatterTestClasses extends MajorResultFormatterTestSuite{
       private long test_start, test_stop;
       private Set<JUnitTest> tmpSet = new TreeSet<JUnitTest>(new MajorComparator<JUnitTest>());
       private JUnitTest tmpTest = null;
       private String currentTestClassName=null;

       private Set<TestMethod> testMethods = new TreeSet<TestMethod>();


        public void setOutput(OutputStream out) {}
        public void setSystemError(String err) {}
        public void setSystemOutput(String out) {}

        public void startTest(Test t) {
            test_start = System.currentTimeMillis();
        }

        public void endTest(Test t) {
            test_stop = System.currentTimeMillis();
            if(!testsSorted){
                //Add every test method as an individual test suite
                JUnitTest indiTest=(JUnitTest)currentTestSuite.clone();
                indiTest.setRunTime(initTime+test_stop-test_start);
                if(t instanceof TestCase){
                    // Reset tmpTest reference to 'null' if the test class within a test suite changes
                    // -> Otherwise a test method would be added to the wrong test class!
                    if(!t.getClass().getName().equals(currentTestClassName)){
                        // The test class changed -> Add old test class (if it exists) with
                        // all methods to the set of individual tests
                        if(tmpTest!=null){
                            StringBuffer buf = new StringBuffer(1024);
                            for(TestMethod m : testMethods){
                                buf.append(m.toString());buf.append(",");
                            }
                            buf.deleteCharAt(buf.length()-1);

                            // Sort methods within classes?
                            if(sortMethods){
                                tmpTest.setMethods(buf.toString());
                            }
                            // check whether test failed
                            if(failingTestClasses.contains(tmpTest.getName())  && excludeFailing){
                                //logMajorError("Skip - "+tmpTest.getName());
                            }else{
                                tmpSet.add(tmpTest);
                            }
                            // Reset the tmpTest reference to indicate that we found a new test case within a test suite!
                            tmpTest=null;

                            /*
                            log("--------------------"+t.getClass().getName());
                            for(TestMethod m : testMethods){
                                log("#### "+m+" ("+m.runtime+")");
                            }
                            */
                            testMethods.clear();
                        }
                        // Set current class name
                        currentTestClassName=t.getClass().getName();
                    }
                    // If the test method was executed within a test suite
                    testMethods.add(new TestMethod(((TestCase)t).getName(), test_stop-test_start));
                    indiTest.setName(t.getClass().getName());
                    indiTest.setMethods((String)null);
                }else if (t instanceof JUnit4TestCaseFacade){
                    testMethods.add(new TestMethod(((JUnit4TestCaseFacade)t).getDescription().getMethodName(), test_stop-test_start));
                    indiTest.setMethods((String)null);
                }else{
                    throw new RuntimeException("Unknow test class: "+t.getClass().getName());
                }

                if(tmpTest==null){
                    tmpTest = indiTest;
                }else{
                    tmpTest.setMethods((String)null);
                    tmpTest.setRunTime(tmpTest.getRunTime()+indiTest.getRunTime());
                }
            }
        }

        public void startTestSuite(JUnitTest suite) throws BuildException {
            super.startTestSuite(suite);
            tmpSet.clear();
            tmpTest=null;
            currentTestClassName=null;
        }

        /*
         * This method is used to sort the tests based on their runtime
         * while executing the test suite in its original order.
         */
        public void endTestSuite(JUnitTest suite) throws BuildException {
            if(!testsSorted){
                if(tmpSet.size()==0 && tmpTest!=null){
                    //tmpTest.setMethods((String)null);
                    StringBuffer buf = new StringBuffer(1024);
                    for(TestMethod m : testMethods){
                        buf.append(m.toString());buf.append(",");
                    }
                    buf.deleteCharAt(buf.length()-1);

                    // Sort methods within classes?
                    if(sortMethods) tmpTest.setMethods(buf.toString());
                }else{
                    sortedTests.addAll(tmpSet);
                }
                //
                if(tmpTest!=null) {
                    // check whether test failed
                    if(failingTestClasses.contains(tmpTest.getName())  && excludeFailing){
                        //logMajorError("Skip - "+tmpTest.getName());
                    }else{
                        sortedTests.add(tmpTest);
                    }
                }
                /*
                log(">>>>>>>>>>>>>>>>>>"+suite.getName());
                for(TestMethod m : testMethods){
                    log("#### "+m+" ("+m.runtime+")");
                }
                */
                testMethods.clear();
            }
            updateTestCoverage(getTestName(suite));
            currentTestSuite=null;
        }
    }


    /*
     * We use this ResultFormatter to gather the runtime of the
     * individual tests and to mark killed mutants during the
     * mutation analysis process.
     */
    private class MajorResultFormatterTestMethods extends MajorResultFormatterTestSuite{
        private long test_start, test_stop;

        public void setOutput(OutputStream out) {}
        public void setSystemError(String err) {}
        public void setSystemOutput(String out) {}

        public void startTest(Test t) {
            test_start = System.currentTimeMillis();
        }

        public void endTest(Test t) {
            test_stop = System.currentTimeMillis();

            // add every non-failing test to sorted set
            if(!testsSorted){
                //Add every test method as an individual test suite
                JUnitTest indiTest=(JUnitTest)currentTestSuite.clone();
                indiTest.setRunTime(initTime+test_stop-test_start);
                if(t instanceof TestCase){
                    // If the test method was executed within a test suite
                    indiTest.setName(t.getClass().getName());

                    indiTest.setMethods(((TestCase)t).getName());
                }else if (t instanceof JUnit4TestCaseFacade){
                    indiTest.setMethods(((JUnit4TestCaseFacade)t).getDescription().getMethodName());
                }else{
                    throw new RuntimeException("Unknow test class: "+t.getClass().getName());
                }

                // check whether test failed
                if(failingTestMethods.contains(getTestName(indiTest)) && excludeFailing){
                    //logMajorError("Skip - "+indiTest.getName()+"$"+indiTest.getMethodsString());
                }else{
                    sortedTests.add(indiTest);
                }
            }
        }

        /*
         * This method is used to sort the tests based on their runtime
         * while executing the test suite in its original order.
         */
        public void endTestSuite(JUnitTest suite) throws BuildException {
            updateTestCoverage(getTestName(suite));
            currentTestSuite=null;
        }
    }

    /*
     * We use this ResultFormatter to gather the runtime of the
     * test suites and to mark killed mutants during the
     * mutation analysis process.
     */
    private class MajorResultFormatterTestSuite implements JUnitResultFormatter{
        public void setOutput(OutputStream out) {}
        public void setSystemError(String err) {}
        public void setSystemOutput(String out) {}

        public void startTest(Test t) {}
        public void endTest(Test t) {}

        protected JUnitTest currentTestSuite=null;

        protected long stopSuite=0;
        protected long startSuite=0;
        //protected long initTime=0;
        protected long initTime=1;

        public void startTestSuite(JUnitTest suite) throws BuildException {
            startSuite=System.currentTimeMillis();
            //initTime=startSuite-stopSuite;
            this.currentTestSuite=suite;
            // reset error flag for new tests
        }

        /*
         * This method is used to sort test suites based on their runtime
         * while executing the test suites in its original order.
         */
        public void endTestSuite(JUnitTest suite) throws BuildException {
            if(!testsSorted){
                // Add complete test suite with corresponding runtime to a sorted set

                // check whether test failed
                if(failingTestClasses.contains(suite.getName())  && excludeFailing){
                    //logMajorError("Skip - "+suite.getName());
                }else{
                    sortedTests.add((JUnitTest)suite.clone());
                }
            }
            updateTestCoverage(getTestName(suite));
            currentTestSuite=null;
            stopSuite=System.currentTimeMillis();
        }

        /*
         * Since every Exception will be reported to this method,
         * we have to distinguish between two cases:
         *  1) The test timed out
         *     -> The thread is killed and a ThreadDeath is thrown.
         *  2) The mutation caused an Exception at runtime
         *
         * We use different counters for these cases, since we want to
         * know how many mutants:
         *  - lead to a timeout;
         *  - are killed by an Exception;
         *  - are killed by the test itself;
         */
        public void addError(Test t, Throwable th) {
            // Always print Stacktrace for errors in the original version
            if(__M_NO<=0){
                String failingTestClass=null;
                String failingTestMethod=null;

                if(t instanceof TestCase){
                    // If the test method was executed within a test suite
                    failingTestClass  = t.getClass().getName();
                    failingTestMethod = failingTestClass+"["+((TestCase)t).getName()+"]";
                }else if (t instanceof JUnit4TestCaseFacade){
                    failingTestClass = ((JUnit4TestCaseFacade)t).getDescription().getClassName();
                    failingTestMethod = failingTestClass+"["+((JUnit4TestCaseFacade)t).getDescription().getMethodName()+"]";
                }else{
                    throw new RuntimeException(th);
                }

                logMajorError(failingTestMethod+" failed!");
                logMajorError(" -> "+th.getClass().getName());
                logMajorError(" -> \""+th.getMessage()+"\"");
                failingTestClasses.add(failingTestClass);
                failingTestMethods.add(failingTestMethod);

                // debug stack trace of failing test
                if(debug){
                    printStackTrace(th);
                }

            }
            // Part for mutation analysis
            else{
                // Already killed mutant -> rethrow the Exception
                if(th instanceof KilledByFailure) {
                    throw (KilledByFailure)th;
                }else{
                    if(th instanceof ThreadDeath){
                        throw (ThreadDeath)th;
                    }
                    // Mutant killed by an arbitrary Exception
                    else{
                        // Throw KilledByException to avoid executing other test methods on the already killed mutant
                        throw new KilledByException(th);
                    }
                }
            }
        }

        public void addFailure(Test t, AssertionFailedError err) {
            if(__M_NO<=0){
                String failingTestClass=null;
                String failingTestMethod=null;

                if(t instanceof TestCase){
                    // If the test method was executed within a test suite
                    failingTestClass  = t.getClass().getName();
                    failingTestMethod = failingTestClass+"["+((TestCase)t).getName()+"]";
                }else if (t instanceof JUnit4TestCaseFacade){
                    failingTestClass = ((JUnit4TestCaseFacade)t).getDescription().getClassName();
                    failingTestMethod = failingTestClass+"["+((JUnit4TestCaseFacade)t).getDescription().getMethodName()+"]";
                }else{
                    throw new RuntimeException("Unknow test class: "+t.getClass().getName());
                }

                logMajorError(failingTestMethod+" failed!");
                logMajorError(" -> AssertionFailedError");
                logMajorError(" -> \""+err.getMessage()+"\"");
                failingTestClasses.add(failingTestClass);
                failingTestMethods.add(failingTestMethod);

                // debug stack trace of failing test
                if(debug){
                    // Print Stacktrace for errors in the original version in debug mode
                    printStackTrace(err);
                }
            }
            // Part for mutation analysis
            else{
                // Throw KilledByFailure to avoid executing other test methods on the already killed mutant
                if(__M_NO>0)throw new KilledByFailure(err);
            }
        }
    }


    // print StackTrace to ant logging system
    private void printStackTrace(Throwable th){
        StackTraceElement[] trace = th.getStackTrace();
        for(int i=0; i< trace.length; ++i){
            logMajorError(trace[i].getFileName()+":"+trace[i].getClassName()+"@"+trace[i].getMethodName()+" : "+trace[i].getLineNumber());
        }
    }

    private String stackTraceAsString(Throwable th) {
        StringWriter sw = new StringWriter();
        th.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }


    private String escapeStackTrace(String stackTrace) {
         return stackTrace.replaceAll("\r\n|[\r\n]", "~newline~");
    }

    private boolean hasColorSupport(){
        // determine terminal on UNIX systems
        String term = System.getenv("TERM");
        if(term==null) return false;

        return term.toLowerCase().startsWith("xterm");
    }

    private void logMajorHighlight(String msg){
        if(!colorTerm) {
            logMajor(msg);
            return;
        }
        logMajor("\033[1;34m"+msg+"\033[0m");
    }

    private void logMajorError(String msg){
        if(!colorTerm) {
            logMajor(msg);
            return;
        }
        logMajor("\033[1;31m"+msg+"\033[0m");
    }

    private void logMajor(String msg){
        log("MAJOR: "+msg);
    }
}

