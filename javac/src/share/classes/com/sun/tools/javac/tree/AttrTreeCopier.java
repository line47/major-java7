package com.sun.tools.javac.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.ArrayTypeTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.CaseTree;
import com.sun.source.tree.CatchTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ErroneousTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.ModifiersTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParameterizedTypeTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.PrimitiveTypeTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TreeVisitor;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.UnionTypeTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.source.tree.WildcardTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCArrayAccess;
import com.sun.tools.javac.tree.JCTree.JCArrayTypeTree;
import com.sun.tools.javac.tree.JCTree.JCAssert;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCAssignOp;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCBreak;
import com.sun.tools.javac.tree.JCTree.JCCase;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;
import com.sun.tools.javac.tree.JCTree.JCConditional;
import com.sun.tools.javac.tree.JCTree.JCContinue;
import com.sun.tools.javac.tree.JCTree.JCDoWhileLoop;
import com.sun.tools.javac.tree.JCTree.JCEnhancedForLoop;
import com.sun.tools.javac.tree.JCTree.JCErroneous;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCForLoop;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCIf;
import com.sun.tools.javac.tree.JCTree.JCImport;
import com.sun.tools.javac.tree.JCTree.JCInstanceOf;
import com.sun.tools.javac.tree.JCTree.JCLabeledStatement;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCModifiers;
import com.sun.tools.javac.tree.JCTree.JCNewArray;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.JCTree.JCParens;
import com.sun.tools.javac.tree.JCTree.JCPrimitiveTypeTree;
import com.sun.tools.javac.tree.JCTree.JCReturn;
import com.sun.tools.javac.tree.JCTree.JCSkip;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCSwitch;
import com.sun.tools.javac.tree.JCTree.JCSynchronized;
import com.sun.tools.javac.tree.JCTree.JCThrow;
import com.sun.tools.javac.tree.JCTree.JCTry;
import com.sun.tools.javac.tree.JCTree.JCTypeApply;
import com.sun.tools.javac.tree.JCTree.JCTypeCast;
import com.sun.tools.javac.tree.JCTree.JCTypeParameter;
import com.sun.tools.javac.tree.JCTree.JCTypeUnion;
import com.sun.tools.javac.tree.JCTree.JCUnary;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.JCTree.JCWhileLoop;
import com.sun.tools.javac.tree.JCTree.JCWildcard;
import com.sun.tools.javac.tree.JCTree.LetExpr;
import com.sun.tools.javac.tree.JCTree.TypeBoundKind;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.util.Name;

/**
 * Creates a copy of an <b>attributed</b> tree with the same type and position.
 * Names, literal values, symbols, etc are shared with the original tree.
 */
public class AttrTreeCopier<P> implements TreeVisitor<JCTree,P>{

    /*
     * Maps label name to target node for all duplicated labeled statements
     */
    private Map<Name,java.util.List<JCTree>> unresolvedLabeledTargets = new HashMap<Name,java.util.List<JCTree>>();

    private Map<JCTree,java.util.List<JCTree>> unresolvedTargets = new HashMap<JCTree,java.util.List<JCTree>>();

    private void addUnresolvedLabeledTarget(Name label, JCTree node){
        if(unresolvedLabeledTargets.containsKey(label)){
            unresolvedLabeledTargets.get(label).add(node);
        }else{
            java.util.List<JCTree> tmp = new ArrayList<JCTree>();
            tmp.add(node);
            unresolvedLabeledTargets.put(label, tmp);
        }
    }

    private void addUnresolvedTarget(JCTree target, JCTree node){
        if(unresolvedTargets.containsKey(target)){
            unresolvedTargets.get(target).add(node);
        }else{
            java.util.List<JCTree> tmp = new ArrayList<JCTree>();
            tmp.add(node);
            unresolvedTargets.put(target, tmp);
        }
    }

    private void resolveTargets(JCTree oldTarget, JCTree newTarget){
        // update all unlabeled nodes targeting at this switch tree
        if(unresolvedTargets.containsKey(oldTarget)){
            for(JCTree stmt : unresolvedTargets.get(oldTarget)){
                switch(stmt.getTag()){
                    case JCTree.BREAK:
                        ((JCBreak)stmt).target=newTarget;
                        break;
                    case JCTree.CONTINUE:
                        ((JCContinue)stmt).target=newTarget;
                        break;
                    default:
                        throw new RuntimeException("Unexpected unlabeled statement: "+stmt.getClass().getName());
                }
            }
            // remove this label since we updated all references
            unresolvedTargets.remove(oldTarget);
        }
    }

    public <T extends JCTree> T copy(T tree) {
        return copy(tree, null);
    }

    @SuppressWarnings("unchecked")
    public <T extends JCTree> T copy(T tree, P p) {
        if (tree == null)
            return null;

        T result = (T) tree.accept(this, p);

        // if this node was referenced by other nodes (e.g., switch targeted by break), update all targets of referencing nodes
        resolveTargets(tree, result);

        return result;
    }

    public <T extends JCTree> List<T> copy(List<T> trees) {
        return copy(trees, null);
    }

    public <T extends JCTree> List<T> copy(List<T> trees, P p) {
        if (trees == null)
            return null;
        ListBuffer<T> lb = new ListBuffer<T>();
        for (T tree: trees)
            lb.append(copy(tree, p));
        return lb.toList();
    }

    @Override
    public JCTree visitBinary(BinaryTree node, P p) {
        JCBinary t = (JCBinary) node;
        JCExpression lhs = copy(t.lhs, p);
        JCExpression rhs = copy(t.rhs, p);

        return new JCBinary(t.getTag(), lhs, rhs, t.operator).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitIdentifier(IdentifierTree node, P p) {
        JCIdent t = (JCIdent) node;


        return new JCIdent(t.name, t.sym).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitMemberSelect(MemberSelectTree node, P p) {
        JCFieldAccess t = (JCFieldAccess)node;
        JCExpression selected = copy(t.selected, p);

        return new JCFieldAccess(selected, t.name, t.sym).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitUnary(UnaryTree node, P p) {
        JCUnary t = (JCUnary) node;
        JCExpression arg = copy(t.arg, p);

        JCUnary copy = (JCUnary)new JCUnary(t.getTag(), arg).setPos(t.pos).setType(t.type);
        copy.operator = t.operator;

        return copy;
    }

    @Override
    public JCTree visitParenthesized(ParenthesizedTree node, P p) {
        JCParens t = (JCParens) node;
        JCExpression expr = copy(t.expr, p);

        return new JCParens(expr).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitLiteral(LiteralTree node, P p) {
        JCLiteral t = (JCLiteral) node;
        return new JCLiteral(t.typetag, t.value).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitArrayAccess(ArrayAccessTree node, P p) {
        JCArrayAccess t = (JCArrayAccess) node;
        JCExpression indexed = copy(t.indexed, p);
        JCExpression index = copy(t.index, p);
        return new JCArrayAccess(indexed, index).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitMethodInvocation(MethodInvocationTree node, P p) {
        JCMethodInvocation t = (JCMethodInvocation) node;
        List<JCExpression> typeargs = copy(t.typeargs, p);
        JCExpression meth = copy(t.meth, p);
        List<JCExpression> args = copy(t.args, p);

        JCMethodInvocation copy = (JCMethodInvocation) new JCMethodInvocation(typeargs, meth, args).setPos(t.pos).setType(t.type);
        copy.varargsElement = t.varargsElement;

        return copy;
    }

    @Override
    public JCTree visitTypeCast(TypeCastTree node, P p) {
        JCTypeCast t = (JCTypeCast) node;
        JCTree clazz = copy(t.clazz, p);
        JCExpression expr = copy(t.expr, p);

        return new JCTypeCast(clazz, expr).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitInstanceOf(InstanceOfTree node, P p) {
        JCInstanceOf t = (JCInstanceOf) node;
        JCExpression expr = copy(t.expr, p);
        JCTree clazz = copy(t.clazz, p);

        return new JCInstanceOf(expr, clazz).setPos(t.pos).setType(t.type);

    }

    @Override
    public JCTree visitPrimitiveType(PrimitiveTypeTree node, P p) {
        JCPrimitiveTypeTree t = (JCPrimitiveTypeTree) node;

        return new JCPrimitiveTypeTree(t.typetag).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitAssignment(AssignmentTree node, P p) {
        JCAssign t = (JCAssign) node;
        JCExpression lhs = copy(t.lhs, p);
        JCExpression rhs = copy(t.rhs, p);

        return new JCAssign(lhs, rhs).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitConditionalExpression(ConditionalExpressionTree node, P p) {
        JCConditional t = (JCConditional) node;
        JCExpression cond = copy(t.cond, p);
        JCExpression truepart = copy(t.truepart, p);
        JCExpression falsepart = copy(t.falsepart, p);

        return new JCConditional(cond, truepart, falsepart).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitArrayType(ArrayTypeTree node, P p) {
        JCArrayTypeTree t = (JCArrayTypeTree) node;
        JCExpression elemtype = copy(t.elemtype, p);

        return new JCArrayTypeTree(elemtype).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitNewClass(NewClassTree node, P p) {
        JCNewClass t = (JCNewClass) node;
        JCExpression encl = copy(t.encl, p);
        List<JCExpression> typeargs = copy(t.typeargs, p);
        JCExpression clazz = copy(t.clazz, p);
        List<JCExpression> args = copy(t.args, p);
        JCClassDecl def = copy(t.def, p);

        JCNewClass copy = (JCNewClass)new JCNewClass(encl, typeargs, clazz, args, def).setPos(t.pos).setType(t.type);
        copy.constructor = t.constructor;
        copy.constructorType = t.constructorType;
        copy.varargsElement=t.varargsElement;

        return copy;
    }

    @Override
    public JCTree visitCompoundAssignment(CompoundAssignmentTree node, P p) {
        JCAssignOp t = (JCAssignOp) node;
        JCTree lhs = copy(t.lhs, p);
        JCTree rhs = copy(t.rhs, p);

        return new JCAssignOp(t.getTag(), lhs, rhs, t.operator).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitWildcard(WildcardTree node, P p) {
        JCWildcard t = (JCWildcard) node;
        TypeBoundKind kind = (TypeBoundKind)new TypeBoundKind(t.kind.kind).setPos(t.kind.pos).setType(t.kind.type);
        JCTree inner = copy(t.inner, p);

        return new JCWildcard(kind, inner).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitParameterizedType(ParameterizedTypeTree node, P p) {
        JCTypeApply t = (JCTypeApply) node;
        JCExpression clazz = copy(t.clazz, p);
        List<JCExpression> arguments = copy(t.arguments, p);

        return new JCTypeApply(clazz, arguments).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitNewArray(NewArrayTree node, P p) {
        JCNewArray t = (JCNewArray) node;
        JCExpression elemtype = copy(t.elemtype, p);
        List<JCExpression> dims = copy(t.dims, p);
        List<JCExpression> elems = copy(t.elems, p);

        return new JCNewArray(elemtype, dims, elems).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitErroneous(ErroneousTree node, P p) {
        JCErroneous t = (JCErroneous) node;
        List<? extends JCTree> errs = copy(t.errs, p);

        return new JCErroneous(errs).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitAnnotation(AnnotationTree node, P p) {
        JCAnnotation t = (JCAnnotation) node;
        JCTree annotationType = copy(t.annotationType, p);
        List<JCExpression> args = copy(t.args, p);

        return new JCAnnotation(annotationType, args).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitAssert(AssertTree node, P p) {
        JCAssert t = (JCAssert) node;
        JCExpression cond = copy(t.cond, p);
        JCExpression detail = copy(t.detail, p);

        return new JCAssert(cond, detail).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitBlock(BlockTree node, P p) {
        JCBlock t = (JCBlock) node;
        List<JCStatement> stats = copy(t.stats, p);

        return new JCBlock(t.flags, stats).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitBreak(BreakTree node, P p) {
        JCBreak t = (JCBreak) node;

        // if this break node has a label, set the original target as target for the
        // copied node -> register this node so that the corresponding labeled statement
        // can update this reference if it will be copied, eventually!
        JCTree result = new JCBreak(t.label, t.target).setPos(t.pos).setType(t.type);
        if(t.label!=null) {
            addUnresolvedLabeledTarget(t.label, result);
        }else{
            addUnresolvedTarget(t.target, result);
        }
        return result;
    }

    @Override
    public JCTree visitCase(CaseTree node, P p) {
        JCCase t = (JCCase) node;
        JCExpression pat = copy(t.pat, p);
        List<JCStatement> stats = copy(t.stats, p);

        return new JCCase(pat, stats).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitCatch(CatchTree node, P p) {
        JCCatch t = (JCCatch) node;
        JCVariableDecl param = copy(t.param, p);
        JCBlock body = copy(t.body, p);

        return new JCCatch(param, body).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitClass(ClassTree node, P p) {
        JCClassDecl t = (JCClassDecl) node;
        JCModifiers mods = copy(t.mods, p);
        List<JCTypeParameter> typarams = copy(t.typarams, p);
        JCExpression extending = copy(t.extending, p);
        List<JCExpression> implementing = copy(t.implementing, p);
        List<JCTree> defs = copy(t.defs, p);

        return new JCClassDecl(mods, t.name, typarams, extending, implementing, defs, t.sym).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitCompilationUnit(CompilationUnitTree node, P p) {
        JCCompilationUnit t = (JCCompilationUnit) node;
        List<JCAnnotation> packageAnnotations = copy(t.packageAnnotations, p);
        JCExpression pid = copy(t.pid, p);
        List<JCTree> defs = copy(t.defs, p);

        // Should already have been checked by TreeMaker which created the original node
        /*
        assert packageAnnotations != null;
        for (JCTree def : defs){
            assert     def instanceof JCClassDecl ||
                    def instanceof JCImport ||
                    def instanceof JCSkip ||
                    def instanceof JCErroneous ||
                    (def instanceof JCExpressionStatement && ((JCExpressionStatement)node).expr instanceof JCErroneous) : def.getClass().getSimpleName();
       }
       */

       return new JCCompilationUnit(packageAnnotations, pid, defs, t.sourcefile, t.packge, t.namedImportScope, t.starImportScope).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitContinue(ContinueTree node, P p) {
        JCContinue t = (JCContinue) node;

        // if this continue node has a label, set the original target as target for the
        // copied node -> register this node so that the corresponding labeled statement
        // can update this reference if it will be copied, eventually!
        JCTree result = new JCContinue(t.label, t.target).setPos(t.pos).setType(t.type);

        if(t.label!=null) {
            addUnresolvedLabeledTarget(t.label, result);
        }else{
            addUnresolvedTarget(t.target, result);
        }

        return result;
    }

    @Override
    public JCTree visitDoWhileLoop(DoWhileLoopTree node, P p) {
        JCDoWhileLoop t = (JCDoWhileLoop) node;
        JCStatement body = copy(t.body, p);
        JCExpression cond = copy(t.cond, p);

        return new JCDoWhileLoop(body, cond).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitEmptyStatement(EmptyStatementTree node, P p) {
        JCSkip t = (JCSkip) node;
        return new JCSkip().setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitEnhancedForLoop(EnhancedForLoopTree node, P p) {
        JCEnhancedForLoop t = (JCEnhancedForLoop) node;
        JCVariableDecl var = copy(t.var, p);
        JCExpression expr = copy(t.expr, p);
        JCStatement body = copy(t.body, p);

        return new JCEnhancedForLoop(var, expr, body).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitExpressionStatement(ExpressionStatementTree node, P p) {
        JCExpressionStatement t = (JCExpressionStatement) node;
        JCExpression expr = copy(t.expr, p);

        return new JCExpressionStatement(expr).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitForLoop(ForLoopTree node, P p) {
        JCForLoop t = (JCForLoop) node;
        List<JCStatement> init = copy(t.init, p);
        JCExpression cond = copy(t.cond, p);
        List<JCExpressionStatement> step = copy(t.step, p);
        JCStatement body = copy(t.body, p);

        return new JCForLoop(init, cond, step, body).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitIf(IfTree node, P p) {
        JCIf t = (JCIf) node;
        JCExpression cond = copy(t.cond, p);
        JCStatement thenpart = copy(t.thenpart, p);
        JCStatement elsepart = copy(t.elsepart, p);

        return new JCIf(cond, thenpart, elsepart).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitImport(ImportTree node, P p) {
        JCImport t = (JCImport) node;
        JCTree qualid = copy(t.qualid, p);

        return new JCImport(qualid, t.staticImport).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitLabeledStatement(LabeledStatementTree node, P p) {
        JCLabeledStatement t = (JCLabeledStatement) node;
        JCStatement body = copy(t.body, p);

        JCLabeledStatement result = (JCLabeledStatement) new JCLabeledStatement(t.label, body).setPos(t.pos).setType(t.type);

        // update all copied labeled statements
        if(unresolvedLabeledTargets.containsKey(t.label)){
            // The target of a labelled break or continue is the
            // (non-labelled) statement tree referred to by the label,
            // not the tree representing the labelled statement itself
            for(JCTree stmt : unresolvedLabeledTargets.get(t.label)){
                switch(stmt.getTag()){
                    case JCTree.BREAK:
                        ((JCBreak)stmt).target=TreeInfo.referencedStatement(result);
                        break;
                    case JCTree.CONTINUE:
                        ((JCContinue)stmt).target=TreeInfo.referencedStatement(result);
                        break;
                    default:
                        throw new RuntimeException("Unexpected labeled statement: "+stmt.getClass().getName());
                }
            }
        }
        // remove this label since we updated all references
        unresolvedLabeledTargets.remove(t.label);

        return result;
    }

    @Override
    public JCTree visitMethod(MethodTree node, P p) {
        JCMethodDecl t  = (JCMethodDecl) node;
        JCModifiers mods = copy(t.mods, p);
        JCExpression restype = copy(t.restype, p);
        List<JCTypeParameter> typarams = copy(t.typarams, p);
        List<JCVariableDecl> params = copy(t.params, p);
        List<JCExpression> thrown = copy(t.thrown, p);
        JCBlock body = copy(t.body, p);
        JCExpression defaultValue = copy(t.defaultValue, p);

        return new JCMethodDecl(mods, t.name, restype, typarams, params, thrown, body, defaultValue,t.sym).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitModifiers(ModifiersTree node, P p) {
        JCModifiers t = (JCModifiers) node;
        List<JCAnnotation> annotations = copy(t.annotations, p);

        return new JCModifiers(t.flags, annotations).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitReturn(ReturnTree node, P p) {
        JCReturn t = (JCReturn) node;
        JCExpression expr = copy(t.expr, p);

        return new JCReturn(expr).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitSwitch(SwitchTree node, P p) {
        JCSwitch t = (JCSwitch) node;
        JCExpression selector = copy(t.selector, p);
        List<JCCase> cases = copy(t.cases, p);

        return new JCSwitch(selector, cases).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitSynchronized(SynchronizedTree node, P p) {
        JCSynchronized t = (JCSynchronized) node;
        JCExpression lock = copy(t.lock, p);
        JCBlock body = copy(t.body, p);

        return new JCSynchronized(lock, body).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitThrow(ThrowTree node, P p) {
        JCThrow t = (JCThrow) node;
        JCTree expr = copy(t.expr, p);

        return new JCThrow(expr).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitTry(TryTree node, P p) {
        JCTry t = (JCTry) node;
        List<JCTree> resources = copy(t.resources, p);
        JCBlock body = copy(t.body, p);
        List<JCCatch> catchers = copy(t.catchers, p);
        JCBlock finalizer = copy(t.finalizer, p);

        return new JCTry(resources, body, catchers, finalizer).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitTypeParameter(TypeParameterTree node, P p) {
        JCTypeParameter t = (JCTypeParameter) node;
        List<JCExpression> bounds = copy(t.bounds, p);

        return new JCTypeParameter(t.name, bounds).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitVariable(VariableTree node, P p) {
        JCVariableDecl t = (JCVariableDecl) node;
        JCModifiers mods = copy(t.mods, p);
        JCExpression vartype = copy(t.vartype, p);
        JCExpression init = copy(t.init, p);

        return new JCVariableDecl(mods, t.name, vartype, init, t.sym).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitWhileLoop(WhileLoopTree node, P p) {
        JCWhileLoop t = (JCWhileLoop) node;
        JCStatement body = copy(t.body, p);
        JCExpression cond = copy(t.cond, p);

        return new JCWhileLoop(cond, body).setPos(t.pos).setType(t.type);
    }

    @Override
    public JCTree visitOther(Tree node, P p) {
        JCTree tree = (JCTree) node;
        switch (tree.getTag()) {
            case JCTree.LETEXPR: {
                LetExpr t = (LetExpr) node;
                List<JCVariableDecl> defs = copy(t.defs, p);
                JCTree expr = copy(t.expr, p);

                return new LetExpr(defs, expr).setPos(t.pos).setType(t.type);
            }
            default:
                throw new AssertionError("unknown tree tag: " + tree.getTag());
        }
    }

    public JCTree visitUnionType(UnionTypeTree node, P p) {
        JCTypeUnion t = (JCTypeUnion) node;
        List<JCExpression> components = copy(t.alternatives, p);

        return new JCTypeUnion(components).setPos(t.pos).setType(t.type);
    }
}
