package com.sun.tools.javac.mutation;

import static com.sun.tools.javac.code.Flags.FINAL;
import static com.sun.tools.javac.code.Flags.HASINIT;
import static com.sun.tools.javac.code.Flags.PARAMETER;
import static com.sun.tools.javac.code.Kinds.MTH;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.sun.source.tree.LineMap;
import com.sun.source.tree.Tree.Kind;
import com.sun.source.util.TreePath;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symbol.ClassSymbol;
import com.sun.tools.javac.code.Symbol.OperatorSymbol;
import com.sun.tools.javac.code.Symbol.TypeSymbol;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Type.MethodType;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.comp.AttrContext;
import com.sun.tools.javac.comp.ConstFold;
import com.sun.tools.javac.comp.Env;
import com.sun.tools.javac.comp.ValidMutationFlow;
import com.sun.tools.javac.jvm.ByteCodes;
import com.sun.tools.javac.mutation.context.NodeContext;
import com.sun.tools.javac.mutation.context.NodeContextVisitor;
import com.sun.tools.javac.mutation.operator.IMutationProvider;
import com.sun.tools.javac.mutation.operator.IMutationProvider.LIT_TYPE;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperator;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperatorType;
import com.sun.tools.javac.mutation.operator.IMutationProvider.STD_TYPE;
import com.sun.tools.javac.tree.AttrTreeCopier;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCAssignOp;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCBreak;
import com.sun.tools.javac.tree.JCTree.JCCase;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCConditional;
import com.sun.tools.javac.tree.JCTree.JCContinue;
import com.sun.tools.javac.tree.JCTree.JCDoWhileLoop;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCForLoop;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCIf;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCParens;
import com.sun.tools.javac.tree.JCTree.JCReturn;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCUnary;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.JCTree.JCWhileLoop;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Names;
import com.sun.tools.javac.util.Pair;
/**
 * The AST mutator implemented as a <code>TreeTranslator</code>.
 *
 * TODO: This class is a mess! It is entirely monolithic, mostly for efficiency
 *       reasons, which turns out to be less of a concern: generating mutants
 *       is a one-time process, so a larger overhead is acceptable for a code
 *       base that is substantially easier to read/maintain/improve.
 *
 * TODO: Carry TreePath in translator and remove the current hack that
 *       uses global variables to monitor the state in recursive calls.
 */
public class Mutator extends TreeTranslator implements IMutator{

    private static Mutator instance = null;

    // Get the Mutator instance for the current context
    public static Mutator instance(Context context, IMutationProvider provider) {
        if (instance == null) {
            instance = new Mutator(context, provider);
        } else if (context != instance.context) {
            instance = null;
            instance = new Mutator(context, provider);
        }
        return instance;
    }

    /*
     *  Defaults for mutant identifier and coverage method:
     *      - package: major.mutation
     *      - class:   Config
     *      - field:   __M_NO
     *      - method:  COVERED
     */
    private static final String IDENT_PACKAGE_NAME = System.getProperty("major.driver.package", "major.mutation");
    private static final String IDENT_CLASS_NAME = System.getProperty("major.driver.class", "Config");
    private static final String IDENT_NAME = System.getProperty("major.driver.identifier", "__M_NO");
    private static final String COVER_NAME = System.getProperty("major.driver.covered", "COVERED");

    /*
     * Additional options for Major
     */
    private static final boolean EXPORT_MUTANTS = Boolean.getBoolean("major.export.mutants");
    private static final String EXPORT_DIR = System.getProperty("major.export.directory", "./mutants");
    private static final boolean STRICT_FLOW = Boolean.getBoolean("major.strict.flow");
    private static final boolean EXPORT_CONTEXT = Boolean.getBoolean("major.export.context");

    // The mutation provider used to query for enabled mutants and corresponding definitions.
    private IMutationProvider mutationProvider;

    // The overall number of all generated mutants.
    private int mutCounter = 0;

    // Symbols for mutant identifier, coverage method, and driver class.
    private TypeSymbol symIdentClass;
    private Symbol symIdent;
    private Symbol symCover;

    private Context context;
    private Symtab tab;
    private TreeMaker make;
    private AttrTreeCopier<JCTree> copy;
    private Names names;
    private LineMap lineMap;
    private JCTree treeCopy;
    private Env<AttrContext> env;

    private Map<MutationOperatorType, Map<String, Pair<MutationOperator, Integer>>> opcodeMap = new TreeMap<MutationOperatorType, Map<String, Pair<MutationOperator, Integer>>>();
    private Map<String, OperatorSymbol> opNameMap = new TreeMap<String, OperatorSymbol>();
    private Symbol symEQ, symNE, symAND, symLT, symGT, symOR, symNOT;
    private boolean mutate=true;
    private boolean mutateLiteral=true;
    private boolean mutateExec=true;

    // Cache for end position of mutated tree nodes
    private Map<Integer,Map<Long,Integer>> endPositions = new HashMap<Integer,Map<Long,Integer>>(60);
    // Cache for simple boolean expressions used in if statements
    private Map<Long,Boolean> boolExprCache;

    private JCClassDecl classDef;
    // Flat name of current class and method
    private String currentEntity;

    private FileWriter mutantsLog;
    private FileWriter mutantsContext;

    /**
     * Constructs a mutator for the given context. We use a singleton instance per context.
     */
    private Mutator(Context c, IMutationProvider provider) {
        context=c;
        mutationProvider = provider;
        tab = Symtab.instance(c);
        make = TreeMaker.instance(c);
        copy = new AttrTreeCopier<JCTree>();
        names = Names.instance(c);

        // Dynamically reference the coverage method and the identifier within the driver class.
        // We do not use the compilers resolver to avoid being forced to have the driver on the
        // classpath during compilation -> An Exception will be thrown at runtime if the driver
        // is not on the classpath or does not meet the requirements!
        symIdentClass = new Symbol.ClassSymbol(Flags.PUBLIC, names.fromString(IDENT_CLASS_NAME), new Symbol.PackageSymbol(names.fromString(IDENT_PACKAGE_NAME), null));
        symIdent=new Symbol.VarSymbol(Flags.PUBLIC|Flags.STATIC|Flags.HASINIT, names.fromString(IDENT_NAME), tab.intType, symIdentClass);
        symCover=new Symbol.MethodSymbol(Flags.PUBLIC|Flags.STATIC,
                                         names.fromString(COVER_NAME),
                                         new MethodType(List.from(new Type[]{tab.intType, tab.intType}), tab.booleanType, List.<Type>nil(), symIdentClass),
                                         symIdentClass);

        // open log file
        try {
            mutantsLog = new FileWriter("mutants.log");
        } catch (IOException e) {
            System.err.println("Cannot open mutants log file!");
            e.printStackTrace();
        }
        if (EXPORT_CONTEXT) {
            try {
                mutantsContext = new FileWriter("mutants.context");
                writeContextHeader();
            } catch (IOException e) {
                System.err.println("Cannot open mutants context file!");
                e.printStackTrace();
            }
        }

        // init all necessary operator symbols
        init();
    }

    /**
     * Initialize all necessary operator symbols and the opcode mapping
     */
    private void init(){
        for (Symbol s : tab.predefClass.members().getElements()) {
            if(! (s instanceof OperatorSymbol)) continue;

            opNameMap.put(s.toString(), (OperatorSymbol)s);

            // cache symbol ==(int,int) for binary expressions with mutant identifier e.g. (ID==xyz) ? mutant : orig
            if(symEQ == null && ((OperatorSymbol)s).opcode == ByteCodes.if_icmpeq && ((MethodType)s.type).contains(tab.intType)){
                symEQ = s;
            }
            // cache symbol !=(int,int) for binary expressions with mutant identifier e.g. if(ID!=xyz) continue
            else if(symNE == null && ((OperatorSymbol)s).opcode == ByteCodes.if_icmpne && ((MethodType)s.type).contains(tab.intType)){
                symNE = s;
            }
            // cache symbol &&(boolean,boolean) for binary expressions
            if(symAND == null && ((OperatorSymbol)s).opcode == ByteCodes.bool_and){
                symAND = s;
            }
            // cache symbol <(int,int) for binary expressions with mutant identifier (ID<x)
            if(symLT == null && ((OperatorSymbol)s).opcode == ByteCodes.if_icmplt && ((MethodType)s.type).contains(tab.intType)){
                symLT = s;
            }

            // cache symbol >(int,int) for binary expressions with mutant identifier (ID>x)
            if(symGT == null && ((OperatorSymbol)s).opcode == ByteCodes.if_icmpgt && ((MethodType)s.type).contains(tab.intType)){
                symGT = s;
            }

            // cache symbol ||(boolean,boolean) for binary expressions
            if(symOR == null && ((OperatorSymbol)s).opcode == ByteCodes.bool_or){
                symOR = s;
            }

            // cache symbol !(boolean) for unary expressions
            if(symNOT == null && ((OperatorSymbol)s).opcode == ByteCodes.bool_not){
                symNOT = s;
            }
        }

        // map unary operators
        Map<String, Pair<MutationOperator, Integer>> tmp = new HashMap<String, Pair<MutationOperator, Integer>>();
        tmp.put("!", Pair.of(MutationOperator.ORU, JCTree.NEG));
        tmp.put("~", Pair.of(MutationOperator.ORU, JCTree.COMPL));
        tmp.put("+", Pair.of(MutationOperator.ORU, JCTree.POS));
        tmp.put("-", Pair.of(MutationOperator.ORU, JCTree.NEG));
        opcodeMap.put(MutationOperatorType.UNR, tmp);

        // map binary operators
        tmp = new HashMap<String, Pair<MutationOperator, Integer>>();
        tmp.put("-", Pair.of(MutationOperator.AOR, JCTree.NEG));
        tmp.put("+", Pair.of(MutationOperator.AOR, JCTree.PLUS));
        tmp.put("-", Pair.of(MutationOperator.AOR, JCTree.MINUS));
        tmp.put("*",  Pair.of(MutationOperator.AOR, JCTree.MUL));
        tmp.put("/",  Pair.of(MutationOperator.AOR, JCTree.DIV));
        tmp.put("%",  Pair.of(MutationOperator.AOR, JCTree.MOD));

        tmp.put("<<",  Pair.of(MutationOperator.SOR, JCTree.SL));
        tmp.put(">>",  Pair.of(MutationOperator.SOR, JCTree.SR));
        tmp.put(">>>",  Pair.of(MutationOperator.SOR, JCTree.USR));

        tmp.put("|",  Pair.of(MutationOperator.LOR, JCTree.BITOR));
        tmp.put("&",  Pair.of(MutationOperator.LOR, JCTree.BITAND));
        tmp.put("^",  Pair.of(MutationOperator.LOR, JCTree.BITXOR));

        tmp.put("||",  Pair.of(MutationOperator.COR, JCTree.OR));
        tmp.put("&&",  Pair.of(MutationOperator.COR, JCTree.AND));

        tmp.put("<",  Pair.of(MutationOperator.ROR, JCTree.LT));
        tmp.put("<=", Pair.of(MutationOperator.ROR, JCTree.LE));
        tmp.put(">",  Pair.of(MutationOperator.ROR, JCTree.GT));
        tmp.put(">=", Pair.of(MutationOperator.ROR, JCTree.GE));
        tmp.put("==", Pair.of(MutationOperator.ROR, JCTree.EQ));
        tmp.put("!=", Pair.of(MutationOperator.ROR, JCTree.NE));

        opcodeMap.put(MutationOperatorType.BIN, tmp);
    }


    @Override
    public int getMutationCounter(){
        return mutCounter;
    }

    @Override
    public void mutateTree(Env<AttrContext>env) {
        this.env = env;
        this.lineMap=env.toplevel.lineMap;
        // copy original tree
        treeCopy = copy.copy(env.tree);
        // init cache for simple boolean expressions
        boolExprCache = new TreeMap<Long,Boolean>();

        translate(env.tree);

        /*
         * Fix flow violations for short circuit operators caused by instrumentation
         */
        TreeMaker localMake = make.forToplevel(env.toplevel);
        ValidMutationFlow.fixFlowViolations(context, env, localMake);

        treeCopy=null;
    }

    @Override
    public <T extends JCTree> T translate(T tree) {
        if (tree!=null) {
            if (!endPositions.containsKey(tree.getTag())) {
                endPositions.put(tree.getTag(), new HashMap<Long,Integer>(300));
            }
            endPositions.get(tree.getTag()).put(getTreePosHash(tree), tree.getEndPosition(env.toplevel.endPositions));
        }
        return super.translate(tree);
    }

    @Override
    public void visitBinary(JCBinary tree) {
        if(!mutationProvider.isOperatorEnabled(getFlatnameList(), convertOperator(tree.operator)) || !mutate){
            tree.lhs = translate(tree.lhs);
            tree.rhs = translate(tree.rhs);
            result = tree;
            checkConstFolding(tree, tree.lhs.type, tree.rhs.type);
            return;
        }

        JCExpression res = tree;


        //TODO Remove clone and traverse children after buildConditional
        //TODO Remove second parm from buildConditional

        // clone original expression (without modified child nodes) as image for the conditional mutation process
        JCBinary clone = copy.copy(tree);

        // traverse child nodes
        tree.lhs = translate(tree.lhs);
        tree.rhs = translate(tree.rhs);

        Collection<String> mutations = mutationProvider.getMutationOperators(getFlatnameList(), MutationOperatorType.BIN, tree.operator.toString());
        if(!mutations.isEmpty()){

            /*
             * Two common pattern of uninitialized local vars:
             *
             * 1)
             * Var v;
             * if(flag && (v=getVar())!=null){
             *         v.xyz();
             * }
             *
             * 2)
             * Var v;
             * if(flag || v=getVar())==null){
             *         return;
             * }
             * v.xyz();
             *
             */

            // TODO: ensure that getTag returns opcode -> write tests for binary and unary
            int opcode = tree.getTag();

            // flag to indicate whether the created mutants should be verified with the flow checker
            boolean verifyMutants = false;

            // Check COR and ROR mutants to see whether they contain assignments to uninitialized
            // local variables or final fields
            if((opcode>=JCTree.EQ && opcode<=JCTree.GE) || opcode==JCTree.AND ||opcode==JCTree.OR){
                final class UnInitVarException extends RuntimeException{
                    public static final long serialVersionUID = 1L;
                }
                try{
                    new TreeScanner(){
                            @Override
                            public void visitAssign(JCAssign tree) {
                                Symbol sym = TreeInfo.symbol(tree.lhs);
                                if(sym!=null && checkInit(sym)){
                                    // we found an assignment -> interrupt the scan
                                    throw new UnInitVarException();
                                }
                            }

                    }.scan(clone);
                }catch(UnInitVarException ve){
                    // we found an assignment -> verify the mutants
                    verifyMutants=true;
                }
            }

            res = buildConditional(tree, clone, mutations.iterator(), verifyMutants);
            // compile time constant folding
            checkConstFolding(tree, tree.lhs.type, tree.rhs.type);
        }

        result = res;
    }

    @Override
    public void visitUnary(JCUnary tree) {
        if(!mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.ORU) || !mutate){
            super.visitUnary(tree);
            checkConstFolding(tree, tree.arg.type);
            return;
        }
        JCExpression res = tree;

        Collection<String> mutations = mutationProvider.getMutationOperators(getFlatnameList(), MutationOperatorType.UNR, tree.operator.toString());
        if(!mutations.isEmpty()){
            // clone original expression (without modified child nodes) as image for the conditional mutation process
            JCUnary clone = copy.copy(tree);

            res = buildConditional(tree, clone, mutations.iterator());
        }

        tree.arg = translate(tree.arg);
        checkConstFolding(tree, tree.arg.type);

        result = res;
    }

    @Override
    public void visitClassDef(JCClassDecl tree) {
        JCClassDecl classDefPrev = classDef;
        classDef = tree;
        String currentEntityPrev = currentEntity;
        currentEntity=tree.sym.flatName().toString();
        super.visitClassDef(tree);
        classDef = classDefPrev;
        currentEntity=currentEntityPrev;
    }

    @Override
    public void visitMethodDef(JCMethodDecl tree) {
        String currentEntityPrev = currentEntity;

        StringBuffer buf = new StringBuffer("(");
        List<Type> paramTypes = tree.type.getParameterTypes();
        if (paramTypes.size() > 0) {
            buf.append(paramTypes.get(0));
            for (int i=1; i<paramTypes.size(); ++i) {
                buf.append(",").append(paramTypes.get(i));
            }
        }
        buf.append(")");
        currentEntity += "@" + tree.name + "::" + buf.toString();
        super.visitMethodDef(tree);
        currentEntity=currentEntityPrev;
    }

    @Override
    public void visitCase(JCCase tree) {
        boolean oldMutate = mutate;
        // Do not mutate case literals
        mutate = false;
        tree.pat = translate(tree.pat);
        // restore old literal mutation flag
        mutate=oldMutate;

        tree.stats = translate(tree.stats);

        result = tree;
    }

    @Override
    public void visitVarDef(JCVariableDecl tree) {
        tree.mods = translate(tree.mods);
        tree.vartype = translate(tree.vartype);
        boolean oldMutate = mutate;
        // Do not mutate constant literals
        if((tree.sym.flags_field&Flags.FINAL)!=0 && tree.init!=null && tree.init.type.constValue()!=null){
            mutate=false;
        }
        JCExpression initClone = copy.copy(tree.init);
        int oldMutCounter = mutCounter;
        tree.init = translate(tree.init);
        mutate=oldMutate;
        result = tree;

        // Check whether operator EVR is enabled
        if(!(mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.EVR))){
            return;
        }

        // Return if tree doesn't have an initializer, if tree init is an enum init, if tree init is the null literal, or if tree init has been mutated already
        if (tree.init==null || TreeInfo.isEnumInit(tree) || tree.init.type==tab.botType || mutCounter > oldMutCounter) {
            return;
        }

        tree.init = replaceTreeWithDefault(tree.init, initClone);
        result = tree;
       }

    @Override
    public void visitWhileLoop(JCWhileLoop tree) {
        boolean oldMutateLiteral = mutateLiteral;
        // Do not mutate the constant condition ('true') of infinite loops
        if(tree.cond.type.isTrue()){
            mutateLiteral = false;
        }
        tree.cond = translate(tree.cond);
        // restore old literal mutation flag
        mutateLiteral=oldMutateLiteral;

        tree.body = translate(tree.body);

        result = tree;
    };

    @Override
    public void visitDoLoop(JCDoWhileLoop tree) {
        boolean oldMutateLiteral = mutateLiteral;
        tree.body = translate(tree.body);

        // Do not mutate the constant condition ('true') of infinite loops
        if(tree.cond.type.isTrue()){
            mutateLiteral = false;
        }
        tree.cond = translate(tree.cond);
        // restore old literal mutation flag
        mutateLiteral=oldMutateLiteral;

        result = tree;
    }

    @Override
    public void visitAnnotation(JCAnnotation tree) {
        boolean oldMutateLiteral = mutateLiteral;
        // wo currently do not mutate annotations -> TODO should we?
        mutateLiteral=false;
        tree.annotationType = translate(tree.annotationType);
        tree.args = translate(tree.args);
        mutateLiteral=oldMutateLiteral;

        result = tree;
    }


    @Override
    public void visitForLoop(JCForLoop tree) {
        tree.init = translate(tree.init);

        boolean oldMutateLiteral = mutateLiteral;
        // Do not mutate the constant condition ('true') of infinite loops
        if(tree.cond!=null && tree.cond.type.isTrue()){
            mutateLiteral = false;
        }
        tree.cond = translate(tree.cond);
        // restore old literal mutation flag
         mutateLiteral=oldMutateLiteral;

         boolean oldMutateExec = mutateExec;
         mutateExec=false;
        tree.step = translate(tree.step);
        mutateExec = oldMutateExec;

        tree.body = translate(tree.body);

        result = tree;
    }

    // TODO Should we replace a break target label with a valid alternative label?
    @Override
    public void visitBreak(JCBreak tree) {
        STD_TYPE stdType = STD_TYPE.BREAK;
        if(!shouldDeleteStmt(stdType)){
            super.visitBreak(tree);
            return;
        }
        result = deleteContBreakReturn(tree, stdType);
    }

    // TODO Should we replace a continue target label with a valid alternative label?
    @Override
    public void visitContinue(JCContinue tree) {
        STD_TYPE stdType = STD_TYPE.CONT;
        if(!shouldDeleteStmt(stdType)){

            super.visitContinue(tree);
            return;
        }
        result = deleteContBreakReturn(tree, stdType);
    }

    // TODO We need cleaner semantics for the STD operator; this greedy approach may not be
    // intuitive and what the user wants -> define a new operator that explicitly replaces
    // return values.
    @Override
    public void visitReturn(JCReturn tree) {
        // Check whether operator STD or EVR is enabled and applicable
        boolean isEVR = mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.EVR);
        STD_TYPE stdType = STD_TYPE.RETURN;
        if (!(shouldDeleteStmt(stdType) || isEVR)) {
            super.visitReturn(tree);
            return;
        }

        int oldMutCounter = mutCounter;
        JCExpression origExpr = copy.copy(tree.expr);
        tree.expr = translate(tree.expr);
        result = tree;

        if (shouldDeleteStmt(stdType)) {
            result = deleteContBreakReturn(tree, stdType);
        }

        // Only mutate return value (to a default value) if no other mutation operator applies
        // and if a return value exists.
        if (isEVR && mutCounter == oldMutCounter && tree.expr != null) {
            tree.expr = replaceTreeWithDefault(tree.expr, origExpr);
            // Always (re)set result after translating children!!!
            result = tree;
        }
    }

    // TODO Should we replace a continue target label with a valid alternative label?
    private JCStatement deleteContBreakReturn(JCStatement tree, STD_TYPE stdType) {
        JCStatement result = tree;
        // Simple sanity check
        if (!shouldDeleteStmt(stdType)) {
            return result;
        }

        TreePath path = TreePath.getPath(env.toplevel, tree);
        while (path.getLeaf() == tree || path.getLeaf() instanceof JCParens) {
            path = path.getParentPath();
        }

        JCTree parent = (JCTree)path.getLeaf();
        JCStatement mutant;
        switch (parent.getKind()) {
        case BLOCK:
            JCBlock block = (JCBlock)parent;
            mutant = copy.copy(block);
            List<JCStatement> stmts = ((JCBlock)mutant).stats;
            ((JCBlock)mutant).stats = List.<JCStatement>nil();
            for (JCStatement stmt : stmts) {
                if (getTreePosHash(stmt) != getTreePosHash(tree)) {
                    ((JCBlock)mutant).stats = ((JCBlock)mutant).stats.append(stmt);
                }
            }
            break;
        case CASE:
            JCCase caze = (JCCase)parent;
            mutant = copy.copy(caze);
            stmts = ((JCCase)mutant).stats;
            ((JCCase)mutant).stats = List.<JCStatement>nil();
            for (JCStatement stmt : stmts) {
                if (getTreePosHash(stmt) != getTreePosHash(tree)) {
                    ((JCCase)mutant).stats = ((JCCase)mutant).stats.append(stmt);
                }
            }
            break;
        case IF:
            JCIf iff = (JCIf)parent;
            mutant = copy.copy(iff);
            if (iff.thenpart == tree) {
                ((JCIf)mutant).thenpart = make.Block(0, List.<JCTree.JCStatement>nil()).setPos(mutant.pos);
            } else if (iff.elsepart == tree) {
                ((JCIf)mutant).elsepart = make.Block(0, List.<JCTree.JCStatement>nil()).setPos(mutant.pos);
            } else {
                throw new IllegalArgumentException("Couldn't localize continue/break in if statement: "+iff);
            }
            break;
        default:
            throw new IllegalArgumentException("Unexpected parent type for continue/break/return statement: "+parent.getKind()+": "+parent);
        }

        if (isStmtMutantValid((JCStatement)parent, mutant)) {
            result = deleteStatement(tree, stdType.toString(), tree, null);
        }
        return result;
    }

    @Override
    public void visitExec(JCExpressionStatement tree) {
        boolean isSTD = mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.STD);
        boolean isEVR = mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.EVR);

        //TODO: remove clone and traverse tree pre-order?
        JCExpressionStatement clone = copy.copy(tree);
        int oldMutCounter = mutCounter;
        tree.expr = translate(tree.expr);
        result = tree;

        // Check whether operator STD is enabled
        if(!(mutateExec && (isSTD || isEVR))) {
            return;
        }

        STD_TYPE stdType;
        // Only mutate method calls -- exclude constructor calls
        if((tree.expr instanceof JCMethodInvocation)){
            if(((JCMethodInvocation)tree.expr).meth instanceof JCIdent
                    && !(((JCIdent)((JCMethodInvocation)tree.expr).meth).getName()==names._super)
                    && !(((JCIdent)((JCMethodInvocation)tree.expr).meth).getName()==names._this)){

                stdType = STD_TYPE.CALL;
            } else if(((JCMethodInvocation)tree.expr).meth instanceof JCFieldAccess){
                stdType = STD_TYPE.CALL;
            } else {
                stdType = null;
            }
        } else if ((tree.expr instanceof JCUnary)){
            switch(tree.expr.getTag()) {
                case JCTree.POSTINC:
                case JCTree.PREINC:
                    stdType = STD_TYPE.INC;
                    break;
                case JCTree.POSTDEC:
                case JCTree.PREDEC:
                    stdType = STD_TYPE.DEC;
                    break;
                default:
                    throw new RuntimeException("Unknown type of unary operator: "+tree.expr.getTag());
            }
        } else if ((tree.expr instanceof JCAssignOp)){
            stdType = STD_TYPE.ASSIGN;
        } else if ((tree.expr instanceof JCAssign)) {
            stdType = STD_TYPE.ASSIGN;
            // If this assignment statement should be deleted, check whether it is valid to do so
            if (shouldDeleteStmt(stdType)) {
                JCAssign expr = (JCAssign)clone.expr;
                JCExpression lhs = expr.lhs;

                JCExpressionStatement mutant = copy.copy(clone);
                JCAssign assign = ((JCAssign)mutant.expr);
                assign.rhs = copy.copy(assign.lhs);

                // Checker whether we can delete the statement (i.e., no flow violations or assignments to final variables);
                // If we can't reset the statement type to null to prevent the deletion of this statement.
                if (isFinal(TreeInfo.symbol(lhs)) || !isExprStmtMutantValid(clone, mutant)) {
                    stdType = null;
                }
            }
        } else {
            System.err.println("Expression statement not mutatable: " + tree);
            stdType = null;
        }

        if (stdType != null && shouldDeleteStmt(stdType)) {
            result = deleteStatement(tree, stdType.toString(), clone, null);
        }

        if (isEVR) {
            // Only mutate the assigned value (to a default value) if no other mutation operator applies and if the assigned value isn't null
            if (isEVR && mutCounter == oldMutCounter
                    && tree.expr instanceof JCAssign && ((JCAssign)tree.expr).rhs.type!=tab.botType) {
                ((JCAssign)tree.expr).rhs = replaceTreeWithDefault(((JCAssign)tree.expr).rhs, ((JCAssign)clone.expr).rhs);
                result = tree;
                return;
            }
        }
    }

    // Check whether:
    // 1) the STD operator is enabled for this tree and
    // 2) the deletion of the statement type is enabled
    private boolean shouldDeleteStmt(STD_TYPE stdType) {
        if (!mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.STD)) {
            return false;
        }
        Collection<String> mutations = mutationProvider.getMutationOperators(
                getFlatnameList(),
                MutationOperatorType.DEL, stdType.toString());

        // Replacement list contains "NO-OP" operator if STD is enabled for this
        // type of statement
        if (!mutations.isEmpty()) {
            return true;
        }

        return false;
    }

    private boolean shouldMutateLiteral(LIT_TYPE litType) {
        if (!mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.LVR)) {
            return false;
        }
        Collection<String> mutations = mutationProvider.getMutationOperators(
                getFlatnameList(),
                MutationOperatorType.LIT, litType.toString());

        // Replacement list, for now, contains "ALL" if LVR is enabled for this type of literal
        if (!mutations.isEmpty()) {
            return true;
        }

        return false;
    }

    //TODO We need to enhance the mml:
    // - the user should be able to define the replacements for numerical literals
    @Override
    public void visitLiteral(JCLiteral tree) {
        LIT_TYPE litType;
        switch (tree.typetag) {
            case TypeTags.CLASS:
                litType = LIT_TYPE.STRING;
                break;
            case TypeTags.BOOLEAN:
                litType = LIT_TYPE.BOOLEAN;
                break;
            default:
                litType = LIT_TYPE.NUMBER;
                break;
        }
        // Check if LVR operator is enabled in general for this tree and if we can mutate this literal
        // -> E.g., we do not mutate case literals or a 'true' literal in while loops
        if(! (shouldMutateLiteral(litType) && mutateLiteral) || !mutate){
            result = tree;
            return;
        }

        Object val1=null, val2=null;
        String ZERO = "0";
        String EMPTY= "<EMPTY>";
        String POS = "POS";
        String NEG = "NEG";
        String TRUE = "TRUE";
        String FALSE= "FALSE";
        String CLASS= "<CLASS>";

        String opFrom = ZERO;
        String opTo1 = POS;
        String opTo2 = NEG;

        switch (tree.typetag) {
            /*
             * Replace constant numerical expression by its arithmetic negation and zero
             * The conditional expression must not have the type of the original literal
             * since the literal has a constant value -> set a constant value for every
             * mutant literal and the correct type for the conditional expressions!
             */
            case TypeTags.BYTE:
                if(tree.value.equals(0)){
                    val1= new Byte((byte)1);
                    val2= new Byte((byte)-1);
                }else{
                    val1= new Byte((byte)0);
                    val2= new Byte((byte)-((Byte)tree.value));
                    if(((Byte)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            case TypeTags.SHORT:
                if(tree.value.equals(0)){
                    val1= new Short((short)1);
                    val2= new Short((short)-1);
                }else{
                    val1= new Short((short)0);
                    val2= new Short((short)-((Short)tree.value));
                    if(((Short)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            case TypeTags.INT:
                if(tree.value.equals(0)){
                    val1= new Integer(1);
                    val2= new Integer(-1);
                }else{
                    val1= new Integer(0);
                    val2= new Integer(-((Integer)tree.value));
                    if(((Integer)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            case TypeTags.LONG:
                if(Long.compare((Long)tree.value, 0L) == 0){
                    val1= new Long(1);
                    val2= new Long(-1);
                }else{
                    val1= new Long(0);
                    val2= new Long(-((Long)tree.value));
                    if(((Long)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            case TypeTags.FLOAT:
                if(Float.compare(0.f, ((Number)tree.value).floatValue()) ==0){
                    val1= new Float(1.);
                    val2= new Float(-1.);
                }else{
                    val1= new Float(0.);
                    val2= new Float(-((Float)tree.value));
                    if(((Float)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            case TypeTags.DOUBLE:
                if(Double.compare(0., ((Number)tree.value).doubleValue())==0){
                    val1= new Double(1.);
                    val2= new Double(-1.);
                }else{
                    val1= new Double(0.);
                    val2= new Double(-((Double)tree.value));
                    if(((Double)tree.value)<0){
                        opFrom = NEG;
                        opTo2=POS;
                    }else{
                        opFrom = POS;
                        opTo2=NEG;
                    }
                    opTo1=ZERO;
                }
                break;

            // replace boolean constant by its logical negation
            case TypeTags.BOOLEAN:
                // boolean is internally represented as integer -> using boolean would lead to a ClassCastException
                if(tree.value.equals(0)){
                    val1 = 1;
                    opFrom = FALSE; opTo1=TRUE;
                }else{
                    val1 = 0;
                    opFrom = TRUE; opTo1=FALSE;
                }
                break;

            // we do not replace String references by the null reference since this mutation was shown to be useless
            // -> use an empty String instead
            case TypeTags.CLASS:
                val1 = "";
                opFrom = CLASS;
                opTo1 = EMPTY;
                break;

            // TODO: Should we replace single chars ?
            case TypeTags.CHAR:
            default:
                result = tree;
                return;
        }

        // Every literal will be replaced by at least one mutant
        JCLiteral mutant1 = copy.copy(tree);
        mutant1.value = val1;
        mutant1.setType(tree.type.constType(val1));

        JCBinary cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
        cond.operator = symEQ;
        cond.type = tab.booleanType;
        result = make.Conditional(cond, mutant1, tree).setType(tree.type.baseType()).setPos(tree.pos);

        JCLiteral from = make.Literal(mutCounter);
        logMutant(MutationOperator.LVR, opFrom, opTo1, mutant1, tree, mutCounter);

        // Check if a second mutant is available
        if(val2!=null){
            JCLiteral mutant2 = copy.copy(tree);
            mutant2.value = val2;
            mutant2.setType(tree.type.constType(val2));

            cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
            cond.operator = symEQ;
            cond.type = tab.booleanType;
            result = make.Conditional(cond, mutant2, (JCExpression)result).setType(tree.type.baseType()).setPos(tree.pos);
            logMutant(MutationOperator.LVR, opFrom, opTo2, mutant2, tree, mutCounter);
        }
        if("true".equals(System.getProperty("major.mutants.only")))
            return;


        JCLiteral to = make.Literal(mutCounter);

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary condM0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(0)).setType(tab.booleanType).setPos(tree.pos);
        condM0.operator = symEQ;
        // Instantiate call of the covered method with corresponding range of mutants
        JCExpression condCover = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(tree.pos), List.<JCExpression>from(new JCExpression[]{from, to})).setType(tab.booleanType).setPos(tree.pos);
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condM0, condCover).setType(tab.booleanType).setPos(tree.pos);
        condExpr.operator = symAND;

        result = make.Conditional(condExpr, tree, (JCExpression)result).setType(tree.type.baseType()).setPos(tree.pos);
    }

    @Override
    public void visitConditional(JCConditional tree) {
        // Mutate the condition first, if possible
        // Check whether the condition is (still) constant
        // -> if so, don't mutate the dead-code part
        tree.cond = translate(tree.cond);
        // Check whether the condition is constant
        if (tree.cond.type.isTrue()) {
            tree.truepart = translate(tree.truepart);
        } else if (tree.cond.type.isFalse()) {
            tree.falsepart = translate(tree.falsepart);
        } else {
            tree.truepart = translate(tree.truepart);
            tree.falsepart = translate(tree.falsepart);
        }

        result = tree;

        // Check whether the type of the child nodes changed
        checkConstFolding((JCConditional)result, tree.cond.type, tree.truepart.type, tree.falsepart.type);
    }

    @Override
    public void visitApply(JCMethodInvocation tree) {
        if (isIfCondition(tree)) {
            JCMethodInvocation clone = copy.copy(tree);
            super.visitApply(tree);
            result = mutateSimpleIfCondition(tree, clone);
            return;
        }
        super.visitApply(tree);
    }

    @Override
    public void visitIdent(JCIdent tree) {
        if (isIfCondition(tree)) {
            JCIdent clone = copy.copy(tree);
            super.visitIdent(tree);
            result = mutateSimpleIfCondition(tree, clone);
            return;
        }
        super.visitIdent(tree);
    }

    @Override
    public void visitSelect(JCFieldAccess tree) {
        if (isIfCondition(tree)) {
            JCFieldAccess clone = copy.copy(tree);
            super.visitSelect(tree);
            result = mutateSimpleIfCondition(tree, clone);
            return;
        }
        super.visitSelect(tree);
    }

    @Override
    public void visitParens(JCParens tree) {
        boolean isConst = tree.expr.type.constValue()!=null;
        super.visitParens(tree);
        // If we mutated a constant condition,
        // use the base type for the conditional tree
        if (isConst && ((JCParens)result).expr.type.constValue() == null) {
            result.type = result.type.baseType();
        }
    }

    /**
     * Iterates over all applicable operators in <code>opList</code> and builds mutated versions
     * of the expression <code>expr</code> by means of the conditional (ternary) operator.
     *
     * @param expr The (binary) expression to be mutated in its current state. The child nodes of this expression may already be mutated.
     * @param orig The original (binary) expression without any mutations -> image for generating mutated expressions.
     * @param opList List of applicable operators
     * @return The complete mutated expression with respect to the operators in opList
     */
    private JCExpression buildConditional(JCBinary expr, JCBinary orig, Iterator<String> opList, boolean verifyMutants){
        // Save first mutant number as 'from' index in the call of the COVERED method
        JCLiteral from = make.Literal(mutCounter+1);

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary condM0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent).setPos(orig.pos), make.Literal(0)).setType(tab.booleanType).setPos(orig.pos);
        condM0.operator = symEQ;
        // Instantiate method call of mut.Config.covered without arguments
        // -> the args will be set after determining the range of the mutation number, i.e. after building all mutations (within the while loop)
        JCExpression condCover = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(orig.pos), null).setType(tab.booleanType).setPos(orig.pos);
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condM0, condCover).setType(tab.booleanType).setPos(orig.pos);
        condExpr.operator = symAND;
        JCExpression result = make.Conditional(condExpr, copy.copy(orig), expr).setType(expr.type.baseType()).setPos(orig.pos);
        if("true".equals(System.getProperty("major.mutants.only")))
            result=expr;

        int mutations=0;
        while(opList.hasNext()){
            OperatorSymbol op = opNameMap.get(opList.next());

            // skip original operator
            if(op==expr.getOperator()) continue;

            // TODO Is there an easy way to introduce the new special operators LHS, RHS, TRUE, and FALSE?
            // TODO Also check Symtab -> remove special operators!?!
            JCExpression mutant = copy.copy(orig);
            if(opcodeMap.get(MutationOperatorType.BIN).containsKey(op.name.toString())) {
                // set the opcode via setTag method
                ((JCBinary)mutant).setTag(opcodeMap.get(MutationOperatorType.BIN).get(op.name.toString()).snd);
                ((JCBinary)mutant).operator = op;

                // check whether original expression has a constant value -> apply constant folding
                if(orig.type.constValue()!=null){
                    Type constType = ConstFold.instance(context).fold(op.opcode, List.<Type>of(((JCBinary)mutant).lhs.type, ((JCBinary)mutant).rhs.type));
                    // constType might be null if an arithmetic exception occurs during constant folding
                    // set to base type in this case!
                    if(constType!=null){
                        mutant.setType(constType);
                    }else{
                        mutant.setType(orig.type.baseType());
                    }
                }
            }else if("TRUE".equals(op.name.toString())){
                mutant = make.Literal(TypeTags.BOOLEAN, 1).setType(orig.type.constType(1)).setPos(orig.pos);
                if (STRICT_FLOW) verifyMutants=true;
            }else if("FALSE".equals(op.name.toString())){
                mutant = make.Literal(TypeTags.BOOLEAN, 0).setType(orig.type.constType(0)).setPos(orig.pos);
                if (STRICT_FLOW) verifyMutants=true;
            }else if("LHS".equals(op.name.toString())){
                mutant = copy.copy(orig.lhs);
            }else if("RHS".equals(op.name.toString())){
                mutant = copy.copy(orig.rhs);
            }else{
                throw new RuntimeException("Unknown opcode for operator: "+op);
            }

            /*
             * Dry run for potentially invalid mutants
             */
            if(verifyMutants){
                JCTree clone = copy.copy(treeCopy);
                replaceExpression(clone, orig, mutant);

                TreeMaker localMake = make.forToplevel(env.toplevel);

                // duplicate environment (updated for cloned tree)
                Env<AttrContext> envClone = env.dup(clone);

                int problems = ValidMutationFlow.checkFlowViolations(context, envClone, localMake);

                // run flow check on already mutated tree
                if((problems & ValidMutationFlow.FLOW_VIOLATION)==0){
                    clone = copy.copy(env.tree);
                    replaceExpression(clone, orig, mutant);
                    // check for further problems
                    problems |= ValidMutationFlow.checkFlowViolations(context, envClone, localMake);

                }

                // Skip invalid mutants
                if((problems & ValidMutationFlow.FLOW_VIOLATION)>0){
                    continue;
                }else if((problems & ValidMutationFlow.UNREACHABLE_STMT)>0){
                    if (STRICT_FLOW) continue;
                }
            }

            JCBinary cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
            cond.operator = symEQ;
            cond.type = tab.booleanType;
            // Log current mutant
            logMutant(opcodeMap.get(MutationOperatorType.BIN).get(orig.operator.name.toString()).fst, orig.operator.toString(), op.toString(), mutant, orig, mutCounter);
            result = make.Conditional(cond, mutant, result).setType(expr.type.baseType());
            ++mutations;
        }

        // Return the expression if it was not mutated (e.g. because mutations were skipped that lead to uninitialized local vars)
        if(mutations==0) return expr;
        JCLiteral to = make.Literal(mutCounter);
        ((JCMethodInvocation)condCover).args = List.from(new JCExpression[]{from, to});

        return result;
    }

    /**
     * Iterates over all applicable operators in <code>opList</code> and builds mutated versions
     * of the expression <code>expr</code> by means of the conditional (ternary) operator.
     *
     * @param expr The (unary) expression to be mutated in its current state. The child nodes of this expression may already be mutated.
     * @param orig The original (unary) expression without any mutations -> image for generating mutated expressions.
     * @param opList List of applicable operators
     * @return The complete mutated expression with respect to the operators in opList
     */
    private JCExpression buildConditional(JCUnary expr, JCUnary orig, Iterator<String> opList){
        // Save first mutant number as 'from' index in the mut.Config.covered method
        JCLiteral from = make.Literal(mutCounter+1);

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary condM0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(0)).setType(tab.booleanType).setPos(orig.pos);
        condM0.operator = symEQ;
        // Instantiate method call of mut.Config.covered without arguments
        // -> the args will be set after determining the range of the mutation number, i.e. after building all mutations (within the while loop)
        JCExpression condCover = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(orig.pos), null).setType(tab.booleanType).setPos(orig.pos);
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condM0, condCover).setType(tab.booleanType).setPos(orig.pos);
        condExpr.operator = symAND;
        JCExpression result = make.Conditional(condExpr, copy.copy(orig), expr).setType(expr.type.baseType()).setPos(orig.pos);
        if("true".equals(System.getProperty("major.mutants.only")))
            result=expr;



        while(opList.hasNext()){
            OperatorSymbol op = opNameMap.get(opList.next());

            if(op==expr.getOperator()) {
                // Skip original operators
                continue;
            }

            JCUnary mutant = copy.copy(orig);
            if(opcodeMap.get(MutationOperatorType.UNR).containsKey(op.name.toString())){
                // set opcode via setTag method
                mutant.setTag(opcodeMap.get(MutationOperatorType.UNR).get(op.name.toString()).snd);
            }
            mutant.operator = op;

            // check whether original expression has a constant value -> apply constant folding
            if(orig.type.constValue()!=null){
                mutant.setType(ConstFold.instance(context).fold(op.opcode, List.<Type>of(mutant.arg.type)));
            }

            JCBinary cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
            cond.operator = symEQ;
            cond.type = tab.booleanType;
            // Log current mutant
            logMutant(opcodeMap.get(MutationOperatorType.UNR).get(op.name.toString()).fst, orig.operator.toString(), op.toString(), mutant, orig, mutCounter);
            result = make.Conditional(cond, mutant, result).setType(expr.type.baseType());
        }
        JCLiteral to = make.Literal(mutCounter);
        ((JCMethodInvocation)condCover).args = List.from(new JCExpression[]{from, to});

        return result;
    }

    private void checkConstFolding(JCExpression tree, Type ... childrenType){
        if(tree.type.constValue()!=null){
            // if the type of at least one child is no longer const -> change type of expression to base type
            for(Type t : childrenType){
                if(t.constValue()==null){
                    tree.setType(tree.type.baseType());
                    return;
                }
            }
        }
    }

    private JCStatement deleteStatement(JCStatement tree, String type, JCStatement orig, JCStatement mutant){
        JCBinary condMut = (JCBinary)make.Binary(JCTree.NE, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter)).setType(tab.booleanType).setPos(tree.pos);
        condMut.operator = symNE;

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary m0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(0)).setType(tab.booleanType).setPos(tree.pos);
        m0.operator = symEQ;
        // Instantiate call of the covered method with corresponding range of mutants
        JCExpression cov = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(tree.pos), List.<JCExpression>from(new JCExpression[]{make.Literal(mutCounter), make.Literal(mutCounter)})).setType(tab.booleanType).setPos(tree.pos);
        JCBinary condCov = (JCBinary) make.Binary(JCTree.AND, m0, cov).setType(tab.booleanType).setPos(tree.pos);
        condCov.operator = symAND;
        // Negate condition -> statement has to be executed unless __M_NO is set to mutants number
        JCUnary negCov = (JCUnary) make.Unary(JCTree.NOT, condCov).setPos(tree.pos).setType(tab.booleanType);
        negCov.operator=symNOT;
        // Complete condition
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condMut, negCov).setType(tab.booleanType).setPos(tree.pos);
        condExpr.operator = symAND;

        JCExpression cond;
        if("true".equals(System.getProperty("major.mutants.only"))){
            cond = condMut;
            cond = (JCBinary) make.Binary(JCTree.AND, cond, cond).setType(tab.booleanType).setPos(tree.pos);
            condExpr.operator = symAND;
        } else {
            cond = condExpr;
        }

        JCStatement res = make.If(cond, tree, mutant).setPos(tree.pos);

        logMutantSTD(MutationOperator.STD, "<"+type+">", mutant==null?"<NO-OP>":mutant.toString(), orig, mutCounter);

        return res;
    }


    // Check whether the provided tree is a simple if condition
    private boolean isIfCondition(JCTree tree) {
        // Check for TypeBoundKind and LetExpr since they are private API and their getKind methods raise an error
        if (tree==null || tree.getTag() == JCTree.TYPEBOUNDKIND || tree.getTag() == JCTree.LETEXPR) {
            return false;
        }
        switch (tree.getKind()) {
            case IDENTIFIER:
            case MEMBER_SELECT:
            case METHOD_INVOCATION:
                Long posHash = getTreePosHash(tree);
                if (!boolExprCache.containsKey(posHash)) {
                    TreePath path = TreePath.getPath(env.toplevel, tree);
                    while (path.getLeaf() == tree || path.getLeaf() instanceof JCParens || path.getLeaf() instanceof JCUnary) {
                        path = path.getParentPath();
                    }
                    boolExprCache.put(posHash, (path.getLeaf() instanceof JCIf ||
                            (path.getLeaf() instanceof JCConditional && ((JCConditional)path.getLeaf()).cond == tree)));
                }
                return boolExprCache.get(posHash);

            default:
                return false;
        }
    }

    private JCLiteral getABSLiteral(JCExpression expr) {
        JCLiteral result;
        switch(expr.type.tag) {
            case TypeTags.BYTE:
                result = make.Literal(new Integer(0));
                break;
            case TypeTags.SHORT:
                result = make.Literal(new Integer(0));
                break;
            case TypeTags.INT:
                result = make.Literal(new Integer(0));
                break;
            case TypeTags.LONG:
                result = make.Literal(new Long(0));
                break;
            case TypeTags.FLOAT:
                result = make.Literal(new Float(0f));
                break;
            case TypeTags.DOUBLE:
                result = make.Literal(new Double(0d));
                break;
            case TypeTags.BOOLEAN:
                result = make.Literal(Boolean.FALSE);
                break;
            case TypeTags.CHAR:
                result = make.Literal(new Integer(0));
                break;
            case TypeTags.CLASS:
            case TypeTags.ARRAY:
            case TypeTags.TYPEVAR:
                result = make.Literal(TypeTags.BOT, null);
                result.setType(tab.botType);
                break;
            default:
                throw new IllegalArgumentException("Unexpected type for default literal: "+expr.type.tag);
        }
        result.setPos(expr.pos);

        return result;
    }

    /*
     * Mutate if conditions that do not have a logical connector (e.g., if (flag) or if (isTrue()))
     */
    private JCExpression mutateSimpleIfCondition(JCExpression tree, JCExpression clone) {
        if (!mutationProvider.isOperatorEnabled(getFlatnameList(), MutationOperator.COR)) {
            return tree;
        }

        // Every simple boolean expression in a condition will be replaced by two mutants -> true, false
        JCLiteral mutant1 = make.Literal(false);
        mutant1.setPos(tree.pos);
        JCLiteral mutant2 = make.Literal(true);
        mutant2.setPos(tree.pos);

        JCBinary cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
        cond.operator = symEQ;
        cond.type = tab.booleanType;
        JCExpression translated = make.Conditional(cond, mutant1, tree).setType(tree.type.baseType()).setPos(tree.pos);

        cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
        cond.operator = symEQ;
        cond.type = tab.booleanType;
        translated = make.Conditional(cond, mutant2, translated).setType(tree.type.baseType()).setPos(tree.pos);

        logMutant(MutationOperator.COR, clone.toString(), "TRUE", mutant1, clone, mutCounter-1);
        logMutant(MutationOperator.COR, clone.toString(), "FALSE", mutant2, clone, mutCounter);

        JCLiteral from = make.Literal(mutCounter-1);
        JCLiteral to = make.Literal(mutCounter);

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary condM0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(0)).setType(tab.booleanType).setPos(tree.pos);
        condM0.operator = symEQ;
        // Instantiate call of the covered method with corresponding range of mutants
        JCExpression condCover = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(tree.pos), List.<JCExpression>from(new JCExpression[]{from, to})).setType(tab.booleanType).setPos(tree.pos);
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condM0, condCover).setType(tab.booleanType).setPos(tree.pos);
        condExpr.operator = symAND;

        JCExpression fake = copy.copy(mutant1);

        translated = make.Conditional(condExpr, fake, translated).setType(tree.type.baseType()).setPos(tree.pos);

        return translated;
    }

    /**
     * Mutate expressions (except for constant literals).
     * The expression tree is replaced by a constant value that has the same
     * type as the mutated expression.
     *
     * @param tree The potentially mutated expression (child nodes may have been mutated already)
     * @param clone The non-mutated (i.e., original) expression
     *
     * @return Returns the mutated expression (embedded, using conditional mutation)
     * for any expression that is not a literal. Returns the unmutated tree for literals.
     */
    private JCExpression replaceTreeWithDefault(JCExpression tree, JCExpression clone) {
        if (clone instanceof JCLiteral) {
            return tree;
        }
        JCLiteral mutant = getABSLiteral(clone);
        JCBinary cond = make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(++mutCounter));
        cond.operator = symEQ;
        cond.type = tab.booleanType;

        JCExpression translated = make.Conditional(cond, mutant, tree).setType(tree.type.baseType());
        if("true".equals(System.getProperty("major.mutants.only"))) {
            return translated;
        }

        // Gather coverage information with an additional expression (__M_NO==0 && Config.covered(from, to)) ? orig : expr;
        JCBinary condM0 = (JCBinary) make.Binary(JCTree.EQ, make.Select(make.Ident(symIdentClass), symIdent), make.Literal(0)).setType(tab.booleanType).setPos(tree.pos);
        condM0.operator = symEQ;
        // Instantiate call of the covered method with corresponding range of mutants
        JCExpression condCover = make.App(make.Select(make.Ident(symIdentClass), symCover).setPos(tree.pos), List.<JCExpression>from(new JCExpression[]{make.Literal(mutCounter), make.Literal(mutCounter)})).setType(tab.booleanType).setPos(tree.pos);
        JCBinary condExpr = (JCBinary) make.Binary(JCTree.AND, condM0, condCover).setType(tab.booleanType).setPos(tree.pos);
        condExpr.operator = symAND;

        JCExpression fake = copy.copy(mutant);
        translated = make.Conditional(condExpr, fake, translated).setType(tree.type.baseType()).setPos(tree.pos);

        // Log mutant
        logMutant(MutationOperator.EVR, "<" + clone.getKind().name() + "(" + clone.type + ")>", "<DEFAULT>", mutant, clone, mutCounter);

        return translated;
    }

    private MutationOperator convertOperator(Symbol s){
        //TODO Enhance check for operator kind
        return opcodeMap.get(s.type.getParameterTypes().size()==2 ? MutationOperatorType.BIN : MutationOperatorType.UNR).get(s.name.toString()).fst;
    }

    private java.util.List<String> getFlatnameList(){
        return Arrays.asList(currentEntity.split("[.$@]|::"));
    }

    private String prettifyFlatname(String flatname) {
        // Remove delimiter between method name and parameter list
        return flatname.replace("::", "");
    }

    private void logMutant(MutationOperator op, String from, String to, JCTree mut, JCTree tree, int m_no){
        writeMutantInline(tree, mut.toString(), m_no);
        // Encode the mutation operator transformation
        String opDef;
        if (to.startsWith("FALSE")) {
            opDef = tree.getKind() + ":" + "FALSE";
        } else if (to.startsWith("TRUE")) {
            opDef = tree.getKind() + ":" + "TRUE";
        } else if (to.startsWith("LHS")) {
            opDef = tree.getKind() + ":" + "LHS";
        } else if (to.startsWith("RHS")) {
            opDef = tree.getKind() + ":" + "RHS";
        } else {
            opDef = tree.getKind() + ":" + mut.getKind();
        }
        exportContext(m_no, op, opDef, tree);
        // truncate call args if they contain a newline
        // the first occurrence of a newline is replaced by "..."
        String strTree = tree.toString();
        // check for newline
        int pos = strTree.indexOf('\n');
        if(pos!=-1){
            strTree = strTree.substring(0, pos)+"...)";
        }
        pos = from.indexOf('\n');
        if(pos!=-1){
            from = from.substring(0, pos)+"...)";
        }
        try {
            // check if debugging -g is enabled!
            if(lineMap==null){
                mutantsLog.write(m_no+":"+op+":"+from+":"+to+":"+prettifyFlatname(currentEntity)+":("+mut.getStartPosition()+"):"+strTree+" |==> "+mut+"\n");
            }else{
                mutantsLog.write(m_no+":"+op+":"+from+":"+to+":"+prettifyFlatname(currentEntity)+":"+lineMap.getLineNumber(mut.getStartPosition())+":"+strTree+" |==> "+mut+"\n");
            }
            mutantsLog.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logMutantSTD(MutationOperator op, String from, String to, JCTree tree, int m_no){
        // Statement deletion: replace the deleted statement with a no-op statement -- simply a semicolon.
        writeMutantInline(tree, ";", m_no);
        String opDef = from + ":" + to;
        exportContext(m_no, op, opDef, tree);
        // truncate call args if they contain a newline
        // the first occurrence of a newline is replaced by "..."
        String strTree = tree.toString();
        // check for newline
        int pos = strTree.indexOf('\n');
        if(pos!=-1){
            strTree = strTree.substring(0, pos)+"...)";
        }
        try {
            // check if debugging -g is enabled!
            if(lineMap==null){
                mutantsLog.write(m_no+":"+op+":"+from+":<NO-OP>:"+prettifyFlatname(currentEntity)+":("+tree.getStartPosition()+"):"+strTree+" |==> "+to+"\n");
            }else{
                mutantsLog.write(m_no+":"+op+":"+from+":<NO-OP>:"+prettifyFlatname(currentEntity)+":"+lineMap.getLineNumber(tree.getStartPosition())+":"+strTree+" |==> "+to+"\n");
            }
            mutantsLog.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeMutantInline(JCTree orig, String repl, int m_no) {
        // Check whether mutant export has been enabled
        if (!EXPORT_MUTANTS) return;

        String packageName = env.toplevel.getPackageName()!=null ? env.toplevel.getPackageName()+"." : "";
        String packageDir = packageName.replaceAll("\\.", "/");
        String className = new File(env.toplevel.getSourceFile().getName()).getName();

        repl.replaceAll("\\n", "");

        // export mutant to file
        try {
            new File(EXPORT_DIR+"/"+m_no+"/"+packageDir).mkdirs();
            BufferedWriter out = new BufferedWriter(new FileWriter(EXPORT_DIR+"/"+m_no+"/"+packageDir+className));

            BufferedReader in = new BufferedReader(new InputStreamReader(env.toplevel.getSourceFile().openInputStream()));
            int endPos = orig.getEndPosition(env.toplevel.endPositions);
            // Could not read end position of current tree -> check whether it exists in the mapping
            if (endPos==-1) {
                assert (endPositions.get(orig.getTag()).containsKey(getTreePosHash(orig)));
                endPos = endPositions.get(orig.getTag()).get(getTreePosHash(orig));
            }
            char[] buf = new char[endPos];

            int read = in.read(buf);
            assert (read == buf.length);
            out.write(buf, 0, orig.getStartPosition());
            // Add parentheses around negated float/double literals
            if( (orig.getKind()==Kind.FLOAT_LITERAL || orig.getKind()==Kind.DOUBLE_LITERAL)    && repl.startsWith("-")) {
                out.write("("+repl+")");
            } else {
                out.write(repl);
            }
            while ((read=in.read(buf)) > 0) {
                out.write(buf,0,read);
            }
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * This method exports different kinds of context of the mutated AST node
     * TODO: Describe the exported fields.
     */
    private void exportContext(int mutantId, MutationOperator op, String opDef, JCTree node) {
        if (!EXPORT_CONTEXT) return;
        List<NodeContext> augContext = getAugmentedContext(node);

        // Determine the type of the mutated node/operator
        String opTypeBasic = simplifyType(node.type);
        String opTypeDetailed;
        if (node instanceof JCBinary) {
            opTypeDetailed = ((JCBinary)node).getOperator().type.toString().replace(',', ';');
        } else if (node instanceof JCExpressionStatement) {
            opTypeDetailed = simplifyType(((JCExpressionStatement)node).expr.type);
        }  else if (node instanceof JCReturn && ((JCReturn)node).expr != null) {
            opTypeDetailed = simplifyType(((JCReturn)node).expr.type);
        } else {
            opTypeDetailed = simplifyType(node.type);
        }
        // The node context of the mutated AST node
        String nodeContext = augContext.head.toString();
        boolean hasLiteralChild = augContext.head.hasLiteralChild();
        boolean hasVariableChild = augContext.head.hasVariableChild();
        boolean hasOperatorChild = augContext.head.hasOperatorChild();
        augContext = augContext.tail;
        // The node context of all parent AST nodes
        StringBuffer bufAstBasic = new StringBuffer();
        StringBuffer bufAstDetailed = new StringBuffer();
        // The node context of the immediate parent node
        String parentBasic = null;
        String parentDetailed = null;
        // The node context of the first statement parent node
        String parentStmtBasic = null;
        String parentStmtDetailed = null;
        for (NodeContext c : augContext) {
            // Determine the basic and detailed context of the immediate parent,
            // ignoring parentheses
            if (parentBasic == null && c.getKind() != Kind.PARENTHESIZED) {
                parentBasic = c.getKind().toString();
                parentDetailed = c.toString();
            }
            // Append current node context to the list of basic and detailed AST contexts
            bufAstBasic.append(c.getKind().toString() + ";");
            bufAstDetailed.append(c.toString() + ";");
            // Determine the basic and detailed context of the first statement
            // parent node that is not a block
            if (parentStmtBasic == null && c.isStmtNode() && !c.isBlockNode()) {
                parentStmtBasic = c.getKind().toString();
                parentStmtDetailed = c.toString();
            }
        }
        // Remove the last separators
        bufAstBasic.deleteCharAt(bufAstBasic.length()-1);
        bufAstDetailed.deleteCharAt(bufAstDetailed.length()-1);

        writeContextRow(""+mutantId,
                     op.toString(),
                     opDef,
                     opTypeBasic,
                     opTypeDetailed,
                     nodeContext,
                     bufAstBasic.toString(),
                     bufAstDetailed.toString(),
                     parentBasic,
                     parentDetailed,
                     parentStmtBasic,
                     parentStmtDetailed,
                     hasLiteralChild ? "1" : "0",
                     hasVariableChild ? "1" : "0",
                     hasOperatorChild ? "1" : "0"
                     );
    }

    /**
     * Simplify a type
     */
    private String simplifyType(Type t) {
        if (t == null) {
            return "null";
        }
        switch(t.tag) {
            case TypeTags.BYTE:
                return "BYTE";
            case TypeTags.CHAR:
                return "CHAR";
            case TypeTags.SHORT:
                return "SHORT";
            case TypeTags.INT:
                return "INT";
            case TypeTags.LONG:
                return "LONG";
            case TypeTags.FLOAT:
                return "FLOAT";
            case TypeTags.DOUBLE:
                return "DOUBLE";
            case TypeTags.BOOLEAN:
                return "BOOLEAN";
            case TypeTags.VOID:
                return "VOID";
            case TypeTags.CLASS:
                return "CLASS";
            case TypeTags.ARRAY:
                return "ARRAY";
            case TypeTags.METHOD:
                return "METHOD";
            case TypeTags.PACKAGE:
                return "PACKAGE";
            case TypeTags.TYPEVAR:
                return "TYPEVAR";
            case TypeTags.WILDCARD:
                return "WILDCARD";
            case TypeTags.FORALL:
                return "FORALL";
            case TypeTags.BOT:
                return "BOT";
            case TypeTags.NONE:
                return "NONE";
            case TypeTags.ERROR:
                return "ERROR";
        }
        return "null";
    }

    /**
     * Write all context fields to file
     */
    private void writeContextHeader() {
        try {
            mutantsContext.write("mutantNo,mutationOperatorGroup,mutationOperator,nodeTypeBasic,nodeTypeDetailed," +
                                 "nodeContextBasic,astContextBasic,astContextDetailed," +
                                 "parentContextBasic,parentContextDetailed,parentStmtContextBasic,parentStmtContextDetailed," +
                                 "hasLiteralChild,hasVariableChild,hasOperatorChild");
            mutantsContext.write("\n");
            mutantsContext.flush();
        } catch (IOException e) {
            throw new RuntimeException("Cannot write context", e);
        }
    }

    /**
     * Write all context fields to file
     */
    private void writeContextRow(String ... fields) {
        try {
            for (int i=0; i<(fields.length-1); ++i) {
                mutantsContext.write(fields[i] + ",");
            }
            mutantsContext.write(fields[fields.length-1]);
            mutantsContext.write("\n");
            mutantsContext.flush();
        } catch (IOException e) {
            throw new RuntimeException("Cannot write context", e);
        }
    }

    /**
     * Returns a list of augmented node contexts by traversing the AST path and
     * determining the augmented context for each node on that path.
     */
    private List<NodeContext> getAugmentedContext(JCTree node) {
        List<JCTree> context = getPath(node, treeCopy);
        NodeContextVisitor ncv = new NodeContextVisitor();
        // Add the node context for the mutated node
        List<NodeContext> augContext = List.of(ncv.getContext(node, null));
        // Iterate over all nodes on the path and add the node context for each
        JCTree child = context.head;
        context = context.tail;
        for (JCTree c : context) {
            augContext = augContext.append(ncv.getContext(c, child));
            child = c;
        }
        return augContext;
    }

    /**
     * Determine the path for a given node in a given compilation unit.
     * Return an empty list if the node doesn't exist in the unit.
     */
    private List<JCTree> getPath(final JCTree node, final JCTree unit) {
        class Result extends Error {
            private static final long serialVersionUID = 1L;
            List<JCTree> path;
            Result(List<JCTree> path) {
                this.path = path;
            }
        }
        class PathFinder extends TreeScanner {
            List<JCTree> path = List.nil();
            public void scan(JCTree tree) {
                if (tree != null) {
                    path = path.prepend(tree);
                    if (isSameNode(tree, node)) {
                        throw new Result(path);
                    }
                    super.scan(tree);
                    path = path.tail;
                }
            }
        }
        try {
            new PathFinder().scan(unit);
        } catch (Result result) {
            return result.path;
        }
        return List.nil();
    }

    /**
     * Replace an expression with a mutated version in a given tree.
     * The equality of trees is determined based on their treePosHash
     *
     * @param tree The toplevel tree that is visited to find the original expression
     * @param orig The original expression
     * @param mutant The mutated version (has to be attributed!)
     * @return Returns the modified toplevel tree
     */
    private JCTree replaceExpression(final JCTree toplevel, final JCBinary orig, final JCExpression mutant) {
        class Replacer extends TreeTranslator {
            @Override
            public void visitBinary(JCBinary tree) {
                if (tree != null) {
                    // replace original expression with mutated version
                    if (getTreePosHash(tree) == getTreePosHash(orig)){
                        result = mutant;
                        return;
                    }
                }
                super.visitBinary(tree);
            }
        }

        return new Replacer().translate(toplevel);
    }

    /**
     * Determines whether the symbol <code>sym</code> represents a variable
     * which needs a definite assignment:
     * local vars and final fields without initializer
     * @param sym The symbol to check
     * @return True if the corresponding variable needs to be checked for definite assignment
     */
     private boolean checkInit(Symbol sym) {
         return ((sym.owner.kind == MTH && (sym.flags() & PARAMETER) == 0)||
                     ((sym.flags() & (FINAL | HASINIT | PARAMETER)) == FINAL &&
                      classDef.sym.isEnclosedBy((ClassSymbol)sym.owner)));
     }

     private boolean isFinal(Symbol sym) {
         if (sym == null) {
             return false;
         }
         return ((sym.flags() & FINAL) == FINAL);
     }

     /**
     * Calculates a simple hash for the position of a tree node
     * We use the upper 32 bit for the start position and the lower ones for the end position.
     * Since the positions are encoded as int, we do not have to deal with overflows
     *
     * @param tree The tree for which the hash is calculated
     * @return Returns the (position) hash of <code>tree</code>
     */
    private static long getTreePosHash(JCTree tree){
        return ((long)tree.getStartPosition())<<32 | tree.pos;
    }

    /**
     * Compare whether two AST nodes are the same.
     *
     * This method checks whether two nodes have the same type and appear in
     * the same position of the AST. In contrast, the default implementation
     * of a node's equals method checks for reference equality.
     */
    private boolean isSameNode(JCTree node1, JCTree node2) {
        if (node1 == null || node2 == null) {
            return false;
        }
        // Check for tags rather than kinds:
        // TypeBoundKind and LetExpr are private API and their getKind methods raise an error.
        return (node1.getTag() == node2.getTag()
                && getTreePosHash(node1) == getTreePosHash(node2));
    }

    private JCTree replaceExpressionStatement(final JCTree toplevel, final JCExpressionStatement orig, final JCExpressionStatement mutant) {
        class Replacer extends TreeTranslator {
            @Override
            public void visitExec(JCExpressionStatement tree) {
                if (tree != null) {
                    // replace original expression with mutated version
                    if (getTreePosHash(tree) == getTreePosHash(orig)){
                        result = mutant;
                        return;
                    }
                }
                super.visitExec(tree);
            }
        }

        return new Replacer().translate(toplevel);
    }

    private JCTree replaceStatement(final JCTree toplevel, final JCStatement orig, final JCStatement mutant) {
        class Replacer extends TreeTranslator {
            @SuppressWarnings("unchecked")
            @Override
            public <T extends JCTree> T translate(T tree) {
                if (tree!=null && getTreePosHash(tree)==getTreePosHash(orig)) {
                    return (T)mutant;
                }
                return super.translate(tree);
            }
        }

        return new Replacer().translate(toplevel);
    }

    private JCTree replaceTree(final JCTree toplevel, final JCTree orig, final JCTree mutant) {
        class Replacer extends TreeTranslator {
            @SuppressWarnings("unchecked")
            @Override
            public <T extends JCTree> T translate(T tree) {
                if (tree!=null && getTreePosHash(tree)==getTreePosHash(orig)) {
                    return (T)mutant;
                }
                return super.translate(tree);
            }
        }

        return new Replacer().translate(toplevel);
    }

    private boolean isExprStmtMutantValid(JCExpressionStatement clone, JCExpressionStatement mutant) {
        JCTree cloneTree = copy.copy(treeCopy);
        replaceExpressionStatement(cloneTree, clone, mutant);

        TreeMaker localMake = make.forToplevel(env.toplevel);

        // duplicate environment (updated for cloned tree)
        Env<AttrContext> envClone = env.dup(cloneTree);

        int problems = ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        // run flow check on already mutated tree
        if((problems & ValidMutationFlow.FLOW_VIOLATION)==0){
            cloneTree = copy.copy(env.tree);
            replaceExpressionStatement(cloneTree, clone, mutant);
            // check for further problems
            problems |= ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        }

        // Skip invalid mutants
        if((problems & ValidMutationFlow.FLOW_VIOLATION)>0){
            return false;
        }else if((problems & ValidMutationFlow.UNREACHABLE_STMT)>0){
            if (STRICT_FLOW) {
                return false;
            }
        }
        //if (!ValidMutationLower.checkViolations(context, envClone, localMake)) {
        //    return false;
        //}
        return true;
    }

    private boolean isStmtMutantValid(JCStatement clone, JCStatement mutant) {
        JCTree cloneTree = copy.copy(treeCopy);
        replaceStatement(cloneTree, clone, mutant);

        TreeMaker localMake = make.forToplevel(env.toplevel);

        // duplicate environment (updated for cloned tree)
        Env<AttrContext> envClone = env.dup(cloneTree);

        int problems = ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        // run flow check on already mutated tree
        if((problems & ValidMutationFlow.FLOW_VIOLATION)==0){
            cloneTree = copy.copy(env.tree);
            replaceStatement(cloneTree, clone, mutant);
            // check for further problems
            problems |= ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        }

        // Skip invalid mutants
        if((problems & ValidMutationFlow.FLOW_VIOLATION)>0){
            return false;
        }else if((problems & ValidMutationFlow.UNREACHABLE_STMT)>0){
            if (STRICT_FLOW) {
                return false;
            }
        }
        //if (!ValidMutationLower.checkViolations(context, envClone, localMake)) {
        //    return false;
        //}
        return true;
    }

    private boolean isMutantValid(JCTree clone, JCTree mutant) {
        JCTree cloneTree = copy.copy(treeCopy);
        replaceTree(cloneTree, clone, mutant);

        TreeMaker localMake = make.forToplevel(env.toplevel);

        // duplicate environment (updated for cloned tree)
        Env<AttrContext> envClone = env.dup(cloneTree);

        int problems = ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        // run flow check on already mutated tree
        if((problems & ValidMutationFlow.FLOW_VIOLATION)==0){
            cloneTree = copy.copy(env.tree);
            replaceTree(cloneTree, clone, mutant);
            // check for further problems
            problems |= ValidMutationFlow.checkFlowViolations(context, envClone, localMake);
        }

        // Skip invalid mutants
        if((problems & ValidMutationFlow.FLOW_VIOLATION)>0){
            return false;
        }else if((problems & ValidMutationFlow.UNREACHABLE_STMT)>0){
            if (STRICT_FLOW) {
                return false;
            }
        }
        //if (!ValidMutationLower.checkViolations(context, envClone, localMake)) {
        //    return false;
        //}
        return true;
    }

}
