\major is a complete mutation analysis framework that divides the mutation 
analysis process into two steps:
\begin{enumerate}
    \item Generate and embed mutants during compilation.
    \item Run the actual mutation analysis (i.e., run tests on the mutants).
\end{enumerate}
For the first step, \major provides a lightweight mutator, which is integrated
in the openjdk Java compiler. For the second step, \major provides a 
default analysis back-end that extends Apache Ant's JUnit task.

\section{Installation}\label{sec:intro/install}
This section describes how to install \major for use from the command line or
Ant. The installation is simple --- all you need is included in the \major
release package!

\begin{itemize*}
    \item Download the \major framework from \url{http://mutation-testing.org/major.zip}.

    \item Unzip \file{major.zip} to create the \dir{major} directory.

    \item Optionally, update your environment and prepend \major's \dir{bin} 
          directory to your execution path (\<PATH> variable).
\end{itemize*}

\noindent
The \dir{major} directory provides the following content:
\vspace*{4pt}
\dirtree{%
    .1 major.
    .2 bin.\DTcomment{Executables for \major's components}.
    .3 \textsf{\it ant}.
    .3 \textsf{\it javac}.
    .3 \textsf{\it mmlc}.
    .2 config.\DTcomment{Archive and sources of \major's driver}.
    .2 doc.\DTcomment{The manual of the current version}.
    .3 \textsf{\it major.pdf}.
    .2 example.\DTcomment{Examples for using \major with Ant and standalone}.
    .3 ant.
    .4 {...}.
    .4 \textsf{\it run.sh}.
    .3 standalone.
    .4 {...}.
    .4 \textsf{\it run.sh}.
    .3 \textsf{\it runAll.sh}.
    .2 lib.\DTcomment{All libraries used by \major}.
    .2 mml.\DTcomment{Example \mml files}.
}

\section{How to get started}

Verify that you are using \major's compiler by running \<javac -version>.
The output should be the following:
\session{sessions/intro.version.session}

Suppose you want to mutate a class \file{MyClass.java}.
The following command mutates and compiles \file{MyClass.java} using all
mutation operators:
\session{sessions/intro.mutate.session}

Note that \<javac> must refer to \major's compiler, which always prints the 
number of generated mutants when the \<-XMutator> flag is enabled. Additionally, 
\major's compiler produces a log file (\file{mutants.log}) for all generated 
mutants. 

All generated mutants are embedded in the compiled class files. The 
\dir{example} directory provides two examples on how to use mutants to perform
mutation analysis on test suites:
\begin{itemize*}
    \item \dir{ant}: mutation analysis using Apache Ant.

    \item \dir{standalone}: mutation analysis using \major's driver standalone.
\end{itemize*}
Execute \<runAll.sh> within the \dir{example} directory to run all examples 
or \<run.sh> in a subdirectory for a particular example.
%
Section~\ref{sec:tutorial} provides a step by step tutorial on how to use \major
for a project using Apache Ant, and the subsequent sections describe \major's 
components and configuration options in detail:

\begin{itemize*}
    \item Section~\ref{sec:javac} provides details about \major's compiler.
    
    \item Section~\ref{sec:mml} describes \major's DSL (\mml).
    
    \item Section~\ref{sec:ant} provides details about \major's default mutation
        analysis back-end.
\end{itemize*}
