\major provides a default back-end for mutation analysis, which extends the
Apache Ant junit task. This back-end supports JUnit 3 and 4 tests.
%
Note that \major does currently not support forking a JVM when executing JUnit
tests, meaning that the \code{fork} option must not be set to \code{true} ---
forking will be supported in a future release.

\section{Setting up a mutation analysis target}
Most software projects that are build with Apache Ant provide a \code{test}
target, which executes the corresponding unit tests. Even if no such target
exists, it can be easily set up to execute a set of given unit tests. The
following code snippet shows an exemplary \code{test} target (See
\url{http://ant.apache.org/manual/Tasks/junit.html} for a detailed description
of the options used in the \code{junit} task): 
%
\begin{Verbatim}[baselinestretch=1.0,fontsize=\small]
<target name="test" description="Run all unit test cases">         
    <junit printsummary="true"                                    
             showoutput="true"                                      
          haltonfailure="true">                              
        
        <formatter type="plain" usefile="true"/>                  
        <classpath path="bin"/>                                                                                                       
        <batchtest fork="no">                                      
            <fileset dir="test">                                   
                <include name="**/*Test*.java"/>                   
            </fileset>                                             
        </batchtest>                                               
    </junit>                                                       
</target>
\end{Verbatim}
%
To enable mutation analysis in \major's enhanced version of the junit task, the
option \code{mutationAnalysis} has to be set to \code{true}. For the sake of
clarity, it is advisable to duplicate an existing \code{test} target, instead of
parameterizing it, and to create a new target, e.g., called \code{mutation.test} 
(See Section~\ref{sec:ant/optimize} for recommended configurations):
%
\newpage
\begin{Verbatim}[baselinestretch=1.0,fontsize=\small]
<target name="mutation.test" description="Run mutation analysis">         
    <junit printsummary="false"                                    
             showoutput="false"                                      
          haltonfailure="true"

       mutationAnalysis="true"
            summaryFile="summary.csv" 
             resultFile="results.csv"
        killDetailsFile="killed.csv">

        <classpath path="bin"/> 
        <batchtest fork="no">                                      
            <fileset dir="test">                                   
                <include name="**/*Test*.java"/>                   
            </fileset>                                             
        </batchtest>                                               
    </junit>                                                       
</target>
\end{Verbatim}

\section{Configuration options for mutation analysis}
\major enhances the junit task with additional options to control the mutation 
analysis process. The available, additional, options are summarized in 
Table~\ref{tab:ant/options}.
\input{sections/ant/tableOptions}

\section{Performance optimization}\label{sec:ant/optimize}
During the mutation analysis process, the provided JUnit tests are repeatedly
executed. For performance reasons, consider the following advices when setting
up the mutation analysis target for a JUnit test suite:
\begin{itemize*}
    \item Turn off logging output (options \code{showsummary}, \code{showoutput}, etc.)
    \item Do not use result formatters (nested task \code{formatter}, especially the \code{usefile} option)
\end{itemize*}
For performance reasons, especially due to frequent class loading and thread
executions, the following JVM options are recommended:
\begin{itemize*}
    \item \code{-XX:ReservedCodeCacheSize=128M}
    \item \code{-XX:MaxPermSize=256M}
\end{itemize*}
