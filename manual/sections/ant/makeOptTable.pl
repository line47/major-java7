#!/usr/bin/env perl
#
# This script generates a LaTex table from the options.csv file.
#
use strict;
use warnings;

$#ARGV==0 or die "usage: $0 <data-file>";

open (IN, "<$ARGV[0]") or die "Cannot open data file! $!";

# print table header
print "\\begin{table}[h!] \n";
print "\\begin{small} \n";
print "\\caption{Additional configuration options for Major-Ant's JUnit task} \n";
print "\\label{tab:ant/options} \n";
print "\\begin{center} \n";
print "\\begin{tabular}{P{3.1cm}|P{4.8cm}|P{3cm}|l} \n";
print "\\rowcolor{colorTableHeader} \n";
print "&&&\\\\ \n";
print "\\noalign{\\vskip-0.5pt} \n";
print "\\rowcolor{colorTableHeader} \n";
print "& \\multirow{-2}{4.5cm}{\\centering\\textbf{Description}} \n";
print "& \\multirow{-2}{3cm}{\\centering\\textbf{Values}} \n";
print "& \\multirow{-2}{*}{\\centering\\textbf{Default}}\\\\ \n";
print "\\noalign{\\vskip+5pt} \n";

# process rows
while(<IN>){
    next if /^#/;
    next if /^$/;
    chomp;
    # escape underscore
    s/_/\\_/g;
    my @cols = split /;/;
    print "\\textbf{$cols[0]} & $cols[1] & \\code{$cols[2]} & \\code{$cols[3]} \\\\ \n";
}

# print table footer
print "\\bottomrule \n";
print "\\multicolumn{4}{l}{*Note: this option leads to the execution of every test on every covered mutant!} \n";
print "\\end{tabular} \n";
print "\\end{center} \n";
print "\\end{small} \n";
print "\\vspace*{-5mm} \n";
print "\\end{table} \n";
