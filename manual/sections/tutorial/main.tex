This sections provides a step by step tutorial on how to use \major for:
\begin{itemize*} 
    \item Configure the mutant generation with \mml scripts 
          (Section~\ref{sec:tutorial/mml}).
    \item Generate mutants with \major's compiler 
          (Section~\ref{sec:tutorial/javac}).
    \item Run mutation analysis with \major's back-end
          (Section~\ref{sec:tutorial/ant}).
\end{itemize*}
You can find all files and the triangle program, which are used in this
tutorial, in the \dir{example} and \dir{mml} directories (see
Section~\ref{sec:intro/install}).

\section{Prepare and compile a \mml script}\label{sec:tutorial/mml}
\major's domain specific language (\mml) supports a detailed specification of the mutation
process. Suppose only return statements, relational operators, and conditional
operators shall be mutated within the method \code{classify} of the class
\code{triangle.Triangle}. The following \mml script (\file{tutorial.mml}) expresses
these requirements: 
\input{sections/tutorial/mmlFile}

\noindent
Section~\ref{sec:mml} provides a detailed description of the syntax and
capabilities of the domain specific language \mml. 
%
The \major framework provides a compiler (\mmlc) that compiles \mml scripts
into a binary representation. Given the \mml script \file{tutorial.mml}, the 
\mmlc compiler is invoked with the following command:
\session{sessions/genMml.session}

\noindent
Note that the second argument is optional --- if omitted, the compiler will add
\file{.bin} to the name of the provided script file, by default.

\section{Generate mutants}\label{sec:tutorial/javac}
To generate mutants based on the compiled \mml script \file{tutorial.mml.bin}
(see Section~\ref{sec:tutorial/mml}), the compiled script has to be passed as an
argument to \major's compiler.

\subsection{Compiling from the command line}
Use the \<-XMutator> option to mutate and compile from the command line:
\session{sessions/genMut.session}

\subsection{Compiling with Apache Ant}
If the source files shall be compiled using Apache Ant, the \code{compile}
target of the corresponding \file{build.xml} file needs to be adapted to use
\major's compiler and to provide the necessary compiler option (See
Section~\ref{sec:javac/ant} for further details):
\begin{Verbatim}[baselinestretch=1.0, fontsize=\small]
<property name="mutOp" value=":NONE"/>
<target name="compile" depends="init" description="Compile">
    <javac srcdir="src" destdir="bin" debug="yes" 
           fork="yes" executable="pathToMajor/javac">
        <compilerarg value="-XMutator${mutOp}"/>
    </javac>
</target>
\end{Verbatim}
Given the compiled \file{tutorial.mml.bin} script and the adapted 
\file{build.xml} file, use the following command to mutate and compile the 
source files:
\session{sessions/genMutAnt.session}


\subsection{Inspecting generated mutants}
If mutation has been enabled (i.e., the \verb|-XMutator| option is used),
\major's compiler reports the number of generated mutants.  Additionally, it
produces the log file \file{mutants.log} that contains detailed information
about the generated mutants (see Section~\ref{sec:javac/log/file} for a description
of the format).  The following example shows the log entries for the first 3
generated mutants:
\scaledSession{sessions/genResult.session}{\scriptsize}

\noindent
\major also supports the export of generated mutants to individual source
files --- see \ref{sec:javac/log/export} for more details.

\section{Analyze mutants}\label{sec:tutorial/ant}
The \file{build.xml} file has to provide a suitable \<mutation.test> target to
use \major's mutation analysis back-end, which performs the mutation analysis
for a given test suite. The following \code{mutation.test} target enables the 
mutation analysis and exports the results to \file{summary.csv},
\file{results.csv}, and 
\file{killed.csv} (see Section~\ref{sec:ant} for further details):
\begin{Verbatim}[baselinestretch=1.0, fontsize=\small]
<target name="mutation.test" description="Run mutation analysis">
    <echo message="Running mutation analysis ..."/>                
    <junit printsummary="false" 
           showoutput="false"            
           mutationAnalysis="true" 
           summaryFile="summary.csv" 
           resultFile="results.csv" 
           killDetailsFile="killed.csv">                                                      

           <classpath path="bin"/>                                    
           <batchtest fork="false">                                   
               <fileset dir="test">                                   
                   <include name="**/*Test*.java"/>                   
               </fileset>                                             
           </batchtest>                                               
    </junit>                                                       
</target>
\end{Verbatim}

\newpage
Using \major's version of \<ant>, the following command invokes the 
\code{mutation.test} target:
\session{sessions/runResult.session}

\noindent
As configured in the \file{build.xml} file, the results of the mutation analysis 
are exported to the files \file{summary.csv}, \file{killed.csv}, and \file{results.csv}, which 
provide the following information:
\begin{itemize*}
    \item \file{summary.csv}: Summary of mutation analysis results.
    \item \file{killed.csv}: The reason why a mutant was killed --- i.e., 
                             assertion failure, exception, or timeout.
    \item \file{results.csv}: Detailed runtime information and mutation analysis
                              results for each executed test.
\end{itemize*}
