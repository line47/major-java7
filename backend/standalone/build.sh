#!/bin/sh

echo
echo "=================================================================="
echo "Compiling and testing the project"
echo "=================================================================="
echo
ant -buildfile build.xml project.compile.tests

echo
echo "=================================================================="
echo "Running major standalone backend unit tests"
echo "=================================================================="
echo
ant -buildfile build.xml backend.compile test
